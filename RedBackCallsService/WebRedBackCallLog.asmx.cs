﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Data;
using System.IO; 
namespace RedBackCallsService
{
    /// <summary>
    /// Summary description for WebRedBackCallLog
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebRedBackCallLog : System.Web.Services.WebService
    {

        [WebMethod]
        public void  RedBackServiceLogingStart()
        {
              string path = Server.MapPath(".");
              if (File.Exists(path + "\\LogInNew.xml") == true)
              {
                  File.Delete(path + "\\LogInNew.xml");
              }
         
            DataTable dt = new DataTable("LogDetails");

           
            dt.Columns.Add("LoginOn");
            string[] strRow=new  string[1];
            strRow[0]="1";
            dt.Rows.Add(strRow);
            dt.WriteXml(path + "\\LogInNew.xml");

            WriteServiceLog("Loging Start " + DateTime.Now, path); 
            
            
           
        }

        [WebMethod]
        public void RedBackServiceLogingEnd()
        {

            string path = Server.MapPath(".");
            if (File.Exists(path + "\\LogInNew.xml") == true)
            {
                File.Delete(path + "\\LogInNew.xml");
            }
       
            DataTable dt = new DataTable("LogDetails");
            dt.Columns.Add("LoginOn");
            string[] strRow = new string[1];
            strRow[0] = "0";
            dt.Rows.Add(strRow);
            dt.WriteXml(path + "\\LogInNew.xml");

            WriteServiceLog("Loging End " + DateTime.Now, path); 
        }


        public void WriteServiceLog(string Log, string Path)
        {

            string FileName = Path + "\\Webservicelog" + DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year + ".txt";
            StreamWriter sw = new StreamWriter(FileName, true);
            sw.WriteLine(DateTime.Now.ToString());
            sw.WriteLine(Log);
            sw.Flush();
            sw.Close();
        }
  
        public string  RedBackServiceGetLogout()
        {

            string path = Server.MapPath(".");
            DataTable dt = new DataTable("LogDetails");
            dt.Columns.Add("LoginOn");
            if (File.Exists(path + "\\LogInNew.xml") == true)
            {
                dt.ReadXml(path + "\\LogInNew.xml");

            }

        

            string LogIn = dt.Rows[0]["LoginOn"].ToString();
            return LogIn;

        }


    }
}
