﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using REDPAGESLib;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using System.Data;
using System.Security.Cryptography;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Data;
using System.IO;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace RedBackCallsService
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService
    {
        private static string Connection = ConfigurationManager.AppSettings["ConnstrRedBack"];

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Cust_Master_AccessCust(string CustNumber, string Title, string CustName, string CustAdd1, string CustAdd2, string CustState, string CustCity, string CustZip, string CustCountry, string CustPhone, string CustFax, string CustEmail, string CustPin, string CustClubFlag, string CustClubDisc, string CustClubDate, string CustPinHint, string CustOptInFlag, string CustError, string CustMessage, string CustOrder, string CustFirstName, string CustLastName, string ClubNumber)
        {
            RedBackLibraryTest obj = new RedBackLibraryTest();
            string strPath = Server.MapPath(".");
            return obj.Cust_MasterAccessCust(CustNumber, Title, CustName, CustAdd1, CustAdd2, CustState, CustCity, CustZip, CustCountry, CustPhone, CustFax, CustEmail, CustPin, CustClubFlag, CustClubDisc, CustClubDate, CustPinHint, CustOptInFlag, CustError, CustMessage, CustOrder, CustFirstName, CustLastName, ClubNumber, strPath);
        }

        //Rajesh 22/10/2012
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Cust_Master_NationalCity(string CustNumber, string Title, string CustName, string CustAdd1, string CustAdd2, string CustState, string CustCity, string CustZip, string CustCountry, string CustPhone, string CustFax, string CustEmail, string CustPin, string CustClubFlag, string CustClubDisc, string CustClubDate, string CustPinHint, string CustOptInFlag, string CustError, string CustMessage, string CustOrder, string CustFirstName, string CustLastName, string ClubNumber)
        {
            RedBackLibraryTest obj = new RedBackLibraryTest();
            string strPath = Server.MapPath(".");
            return obj.Cust_Master_NationalCity(CustNumber, Title, CustName, CustAdd1, CustAdd2, CustState, CustCity, CustZip, CustCountry, CustPhone, CustFax, CustEmail, CustPin, CustClubFlag, CustClubDisc, CustClubDate, CustPinHint, CustOptInFlag, CustError, CustMessage, CustOrder, CustFirstName, CustLastName, ClubNumber, strPath);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Cust_Master_AccessCustTest(string CustNumber, string Title, string CustName, string CustAdd1, string CustAdd2, string CustState, string CustCity, string CustZip, string CustCountry, string CustPhone, string CustFax, string CustEmail, string CustPin, string CustClubFlag, string CustClubDisc, string CustClubDate, string CustPinHint, string CustOptInFlag, string CustError, string CustMessage, string CustOrder, string CustFirstName, string CustLastName, string ClubNumber)
        {
            RedBackLibraryTest obj = new RedBackLibraryTest();
            string strPath = Server.MapPath(".");
            return obj.Cust_Master_AccessCustTest(CustNumber, Title, CustName, CustAdd1, CustAdd2, CustState, CustCity, CustZip, CustCountry, CustPhone, CustFax, CustEmail, CustPin, CustClubFlag, CustClubDisc, CustClubDate, CustPinHint, CustOptInFlag, CustError, CustMessage, CustOrder, CustFirstName, CustLastName, ClubNumber, strPath);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Cust_Master_UpdateCust(string CustNumber, string Title, string CustName, string CustAdd1, string CustAdd2, string CustState, string CustCity, string CustZip, string CustCountry, string CustPhone, string CustFax, string CustEmail, string CustPin, string CustClubFlag, string CustClubDisc, string CustClubDate, string CustPinHint, string CustOptInFlag, string CustError, string CustMessage, string CustOrder, string CustFirstName, string CustLastName, string ClubNumber)
        {
            RedBackLibraryTest obj = new RedBackLibraryTest();
            string strPath = Server.MapPath(".");
            return obj.Cust_Master_UpdateCust(CustNumber, Title, CustName, CustAdd1, CustAdd2, CustState, CustCity, CustZip, CustCountry, CustPhone, CustFax, CustEmail, CustPin, CustClubFlag, CustClubDisc, CustClubDate, CustPinHint, CustOptInFlag, CustError, CustMessage, CustOrder, CustFirstName, CustLastName, ClubNumber, strPath);
        }

        //Code Block changes End 
        //Rajesh 22/10/2012

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Key_Code_Login(string Keycode, string Title, string Catalog, string CustNumber, string CustName, string CustFirstName, string CustLastName, string CustCompany, string CustAdd1, string CustAdd2, string CustCity, string CustState, string CustZip, string CustPhone, string CustEmail, string CustClubFlag, string CustClubDisc, string CustClubDate, string CustError, string CustMessage, string debug_id)
        {
            //RedBackLibraryTest obj = new RedBackLibraryTest();
            //return obj.KeyCodeLogin("406448", "4", "32168530", "22025");
            RedBackLibraryTest obj = new RedBackLibraryTest();
            string strPath = Server.MapPath(".");
            return obj.KeyCodeLogin(Keycode, Title, Catalog, CustNumber, CustName, CustFirstName, CustLastName, CustCompany, CustAdd1, CustAdd2, CustCity, CustState, CustZip, CustPhone, CustEmail, CustClubFlag, CustClubDisc, CustClubDate, CustError, CustMessage, debug_id, strPath);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Order_New(string OrderNumber, string CustFirstName, string CustLastName, string CustAdd1, string CustAdd2, string CustAdd3, string CustCity, string CustState, string CustZip, string CustCountry,
                                string CustPhoneDay, string CustPhoneEve, string CustEmail1, string CustEmail2, string CustPin, string ShipFirstName, string ShipLastName, string ShipAdd1, string ShipAdd2, string ShipAdd3, string ShipCity, string ShipState, string ShipZip, string ShipCountry,
                                string ShipPhone, string ShipEmail, string ShipAttention, string CustNumber, string WebReference, string PromoCode, string Title, string ShipMethod, string PaymentMethod,
                                string CreditCardNumber, string CardExpDate, string CardCVV, string CardAddress, string CardZip, string MerchAmount, string CouponAmount, string DiscAmount, string ShipAmount, string PremShipAmount,
                                string OverweightAmount, string TaxAmount, string TotalAmount, string Comments, string Items, string QtyOrdered, string UnitPrice,
                                string ReleaseDate, string FreeFlag, string PersonalizationCode, string PersonalizationDetail1, string PersonalizationDetail2, string PersonalizationDetail3, string PersonalizationDetail4,
                                string PersonalizationDetail5, string PIN, string PINHint, string OptInFlag, string OptInDate, string OptOutDate, string Etrack, string IPSource,
                                string GVCFlag, string GVCDate, string ValidErrCode, string ValidErrMsg, string OrderErrCode, string OrderErrMsg, string Cert1_Number, string Cert1_Amount, string Cert2_Number, string Cert2_Amount, string Cert3_Number,
                                string Cert3_Amount, string Repc1_Number, string Repc1_Amount, string Repc2_Number, string Repc2_Amount, string Repc3_Number, string Repc3_Amount, string Web_Date, string Web_Time,string MobilePhone,string MobileOptin)
        {
            //RedBackLibraryTest obj = new RedBackLibraryTest();
            //return obj.KeyCodeLogin("406448", "4", "32168530", "22025");
            RedBackLibraryTest obj = new RedBackLibraryTest();
            string strPath = Server.MapPath(".");
            return obj.Order_New(OrderNumber, CustFirstName, CustLastName, CustAdd1, CustAdd2, CustAdd3, CustCity, CustState, CustZip, CustCountry, CustPhoneDay, CustPhoneEve, CustEmail1, CustEmail2,
                                    CustPin, ShipFirstName, ShipLastName, ShipAdd1, ShipAdd2, ShipAdd3, ShipCity, ShipState, ShipZip, ShipCountry,
                                    ShipPhone, ShipEmail, ShipAttention, CustNumber, WebReference, PromoCode, Title, ShipMethod, PaymentMethod,
                                    CreditCardNumber, CardExpDate, CardCVV, CardAddress, CardZip, MerchAmount, CouponAmount, DiscAmount, ShipAmount, PremShipAmount,
                                    OverweightAmount, TaxAmount, TotalAmount, Comments, Items, QtyOrdered, UnitPrice,
                                    ReleaseDate, FreeFlag, PersonalizationCode, PersonalizationDetail1, PersonalizationDetail2, PersonalizationDetail3, PersonalizationDetail4,
                                    PersonalizationDetail5, PIN, PINHint, OptInFlag, OptInDate, OptOutDate, Etrack, IPSource,
                                    GVCFlag, GVCDate, ValidErrCode, ValidErrMsg, OrderErrCode, OrderErrMsg, Cert1_Number, Cert1_Amount, Cert2_Number, Cert2_Amount, Cert3_Number,
                                    Cert3_Amount, Repc1_Number, Repc1_Amount, Repc2_Number, Repc2_Amount, Repc3_Number, Repc3_Amount, Web_Date, Web_Time, strPath,MobilePhone,MobileOptin);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Catalog_Request(string Title, string CustNumber, string CustFirstName, string CustLastName, string CustCompany, string CustAddr1, string CustAddr2, string CustCity, string CustState,
                                    string CustZip, string CustEmail, string CustEmIP, string CustPhone, string PromoCode, string ContestCode, string OptOutFlag, string CatErrCode,
                                    string CatErrMsg, string Bonus, string debug, string debug_id, string BirthMonth, string BirthDay, string ConfirmEmail, string IPaddress, string TimeStamp)
        {
            //RedBackLibraryTest obj = new RedBackLibraryTest();
            //return obj.KeyCodeLogin("406448", "4", "32168530", "22025");
            RedBackLibraryTest obj = new RedBackLibraryTest();
            string strPath = Server.MapPath(".");
            return obj.Catalog_Request(Title, CustNumber, CustFirstName, CustLastName, CustCompany, CustAddr1, CustAddr2, CustCity, CustState,
                                       CustZip, CustEmail, CustEmIP, CustPhone, PromoCode, ContestCode, OptOutFlag, CatErrCode,
                                       CatErrMsg, Bonus, debug, debug_id, BirthMonth, BirthDay, ConfirmEmail, IPaddress, TimeStamp, strPath);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Catalog_Request_OptOut(string Title, string CustNumber, string CustFirstName, string CustLastName, string CustCompany, string CustAddr1, string CustAddr2, string CustCity, string CustState,
                                    string CustZip, string CustEmail, string CustEmIP, string CustPhone, string PromoCode, string ContestCode, string OptOutFlag, string CatErrCode,
                                    string CatErrMsg, string Bonus, string debug, string debug_id, string BirthMonth, string BirthDay, string ConfirmEmail, string IPaddress, string TimeStamp)
        {
            //RedBackLibraryTest obj = new RedBackLibraryTest();
            //return obj.KeyCodeLogin("406448", "4", "32168530", "22025");
            RedBackLibraryTest obj = new RedBackLibraryTest();
            string strPath = Server.MapPath(".");
            return obj.Catalog_Request_OptOut(Title, CustNumber, CustFirstName, CustLastName, CustCompany, CustAddr1, CustAddr2, CustCity, CustState,
                                       CustZip, CustEmail, CustEmIP, CustPhone, PromoCode, ContestCode, OptOutFlag, CatErrCode,
                                       CatErrMsg, Bonus, debug, debug_id, BirthMonth, BirthDay, ConfirmEmail, IPaddress, TimeStamp, strPath);
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Order_Status(string CustNumber, string CustName, string CustAdd1, string CustAdd2, string CustState, string CustCity, string CustZip, string CustCountry,
                                   string CustPhone, string CustFax, string CustEmail, string CustPin, string Title, string SumOrderNumbers, string SumOrderDates, string SumShipNames,
                                   string SumShipStatus, string SumOrderTotal, string DetOrderNumber, string DetOrderDate, string DetOrderAmount, string DetShipName, string DetShipAdd1,
                                   string DetShipAdd2, string DetShipCity, string DetShipState, string DetShipZip, string DetShipNumber, string DetShipItems, string DetShipQty,
                                   string DetShipDesc, string DetShipVia, string DetShipDate, string DetShipTrackNums, string DetShipTrackLink,
                                   string DetShipEstDelBegin, string DetShipEstDelEnd, string DetOpenLocs, string DetOpenItems, string DetOpenQtys,
                                   string DetOpenDesc, string DetOpenEstDelBegin, string DetOpenEstDelEnd, string StatusErr, string StatusMsg)
        {
            //RedBackLibraryTest obj = new RedBackLibraryTest();
            //return obj.KeyCodeLogin("406448", "4", "32168530", "22025");
            RedBackLibraryTest obj = new RedBackLibraryTest();
            string strPath = Server.MapPath(".");
            return obj.Order_Status(CustNumber, CustName, CustAdd1, CustAdd2, CustState, CustCity, CustZip, CustCountry,
                                    CustPhone, CustFax, CustEmail, CustPin, Title, SumOrderNumbers, SumOrderDates, SumShipNames,
                                    SumShipStatus, SumOrderTotal, DetOrderNumber, DetOrderDate, DetOrderAmount, DetShipName, DetShipAdd1,
                                    DetShipAdd2, DetShipCity, DetShipState, DetShipZip, DetShipNumber, DetShipItems, DetShipQty,
                                    DetShipDesc, DetShipVia, DetShipDate, DetShipTrackNums, DetShipTrackLink,
                                    DetShipEstDelBegin, DetShipEstDelEnd, DetOpenLocs, DetOpenItems, DetOpenQtys,
                                    DetOpenDesc, DetOpenEstDelBegin, DetOpenEstDelEnd, StatusErr, StatusMsg, strPath);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Gift_Certificate(string Cert_Number, string Cert_Amount, string Cert_Status, string Cert_Err, string Cert_Errmsg)
        {
            RedBackLibraryTest obj = new RedBackLibraryTest();
            string strPath = Server.MapPath(".");
            return obj.GiftCertificate(Cert_Number, Cert_Amount, Cert_Status, Cert_Err, Cert_Errmsg, strPath);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Replace_Certificate(string Cert_Number, string Cert_Amount, string Cert_Status, string Cert_Err, string Cert_Errmsg)
        {
            RedBackLibraryTest obj = new RedBackLibraryTest();
            string strPath = Server.MapPath(".");
            return obj.Replacecertificate(Cert_Number, Cert_Amount, Cert_Status, Cert_Err, Cert_Errmsg, strPath);
        }


        public class CustomerInfo
        {
            public string Message = "";
            public string CustNumber = "";
            public string Title = "";
            public string CustName = "";
            public string CustAdd1 = "";
            public string CustAdd2 = "";
            public string CustState = "";
            public string CustCity = "";
            public string CustZip = "";
            public string CustCountry = "";
            public string CustPhone = "";
            public string CustFax = "";
            public string CustEmail = "";
            public string CustPin = "";
            public string CustClubFlag = "";
            public string CustClubDisc = "";
            public string CustClubDate = "";
            public string CustPinHint = "";
            public string CustOptInFlag = "";
            public string CustError = "";
            public string CustMessage = "";
            public string CustOrder = "";
            public string CustFirstName = "";
            public string CustLastName = "";
            public string ClubNumber = "";

        }

        public class KeyCodeLogin
        {
            public string Message = "";
            public string Keycode = "";
            public string Title = "";
            public string Catalog = "";
            public string CustNumber = "";
            public string CustName = "";
            public string CustFirstName = "";
            public string CustLastName = "";
            public string CustCompany = "";
            public string CustAdd1 = "";
            public string CustAdd2 = "";
            public string CustCity = "";
            public string CustState = "";
            public string CustZip = "";
            public string CustPhone = "";
            public string CustEmail = "";
            public string CustClubFlag = "";
            public string CustClubDisc = "";
            public string CustClubDate = "";
            public string CustError = "";
            public string CustMessage = "";
            public string debug_id = "";
        }

        public class OrderNew
        {
            public string Message = "";
            public string OrderNumber = "";
            public string CustFirstName = "";
            public string CustLastName = "";
            public string CustAdd1 = "";
            public string CustAdd2 = "";
            public string CustAdd3 = "";
            public string CustCity = "";
            public string CustState = "";
            public string CustZip = "";
            public string CustCountry = "";
            public string CustPhoneDay = "";
            public string CustPhoneEve = "";
            public string CustEmail1 = "";
            public string CustEmail2 = "";
            public string CustPin = "";
            public string ShipFirstName = "";
            public string ShipLastName = "";
            public string ShipAdd1 = "";
            public string ShipAdd2 = "";
            public string ShipAdd3 = "";
            public string ShipCity = "";
            public string ShipState = "";
            public string ShipZip = "";
            public string ShipCountry = "";
            public string ShipPhone = "";
            public string ShipEmail = "";
            public string ShipAttention = "";
            public string CustNumber = "";
            public string WebReference = "";
            public string PromoCode = "";
            public string Title = "";
            public string ShipMethod = "";
            public string PaymentMethod = "";
            public string CreditCardNumber = "";
            public string CardExpDate = "";
            public string CardCVV = "";
            public string CardAddress = "";
            public string CardZip = "";
            public string MerchAmount = "";
            public string CouponAmount = "";
            public string DiscAmount = "";
            public string ShipAmount = "";
            public string PremShipAmount = "";
            public string OverweightAmount = "";
            public string TaxAmount = "";
            public string TotalAmount = "";
            public string Comments = "";
            public string Items = "";
            public string QtyOrdered = "";
            public string UnitPrice = "";
            public string ReleaseDate = "";
            public string FreeFlag = "";
            public string PersonalizationCode = "";
            public string PersonalizationDetail1 = "";
            public string PersonalizationDetail2 = "";
            public string PersonalizationDetail3 = "";
            public string PersonalizationDetail4 = "";
            public string PersonalizationDetail5 = "";
            public string PIN = "";
            public string PINHint = "";
            public string OptInFlag = "";
            public string OptInDate = "";
            public string OptOutDate = "";
            public string Etrack = "";
            public string IPSource = "";
            public string GVCFlag = "";
            public string GVCDate = "";
            public string ValidErrCode = "";
            public string ValidErrMsg = "";
            public string OrderErrCode = "";
            public string OrderErrMsg = "";
            public string Cert1_Number = "";
            public string Cert1_Amount = "";
            public string Cert2_Number = "";
            public string Cert2_Amount = "";
            public string Cert3_Number = "";
            public string Cert3_Amount = "";
            public string Repc1_Number = "";
            public string Repc1_Amount = "";
            public string Repc2_Number = "";
            public string Repc2_Amount = "";
            public string Repc3_Number = "";
            public string Repc3_Amount = "";
            public string Web_Date = "";
            public string Web_Time = "";
            public string MobilePhone = "";  //added by aditya 15-nov-2012
            public string MobileOptin = "";
        }

        public class CatalogRequest
        {
            public string Message = "";
            public string Title = "";
            public string CustNumber = "";
            public string CustFirstName = "";
            public string CustLastName = "";
            public string CustCompany = "";
            public string CustAddr1 = "";
            public string CustAddr2 = "";
            public string CustCity = "";
            public string CustState = "";
            public string CustZip = "";
            public string CustEmail = "";
            public string CustEmIP = "";
            public string CustPhone = "";
            public string PromoCode = "";
            public string ContestCode = "";
            public string OptOutFlag = "";
            public string CatErrCode = "";
            public string CatErrMsg = "";
            public string Bonus = "";
            public string debug = "";
            public string debug_id = "";
            public string BirthMonth = "";
            public string BirthDay = "";
            public string ConfirmEmail = "";
            public string IPaddress = "";
            public string TimeStamp = "";
        }

        public class OrderStatus
        {
            public string Message = "";
            public string CustNumber = "";
            public string CustName = "";
            public string CustAdd1 = "";
            public string CustAdd2 = "";
            public string CustState = "";
            public string CustCity = "";
            public string CustZip = "";
            public string CustCountry = "";
            public string CustPhone = "";
            public string CustFax = "";
            public string CustEmail = "";
            public string CustPin = "";
            public string Title = "";
            public string SumOrderNumbers = "";
            public string SumOrderDates = "";
            public string SumShipNames = "";
            public string SumShipStatus = "";
            public string SumOrderTotal = "";
            public string DetOrderNumber = "";
            public string DetOrderDate = "";
            public string DetOrderAmount = "";
            public string DetShipName = "";
            public string DetShipAdd1 = "";
            public string DetShipAdd2 = "";
            public string DetShipCity = "";
            public string DetShipState = "";
            public string DetShipZip = "";
            public string DetShipNumber = "";
            public string DetShipItems = "";
            public string DetShipQty = "";
            public string DetShipDesc = "";
            public string DetShipVia = "";
            public string DetShipDate = "";
            public string DetShipTrackNums = "";
            public string DetShipTrackLink = "";
            public string DetShipEstDelBegin = "";
            public string DetShipEstDelEnd = "";
            public string DetOpenLocs = "";
            public string DetOpenItems = "";
            public string DetOpenQtys = "";
            public string DetOpenDesc = "";
            public string DetOpenEstDelBegin = "";
            public string DetOpenEstDelEnd = "";
            public string StatusErr = "";
            public string StatusMsg = "";
        }

        public class GiftCertificate
        {
            public string Message = "";
            public string Cert_Number = "";
            public string Cert_Amount = "";
            public string Cert_Status = "";
            public string Cert_Err = "";
            public string Cert_Errmsg = "";
        }

        public class RedBackLibraryTest
        {
            private const string RedBackAccount = "172.16.1.15:8402";

            public RedBackLibraryTest()
            {
            }

            public DataTable CreateCustomerTable()
            {
                DataTable dtCustInfo = new DataTable();

                dtCustInfo.Columns.Add("Message");
                dtCustInfo.Columns.Add("CustNumber");
                dtCustInfo.Columns.Add(" Title");
                dtCustInfo.Columns.Add(" CustName");
                dtCustInfo.Columns.Add(" CustAdd1");
                dtCustInfo.Columns.Add(" CustAdd2");
                dtCustInfo.Columns.Add(" CustState");
                dtCustInfo.Columns.Add(" CustCity");
                dtCustInfo.Columns.Add(" CustZip");
                dtCustInfo.Columns.Add(" CustCountry");
                dtCustInfo.Columns.Add(" CustPhone");
                dtCustInfo.Columns.Add(" CustFax");
                dtCustInfo.Columns.Add(" CustEmail");
                dtCustInfo.Columns.Add(" CustPin");
                dtCustInfo.Columns.Add(" CustClubFlag");
                dtCustInfo.Columns.Add(" CustClubDisc");
                dtCustInfo.Columns.Add(" CustClubDate");
                dtCustInfo.Columns.Add(" CustPinHint");
                dtCustInfo.Columns.Add(" CustOptInFlag");
                dtCustInfo.Columns.Add(" CustError");
                dtCustInfo.Columns.Add(" CustMessage");
                dtCustInfo.Columns.Add(" CustOrder");
                dtCustInfo.Columns.Add(" CustFirstName");
                dtCustInfo.Columns.Add(" CustLastName");
                dtCustInfo.Columns.Add("ClubNumber");
                return dtCustInfo;
            }

            public DataTable CreateKeyCodeLoginTable()
            {
                DataTable dtCustInfo = new DataTable();

                dtCustInfo.Columns.Add("Message");
                dtCustInfo.Columns.Add("Keycode");
                dtCustInfo.Columns.Add("Title");
                dtCustInfo.Columns.Add("Catalog");
                dtCustInfo.Columns.Add("CustNumber");
                dtCustInfo.Columns.Add("CustName");
                dtCustInfo.Columns.Add("CustFirstName");
                dtCustInfo.Columns.Add("CustLastName");
                dtCustInfo.Columns.Add("CustCompany");
                dtCustInfo.Columns.Add("CustAdd1");
                dtCustInfo.Columns.Add("CustAdd2");
                dtCustInfo.Columns.Add("CustCity");
                dtCustInfo.Columns.Add("CustState");
                dtCustInfo.Columns.Add("CustZip");
                dtCustInfo.Columns.Add("CustPhone");
                dtCustInfo.Columns.Add("CustEmail");
                dtCustInfo.Columns.Add("CustClubFlag");
                dtCustInfo.Columns.Add("CustClubDisc");
                dtCustInfo.Columns.Add("CustClubDate");
                dtCustInfo.Columns.Add("CustError");
                dtCustInfo.Columns.Add("CustMessage");
                dtCustInfo.Columns.Add("debug_id");

                return dtCustInfo;
            }

            public DataTable CreateOrderNewTable()
            {
                DataTable dtCustInfo = new DataTable();

                dtCustInfo.Columns.Add("Message");
                dtCustInfo.Columns.Add("OrderNumber");
                dtCustInfo.Columns.Add("CustFirstName");
                dtCustInfo.Columns.Add("CustLastName");
                dtCustInfo.Columns.Add("CustAdd1");
                dtCustInfo.Columns.Add("CustAdd2");
                dtCustInfo.Columns.Add("CustAdd3");
                dtCustInfo.Columns.Add("CustCity");
                dtCustInfo.Columns.Add("CustState");
                dtCustInfo.Columns.Add("CustZip");
                dtCustInfo.Columns.Add("CustCountry");
                dtCustInfo.Columns.Add("CustPhoneDay");
                dtCustInfo.Columns.Add("CustPhoneEve");
                dtCustInfo.Columns.Add("CustEmail1");
                dtCustInfo.Columns.Add("CustEmail2");
                dtCustInfo.Columns.Add("CustPin");
                dtCustInfo.Columns.Add("ShipFirstName");
                dtCustInfo.Columns.Add("ShipLastName");
                dtCustInfo.Columns.Add("ShipAdd1");
                dtCustInfo.Columns.Add("ShipAdd2");
                dtCustInfo.Columns.Add("ShipAdd3");
                dtCustInfo.Columns.Add("ShipCity");
                dtCustInfo.Columns.Add("ShipState");
                dtCustInfo.Columns.Add("ShipZip");
                dtCustInfo.Columns.Add("ShipCountry");

                dtCustInfo.Columns.Add("ShipPhone");
                dtCustInfo.Columns.Add("ShipEmail");
                dtCustInfo.Columns.Add("ShipAttention");
                dtCustInfo.Columns.Add("CustNumber");
                dtCustInfo.Columns.Add("WebReference");
                dtCustInfo.Columns.Add("PromoCode");
                dtCustInfo.Columns.Add("Title");
                dtCustInfo.Columns.Add("ShipMethod");
                dtCustInfo.Columns.Add("PaymentMethod");
                dtCustInfo.Columns.Add("CreditCardNumber");
                dtCustInfo.Columns.Add("CardExpDate");
                dtCustInfo.Columns.Add("CardCVV");
                dtCustInfo.Columns.Add("CardAddress");
                dtCustInfo.Columns.Add("CardZip");
                dtCustInfo.Columns.Add("MerchAmount");
                dtCustInfo.Columns.Add("CouponAmount");
                dtCustInfo.Columns.Add("DiscAmount");
                dtCustInfo.Columns.Add("ShipAmount");
                dtCustInfo.Columns.Add("PremShipAmount");

                dtCustInfo.Columns.Add("OverweightAmount");
                dtCustInfo.Columns.Add("TaxAmount");
                dtCustInfo.Columns.Add("TotalAmount");
                dtCustInfo.Columns.Add("Comments");
                dtCustInfo.Columns.Add("Items");
                dtCustInfo.Columns.Add("QtyOrdered");
                dtCustInfo.Columns.Add("UnitPrice");
                dtCustInfo.Columns.Add("ReleaseDate");
                dtCustInfo.Columns.Add("FreeFlag");
                dtCustInfo.Columns.Add("PersonalizationCode");
                dtCustInfo.Columns.Add("PersonalizationDetail1");
                dtCustInfo.Columns.Add("PersonalizationDetail2");
                dtCustInfo.Columns.Add("PersonalizationDetail3");
                dtCustInfo.Columns.Add("PersonalizationDetail4");
                dtCustInfo.Columns.Add("PersonalizationDetail5");

                dtCustInfo.Columns.Add("PIN");
                dtCustInfo.Columns.Add("PINHint");
                dtCustInfo.Columns.Add("OptInFlag");
                dtCustInfo.Columns.Add("OptInDate");
                dtCustInfo.Columns.Add("OptOutDate");
                dtCustInfo.Columns.Add("Etrack");
                dtCustInfo.Columns.Add("IPSource");
                dtCustInfo.Columns.Add("GVCFlag");
                dtCustInfo.Columns.Add("GVCDate");
                dtCustInfo.Columns.Add("ValidErrCode");

                dtCustInfo.Columns.Add("ValidErrMsg");
                dtCustInfo.Columns.Add("OrderErrCode");
                dtCustInfo.Columns.Add("OrderErrMsg");
                dtCustInfo.Columns.Add("Cert1_Number");
                dtCustInfo.Columns.Add("Cert1_Amount");
                dtCustInfo.Columns.Add("Cert2_Number");
                dtCustInfo.Columns.Add("Cert2_Amount");
                dtCustInfo.Columns.Add("Cert3_Number");
                dtCustInfo.Columns.Add("Cert3_Amount");
                dtCustInfo.Columns.Add("Repc1_Number");
                dtCustInfo.Columns.Add("Repc1_Amount");
                dtCustInfo.Columns.Add("Repc2_Number");
                dtCustInfo.Columns.Add("Repc2_Amount");
                dtCustInfo.Columns.Add("Repc3_Number");
                dtCustInfo.Columns.Add("Repc3_Amount");
                dtCustInfo.Columns.Add("Web_Date");
                dtCustInfo.Columns.Add("Web_Time");
                dtCustInfo.Columns.Add("MobilePhone");  //added by aditya on 15-nov-2012
                dtCustInfo.Columns.Add("MobileOptin");
                



                return dtCustInfo;
            }

            public DataTable CreateCatalogRequestTable()
            {
                DataTable dtCustInfo = new DataTable();

                dtCustInfo.Columns.Add("Message");
                dtCustInfo.Columns.Add("Title ");
                dtCustInfo.Columns.Add("CustNumber");
                dtCustInfo.Columns.Add("CustFirstName");
                dtCustInfo.Columns.Add("CustLastName");
                dtCustInfo.Columns.Add("CustCompany");
                dtCustInfo.Columns.Add("CustAddr1");
                dtCustInfo.Columns.Add("CustAddr2");
                dtCustInfo.Columns.Add("CustCity");
                dtCustInfo.Columns.Add("CustState");
                dtCustInfo.Columns.Add("CustZip");
                dtCustInfo.Columns.Add("CustEmail");
                dtCustInfo.Columns.Add("CustEmIP");
                dtCustInfo.Columns.Add("CustPhone");
                dtCustInfo.Columns.Add("PromoCode");
                dtCustInfo.Columns.Add("ContestCode");
                dtCustInfo.Columns.Add("OptOutFlag");
                dtCustInfo.Columns.Add("CatErrCode");
                dtCustInfo.Columns.Add("CatErrMsg");
                dtCustInfo.Columns.Add("Bonus");
                dtCustInfo.Columns.Add("debug");
                dtCustInfo.Columns.Add("debug_id");
                dtCustInfo.Columns.Add("BirthMonth");
                dtCustInfo.Columns.Add("BirthDay");
                dtCustInfo.Columns.Add("ConfirmEmail");
                dtCustInfo.Columns.Add("IPaddress");
                dtCustInfo.Columns.Add("TimeStamp");
                return dtCustInfo;
            }

            public DataTable CreateOrderStatusTable()
            {
                DataTable dtCustInfo = new DataTable();

                dtCustInfo.Columns.Add("Message");
                dtCustInfo.Columns.Add("CustNumber ");
                dtCustInfo.Columns.Add("CustName");
                dtCustInfo.Columns.Add("CustAdd1");
                dtCustInfo.Columns.Add("CustAdd2");
                dtCustInfo.Columns.Add("CustState");
                dtCustInfo.Columns.Add("CustCity");
                dtCustInfo.Columns.Add("CustZip");
                dtCustInfo.Columns.Add("CustCountry");
                dtCustInfo.Columns.Add("CustPhone");
                dtCustInfo.Columns.Add("CustFax");
                dtCustInfo.Columns.Add("CustEmail");
                dtCustInfo.Columns.Add("CustPin");
                dtCustInfo.Columns.Add("Title");
                dtCustInfo.Columns.Add("SumOrderNumbers");
                dtCustInfo.Columns.Add("SumOrderDates");
                dtCustInfo.Columns.Add("SumShipNames");
                dtCustInfo.Columns.Add("SumShipStatus");
                dtCustInfo.Columns.Add("SumOrderTotal");
                dtCustInfo.Columns.Add("DetOrderNumber");
                dtCustInfo.Columns.Add("DetOrderDate");
                dtCustInfo.Columns.Add("DetOrderAmount");
                dtCustInfo.Columns.Add("DetShipName");
                dtCustInfo.Columns.Add("DetShipAdd1");
                dtCustInfo.Columns.Add("DetShipAdd2");
                dtCustInfo.Columns.Add("DetShipCity");
                dtCustInfo.Columns.Add("DetShipState");

                dtCustInfo.Columns.Add("DetShipZip");
                dtCustInfo.Columns.Add("DetShipNumber");
                dtCustInfo.Columns.Add("DetShipItems");
                dtCustInfo.Columns.Add("DetShipQty");
                dtCustInfo.Columns.Add("DetShipDesc");
                dtCustInfo.Columns.Add("DetShipVia");
                dtCustInfo.Columns.Add("DetShipDate");
                dtCustInfo.Columns.Add("DetShipTrackNums");
                dtCustInfo.Columns.Add("DetShipTrackLink");
                dtCustInfo.Columns.Add("DetShipEstDelBegin");
                dtCustInfo.Columns.Add("DetShipEstDelEnd");
                dtCustInfo.Columns.Add("DetOpenLocs");
                dtCustInfo.Columns.Add("DetOpenItems");
                dtCustInfo.Columns.Add("DetOpenQtys");
                dtCustInfo.Columns.Add("DetOpenDesc");
                dtCustInfo.Columns.Add("DetOpenEstDelBegin");
                dtCustInfo.Columns.Add("DetOpenEstDelEnd");
                dtCustInfo.Columns.Add("StatusErr");
                dtCustInfo.Columns.Add("StatusMsg");

                return dtCustInfo;
            }

            public DataTable CreateGiftCertificateTable()
            {
                DataTable dtCustInfo = new DataTable();

                dtCustInfo.Columns.Add("Message");
                dtCustInfo.Columns.Add("Cert_Number");
                dtCustInfo.Columns.Add("Cert_Amount");
                dtCustInfo.Columns.Add("Cert_Status");
                dtCustInfo.Columns.Add("Cert_Err");
                dtCustInfo.Columns.Add("Cert_Errmsg");
                return dtCustInfo;
            }

            public string KeyCodeLogin(string Keycode, string Title, string Catalog, string CustNumber, string CustName, string CustFirstName, string CustLastName, string CustCompany, string CustAdd1, string CustAdd2, string CustCity, string CustState, string CustZip, string CustPhone, string CustEmail, string CustClubFlag, string CustClubDisc, string CustClubDate, string CustError, string CustMessage, string debug_id, string Path)
            {

                StringBuilder rsp = new StringBuilder();
                StringBuilder InputParam = new StringBuilder();
                StringBuilder OutPutParam = new StringBuilder();
                InputParam = KeyCodeLoginGetData(Keycode, Title, Catalog, CustNumber, CustName, CustFirstName, CustLastName, CustCompany, CustAdd1, CustAdd2, CustCity, CustState, CustZip, CustPhone, CustEmail, CustClubFlag, CustClubDisc, CustClubDate, CustError, CustMessage, debug_id, Path);
               
                WriteServiceLog("Method GetPost Start" + DateTime.Now, Path);                
                JavaScriptSerializer obj = new JavaScriptSerializer();
                DataTable CustInfo = CreateKeyCodeLoginTable();
                string[] ArryCustInfo = new string[CustInfo.Columns.Count];
                for (int dataIndex = 0; dataIndex < CustInfo.Columns.Count; dataIndex++)
                {
                    ArryCustInfo[dataIndex] = "";
                }
                RedBackCallsService.Service1.KeyCodeLogin OutCutomerInfo = new RedBackCallsService.Service1.KeyCodeLogin();
                //if ((Keycode != ""))
                //{
                    RedObject rb = new RedObject();
                    rb.Open3(RedBackAccount, "OPM:KeycodeLogin");
                    ((RedProperty)rb.Property("Keycode")).Value = Keycode;
                    ((RedProperty)rb.Property("Title")).Value = Title;
                    ((RedProperty)rb.Property("Catalog")).Value = Catalog;
                    ((RedProperty)rb.Property("CustNumber")).Value = CustNumber;
                    ((RedProperty)rb.Property("CustName")).Value = CustName;
                    ((RedProperty)rb.Property("CustFirstName")).Value = CustFirstName;
                    ((RedProperty)rb.Property("CustLastName")).Value = CustLastName;
                    ((RedProperty)rb.Property("CustCompany")).Value = CustCompany;
                    ((RedProperty)rb.Property("CustAdd1")).Value = CustAdd1;
                    ((RedProperty)rb.Property("CustAdd2")).Value = CustAdd2;
                    ((RedProperty)rb.Property("CustCity")).Value = CustCity;
                    ((RedProperty)rb.Property("CustState")).Value = CustState;
                    ((RedProperty)rb.Property("CustZip")).Value = CustZip;
                    ((RedProperty)rb.Property("CustPhone")).Value = CustPhone;
                    ((RedProperty)rb.Property("CustEmail")).Value = CustEmail;
                    ((RedProperty)rb.Property("CustClubFlag")).Value = CustClubFlag;
                    ((RedProperty)rb.Property("CustClubDisc")).Value = CustClubDisc;
                    ((RedProperty)rb.Property("CustClubDate")).Value = CustClubDate;
                    ((RedProperty)rb.Property("CustError")).Value = CustError;
                    ((RedProperty)rb.Property("CustMessage")).Value = CustMessage;
                    ((RedProperty)rb.Property("debug_id")).Value = debug_id;

                    rb.CallMethod("KeycodeLogin");
                    string custMsg = "Success";
                    string retu = ((RedProperty)rb.Property("CustError")).Value;
                    if (((RedProperty)rb.Property("CustError")).Value != "")
                        custMsg = ((RedProperty)rb.Property("CustMessage")).Value;

                    OutCutomerInfo.Message = custMsg;
                    OutCutomerInfo.Keycode = ((RedProperty)rb.Property("Keycode")).Value.ToString();
                    OutCutomerInfo.Title = ((RedProperty)rb.Property("Title")).Value.ToString();
                    OutCutomerInfo.Catalog = ((RedProperty)rb.Property("Catalog")).Value.ToString();
                    OutCutomerInfo.CustNumber = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    OutCutomerInfo.CustName = ((RedProperty)rb.Property("CustName")).Value.ToString();
                    OutCutomerInfo.CustFirstName = ((RedProperty)rb.Property("CustFirstName")).Value.ToString();
                    OutCutomerInfo.CustLastName = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    OutCutomerInfo.CustCompany = ((RedProperty)rb.Property("CustCompany")).Value.ToString();
                    OutCutomerInfo.CustAdd1 = ((RedProperty)rb.Property("CustAdd1")).Value.ToString();
                    OutCutomerInfo.CustAdd2 = ((RedProperty)rb.Property("CustAdd2")).Value.ToString();
                    OutCutomerInfo.CustCity = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    OutCutomerInfo.CustState = ((RedProperty)rb.Property("CustState")).Value.ToString();
                    OutCutomerInfo.CustZip = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    OutCutomerInfo.CustPhone = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    OutCutomerInfo.CustEmail = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    OutCutomerInfo.CustClubFlag = ((RedProperty)rb.Property("CustClubFlag")).Value.ToString();
                    OutCutomerInfo.CustClubDisc = ((RedProperty)rb.Property("CustClubDisc")).Value.ToString();
                    OutCutomerInfo.CustClubDate = ((RedProperty)rb.Property("CustClubDate")).Value.ToString();
                    OutCutomerInfo.CustError = ((RedProperty)rb.Property("CustError")).Value.ToString();
                    OutCutomerInfo.CustMessage = ((RedProperty)rb.Property("CustMessage")).Value.ToString();
                    OutCutomerInfo.debug_id = ((RedProperty)rb.Property("debug_id")).Value.ToString();


                    ArryCustInfo[1] = ((RedProperty)rb.Property("Keycode")).Value.ToString();
                    ArryCustInfo[2] = ((RedProperty)rb.Property("Title")).Value.ToString();
                    ArryCustInfo[3] = ((RedProperty)rb.Property("Catalog")).Value.ToString();
                    ArryCustInfo[4] = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    ArryCustInfo[5] = ((RedProperty)rb.Property("CustName")).Value.ToString();
                    ArryCustInfo[6] = ((RedProperty)rb.Property("CustFirstName")).Value.ToString();
                    ArryCustInfo[7] = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    ArryCustInfo[8] = ((RedProperty)rb.Property("CustCompany")).Value.ToString();
                    ArryCustInfo[9] = ((RedProperty)rb.Property("CustAdd1")).Value.ToString();
                    ArryCustInfo[10] = ((RedProperty)rb.Property("CustAdd2")).Value.ToString();
                    ArryCustInfo[11] = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    ArryCustInfo[12] = ((RedProperty)rb.Property("CustState")).Value.ToString();
                    ArryCustInfo[13] = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    ArryCustInfo[14] = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    ArryCustInfo[15] = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    ArryCustInfo[16] = ((RedProperty)rb.Property("CustClubFlag")).Value.ToString();
                    ArryCustInfo[17] = ((RedProperty)rb.Property("CustClubDisc")).Value.ToString();
                    ArryCustInfo[18] = ((RedProperty)rb.Property("CustClubDate")).Value.ToString();
                    ArryCustInfo[19] = ((RedProperty)rb.Property("CustError")).Value.ToString();
                    ArryCustInfo[20] = ((RedProperty)rb.Property("CustMessage")).Value.ToString();
                    ArryCustInfo[21] = ((RedProperty)rb.Property("debug_id")).Value.ToString();

                //}
                CustInfo.Rows.Add(ArryCustInfo);
                OutPutParam = KeyCodeLoginGetData(ArryCustInfo);
                string LogOn = RedBackServiceGetLogOnDetails(Path);
                StringBuilder MethodName = new StringBuilder();
                if (LogOn == "1")
                {
                    MethodName.AppendLine(" ");
                    MethodName.AppendLine("Method Name : KeyCodeLogin");
                    MethodName.AppendLine(" ");
                    InputParam.AppendLine(" ");
                    InputParam.AppendLine("OutPut Details");
                    InputParam.AppendLine(" ");
                    string Log = MethodName.ToString() + InputParam.ToString();
                    Log = Log + OutPutParam.ToString();
                    WriteServiceLog(Log, Path);
                    //WriteServiceLog("Method GetPost End" + DateTime.Now, Path);
                    WriteServiceLog("Method GetPost End", Path);
                }
                return obj.Serialize(OutCutomerInfo);
            }

            public string Catalog_Request(string Title, string CustNumber, string CustFirstName, string CustLastName, string CustCompany, string CustAddr1, string CustAddr2, string CustCity, string CustState,
                                    string CustZip, string CustEmail, string CustEmIP, string CustPhone, string PromoCode, string ContestCode, string OptOutFlag, string CatErrCode,
                                    string CatErrMsg, string Bonus, string debug, string debug_id, string BirthMonth, string BirthDay, string ConfirmEmail, string IPaddress, string TimeStamp, string Path)
            {
                StringBuilder rsp = new StringBuilder();
                StringBuilder InputParam = new StringBuilder();
                StringBuilder OutPutParam = new StringBuilder();
                InputParam = CatalogRequestGetData(Title, CustNumber, CustFirstName, CustLastName, CustCompany, CustAddr1, CustAddr2, CustCity, CustState,
                                       CustZip, CustEmail, CustEmIP, CustPhone, PromoCode, ContestCode, OptOutFlag, CatErrCode,
                                       CatErrMsg, Bonus, debug, debug_id, BirthMonth, BirthDay, ConfirmEmail, IPaddress, TimeStamp, Path);
                WriteServiceLog("Method GetPost Start" + DateTime.Now, Path);
                
                JavaScriptSerializer obj = new JavaScriptSerializer();
                DataTable CustInfo = CreateCatalogRequestTable();
                string[] ArryCustInfo = new string[CustInfo.Columns.Count];
                for (int dataIndex = 0; dataIndex < CustInfo.Columns.Count; dataIndex++)
                {
                    ArryCustInfo[dataIndex] = "";
                }
                RedBackCallsService.Service1.CatalogRequest OutCutomerInfo = new RedBackCallsService.Service1.CatalogRequest();
                //if ((Title != ""))// && (custZip != ""))
                //{
                    RedObject rb = new RedObject();
                    rb.Open3(RedBackAccount, "OPM:Catalog_Request");
                    ((RedProperty)rb.Property("Title")).Value = Title;
                    ((RedProperty)rb.Property("CustNumber")).Value = CustNumber;
                    ((RedProperty)rb.Property("CustFirstName")).Value = CustFirstName;
                    ((RedProperty)rb.Property("CustLastName")).Value = CustLastName;
                    ((RedProperty)rb.Property("CustCompany")).Value = CustCompany;
                    ((RedProperty)rb.Property("CustAddr1")).Value = CustAddr1;
                    ((RedProperty)rb.Property("CustAddr2")).Value = CustAddr2;
                    ((RedProperty)rb.Property("CustCity")).Value = CustCity;
                    ((RedProperty)rb.Property("CustState")).Value = CustState;
                    ((RedProperty)rb.Property("CustZip")).Value = CustZip;
                    ((RedProperty)rb.Property("CustEmail")).Value = CustEmail;
                    ((RedProperty)rb.Property("CustEmIP")).Value = CustEmIP;
                    ((RedProperty)rb.Property("CustPhone")).Value = CustPhone;
                    ((RedProperty)rb.Property("PromoCode")).Value = PromoCode;
                    ((RedProperty)rb.Property("ContestCode")).Value = ContestCode;
                    ((RedProperty)rb.Property("OptOutFlag")).Value = OptOutFlag;
                    ((RedProperty)rb.Property("CatErrCode")).Value = CatErrCode;
                    ((RedProperty)rb.Property("CatErrMsg")).Value = CatErrMsg;
                    ((RedProperty)rb.Property("Bonus")).Value = Bonus;
                    ((RedProperty)rb.Property("debug")).Value = debug;
                    ((RedProperty)rb.Property("debug_id")).Value = debug_id;
                    ((RedProperty)rb.Property("BirthMonth")).Value = BirthMonth;
                    ((RedProperty)rb.Property("BirthDay")).Value = BirthDay;
                    ((RedProperty)rb.Property("ConfirmEmail")).Value = ConfirmEmail;
                    ((RedProperty)rb.Property("IPaddress")).Value = IPaddress;
                    ((RedProperty)rb.Property("TimeStamp")).Value = TimeStamp;
                    rb.CallMethod("CatRequest");
                    //rb.CallMethod("OptOutRequest");
                    string custMsg = "Success";
                    string retu = ((RedProperty)rb.Property("CustError")).Value;
                    if (((RedProperty)rb.Property("CustError")).Value != "")
                        custMsg = ((RedProperty)rb.Property("CustMessage")).Value;

                    OutCutomerInfo.Message = custMsg;
                    OutCutomerInfo.Title = ((RedProperty)rb.Property("Title")).Value.ToString();
                    OutCutomerInfo.CustNumber = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    OutCutomerInfo.CustFirstName = ((RedProperty)rb.Property("CustFirstName")).Value.ToString();
                    OutCutomerInfo.CustLastName = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    OutCutomerInfo.CustCompany = ((RedProperty)rb.Property("CustCompany")).Value.ToString();
                    OutCutomerInfo.CustAddr1 = ((RedProperty)rb.Property("CustAddr1")).Value.ToString();
                    OutCutomerInfo.CustAddr2 = ((RedProperty)rb.Property("CustAddr2")).Value.ToString();
                    OutCutomerInfo.CustCity = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    OutCutomerInfo.CustState = ((RedProperty)rb.Property("CustState")).Value.ToString();
                    OutCutomerInfo.CustZip = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    OutCutomerInfo.CustEmail = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    OutCutomerInfo.CustEmIP = ((RedProperty)rb.Property("CustEmIP")).Value.ToString();
                    OutCutomerInfo.CustPhone = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    OutCutomerInfo.PromoCode = ((RedProperty)rb.Property("PromoCode")).Value.ToString();
                    OutCutomerInfo.ContestCode = ((RedProperty)rb.Property("ContestCode")).Value.ToString();
                    OutCutomerInfo.OptOutFlag = ((RedProperty)rb.Property("OptOutFlag")).Value.ToString();
                    OutCutomerInfo.CatErrCode = ((RedProperty)rb.Property("CatErrCode")).Value.ToString();
                    OutCutomerInfo.CatErrMsg = ((RedProperty)rb.Property("CatErrMsg")).Value.ToString();
                    OutCutomerInfo.Bonus = ((RedProperty)rb.Property("Bonus")).Value.ToString();
                    OutCutomerInfo.debug = ((RedProperty)rb.Property("debug")).Value.ToString();
                    OutCutomerInfo.debug_id = ((RedProperty)rb.Property("debug_id")).Value.ToString();
                    OutCutomerInfo.BirthMonth = ((RedProperty)rb.Property("BirthMonth")).Value.ToString();
                    OutCutomerInfo.BirthDay = ((RedProperty)rb.Property("BirthDay")).Value.ToString();
                    OutCutomerInfo.ConfirmEmail = ((RedProperty)rb.Property("ConfirmEmail")).Value.ToString();
                    OutCutomerInfo.IPaddress = ((RedProperty)rb.Property("IPaddress")).Value.ToString();
                    OutCutomerInfo.TimeStamp = ((RedProperty)rb.Property("TimeStamp")).Value.ToString();

                    ArryCustInfo[1] = ((RedProperty)rb.Property("Title")).Value.ToString();
                    ArryCustInfo[2] = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    ArryCustInfo[3] = ((RedProperty)rb.Property("CustFirstName")).Value.ToString();
                    ArryCustInfo[4] = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    ArryCustInfo[5] = ((RedProperty)rb.Property("CustCompany")).Value.ToString();
                    ArryCustInfo[6] = ((RedProperty)rb.Property("CustAddr1")).Value.ToString();
                    ArryCustInfo[7] = ((RedProperty)rb.Property("CustAddr2")).Value.ToString();
                    ArryCustInfo[8] = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    ArryCustInfo[9] = ((RedProperty)rb.Property("CustState")).Value.ToString();
                    ArryCustInfo[10] = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    ArryCustInfo[11] = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    ArryCustInfo[12] = ((RedProperty)rb.Property("CustEmIP")).Value.ToString();
                    ArryCustInfo[13] = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    ArryCustInfo[14] = ((RedProperty)rb.Property("PromoCode")).Value.ToString();
                    ArryCustInfo[15] = ((RedProperty)rb.Property("ContestCode")).Value.ToString();
                    ArryCustInfo[16] = ((RedProperty)rb.Property("OptOutFlag")).Value.ToString();
                    ArryCustInfo[17] = ((RedProperty)rb.Property("CatErrCode")).Value.ToString();
                    ArryCustInfo[18] = ((RedProperty)rb.Property("CatErrMsg")).Value.ToString();
                    ArryCustInfo[19] = ((RedProperty)rb.Property("Bonus")).Value.ToString();
                    ArryCustInfo[20] = ((RedProperty)rb.Property("debug")).Value.ToString();
                    ArryCustInfo[21] = ((RedProperty)rb.Property("debug_id")).Value.ToString();
                    ArryCustInfo[22] = ((RedProperty)rb.Property("BirthMonth")).Value.ToString();
                    ArryCustInfo[23] = ((RedProperty)rb.Property("BirthDay")).Value.ToString();
                    ArryCustInfo[24] = ((RedProperty)rb.Property("ConfirmEmail")).Value.ToString();
                    ArryCustInfo[25] = ((RedProperty)rb.Property("IPaddress")).Value.ToString();
                    ArryCustInfo[26] = ((RedProperty)rb.Property("TimeStamp")).Value.ToString();
                //}
                CustInfo.Rows.Add(ArryCustInfo);
                OutPutParam = CatalogRequestGetData(ArryCustInfo);
                string LogOn = RedBackServiceGetLogOnDetails(Path);
                StringBuilder MethodName = new StringBuilder();
                if (LogOn == "1")
                {
                    MethodName.AppendLine(" ");
                    MethodName.AppendLine("Method Name : CatRequest");
                    MethodName.AppendLine(" ");
                    InputParam.AppendLine(" ");
                    InputParam.AppendLine("OutPut Details");
                    InputParam.AppendLine(" ");
                    string Log = MethodName.ToString() + InputParam.ToString();
                    Log = Log + OutPutParam.ToString();
                    WriteServiceLog(Log, Path);
                    //WriteServiceLog("Method GetPost End" + DateTime.Now, Path);
                    WriteServiceLog("Method GetPost End", Path);
                }
                return obj.Serialize(OutCutomerInfo);
            }

            public string Catalog_Request_OptOut(string Title, string CustNumber, string CustFirstName, string CustLastName, string CustCompany, string CustAddr1, string CustAddr2, string CustCity, string CustState,
                                    string CustZip, string CustEmail, string CustEmIP, string CustPhone, string PromoCode, string ContestCode, string OptOutFlag, string CatErrCode,
                                    string CatErrMsg, string Bonus, string debug, string debug_id, string BirthMonth, string BirthDay, string ConfirmEmail, string IPaddress, string TimeStamp, string Path)
            {
                StringBuilder rsp = new StringBuilder();
                StringBuilder InputParam = new StringBuilder();
                StringBuilder OutPutParam = new StringBuilder();
                InputParam = CatalogRequestGetData(Title, CustNumber, CustFirstName, CustLastName, CustCompany, CustAddr1, CustAddr2, CustCity, CustState,
                                       CustZip, CustEmail, CustEmIP, CustPhone, PromoCode, ContestCode, OptOutFlag, CatErrCode,
                                       CatErrMsg, Bonus, debug, debug_id, BirthMonth, BirthDay, ConfirmEmail, IPaddress, TimeStamp, Path);
                WriteServiceLog("Method GetPost Start" + DateTime.Now, Path);
                
                JavaScriptSerializer obj = new JavaScriptSerializer();
                DataTable CustInfo = CreateCatalogRequestTable();
                string[] ArryCustInfo = new string[CustInfo.Columns.Count];
                for (int dataIndex = 0; dataIndex < CustInfo.Columns.Count; dataIndex++)
                {
                    ArryCustInfo[dataIndex] = "";
                }
                RedBackCallsService.Service1.CatalogRequest OutCutomerInfo = new RedBackCallsService.Service1.CatalogRequest();
                //if ((CustEmail != ""))
                //{
                    RedObject rb = new RedObject();
                    rb.Open3(RedBackAccount, "OPM:Catalog_Request");
                    ((RedProperty)rb.Property("Title")).Value = Title;
                    ((RedProperty)rb.Property("CustNumber")).Value = CustNumber;
                    ((RedProperty)rb.Property("CustFirstName")).Value = CustFirstName;
                    ((RedProperty)rb.Property("CustLastName")).Value = CustLastName;
                    ((RedProperty)rb.Property("CustCompany")).Value = CustCompany;
                    ((RedProperty)rb.Property("CustAddr1")).Value = CustAddr1;
                    ((RedProperty)rb.Property("CustAddr2")).Value = CustAddr2;
                    ((RedProperty)rb.Property("CustCity")).Value = CustCity;
                    ((RedProperty)rb.Property("CustState")).Value = CustState;
                    ((RedProperty)rb.Property("CustZip")).Value = CustZip;
                    ((RedProperty)rb.Property("CustEmail")).Value = CustEmail;
                    ((RedProperty)rb.Property("CustEmIP")).Value = CustEmIP;
                    ((RedProperty)rb.Property("CustPhone")).Value = CustPhone;
                    ((RedProperty)rb.Property("PromoCode")).Value = PromoCode;
                    ((RedProperty)rb.Property("ContestCode")).Value = ContestCode;
                    ((RedProperty)rb.Property("OptOutFlag")).Value = OptOutFlag;
                    ((RedProperty)rb.Property("CatErrCode")).Value = CatErrCode;
                    ((RedProperty)rb.Property("CatErrMsg")).Value = CatErrMsg;
                    ((RedProperty)rb.Property("Bonus")).Value = Bonus;
                    ((RedProperty)rb.Property("debug")).Value = debug;
                    ((RedProperty)rb.Property("debug_id")).Value = debug_id;
                    ((RedProperty)rb.Property("BirthMonth")).Value = BirthMonth;
                    ((RedProperty)rb.Property("BirthDay")).Value = BirthDay;
                    ((RedProperty)rb.Property("ConfirmEmail")).Value = ConfirmEmail;
                    ((RedProperty)rb.Property("IPaddress")).Value = IPaddress;
                    ((RedProperty)rb.Property("TimeStamp")).Value = TimeStamp;
                    rb.CallMethod("OptOutRequest");
                    string custMsg = "Success";
                    string retu = ((RedProperty)rb.Property("CustError")).Value;
                    if (((RedProperty)rb.Property("CustError")).Value != "")
                        custMsg = ((RedProperty)rb.Property("CustMessage")).Value;

                    OutCutomerInfo.Message = custMsg;
                    OutCutomerInfo.Title = ((RedProperty)rb.Property("Title")).Value.ToString();
                    OutCutomerInfo.CustNumber = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    OutCutomerInfo.CustFirstName = ((RedProperty)rb.Property("CustFirstName")).Value.ToString();
                    OutCutomerInfo.CustLastName = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    OutCutomerInfo.CustCompany = ((RedProperty)rb.Property("CustCompany")).Value.ToString();
                    OutCutomerInfo.CustAddr1 = ((RedProperty)rb.Property("CustAddr1")).Value.ToString();
                    OutCutomerInfo.CustAddr2 = ((RedProperty)rb.Property("CustAddr2")).Value.ToString();
                    OutCutomerInfo.CustCity = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    OutCutomerInfo.CustState = ((RedProperty)rb.Property("CustState")).Value.ToString();
                    OutCutomerInfo.CustZip = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    OutCutomerInfo.CustEmail = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    OutCutomerInfo.CustEmIP = ((RedProperty)rb.Property("CustEmIP")).Value.ToString();
                    OutCutomerInfo.CustPhone = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    OutCutomerInfo.PromoCode = ((RedProperty)rb.Property("PromoCode")).Value.ToString();
                    OutCutomerInfo.ContestCode = ((RedProperty)rb.Property("ContestCode")).Value.ToString();
                    OutCutomerInfo.OptOutFlag = ((RedProperty)rb.Property("OptOutFlag")).Value.ToString();
                    OutCutomerInfo.CatErrCode = ((RedProperty)rb.Property("CatErrCode")).Value.ToString();
                    OutCutomerInfo.CatErrMsg = ((RedProperty)rb.Property("CatErrMsg")).Value.ToString();
                    OutCutomerInfo.Bonus = ((RedProperty)rb.Property("Bonus")).Value.ToString();
                    OutCutomerInfo.debug = ((RedProperty)rb.Property("debug")).Value.ToString();
                    OutCutomerInfo.debug_id = ((RedProperty)rb.Property("debug_id")).Value.ToString();
                    OutCutomerInfo.BirthMonth = ((RedProperty)rb.Property("BirthMonth")).Value.ToString();
                    OutCutomerInfo.BirthDay = ((RedProperty)rb.Property("BirthDay")).Value.ToString();
                    OutCutomerInfo.ConfirmEmail = ((RedProperty)rb.Property("ConfirmEmail")).Value.ToString();
                    OutCutomerInfo.IPaddress = ((RedProperty)rb.Property("IPaddress")).Value.ToString();
                    OutCutomerInfo.TimeStamp = ((RedProperty)rb.Property("TimeStamp")).Value.ToString();

                    ArryCustInfo[1] = ((RedProperty)rb.Property("Title")).Value.ToString();
                    ArryCustInfo[2] = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    ArryCustInfo[3] = ((RedProperty)rb.Property("CustFirstName")).Value.ToString();
                    ArryCustInfo[4] = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    ArryCustInfo[5] = ((RedProperty)rb.Property("CustCompany")).Value.ToString();
                    ArryCustInfo[6] = ((RedProperty)rb.Property("CustAddr1")).Value.ToString();
                    ArryCustInfo[7] = ((RedProperty)rb.Property("CustAddr2")).Value.ToString();
                    ArryCustInfo[8] = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    ArryCustInfo[9] = ((RedProperty)rb.Property("CustState")).Value.ToString();
                    ArryCustInfo[10] = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    ArryCustInfo[11] = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    ArryCustInfo[12] = ((RedProperty)rb.Property("CustEmIP")).Value.ToString();
                    ArryCustInfo[13] = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    ArryCustInfo[14] = ((RedProperty)rb.Property("PromoCode")).Value.ToString();
                    ArryCustInfo[15] = ((RedProperty)rb.Property("ContestCode")).Value.ToString();
                    ArryCustInfo[16] = ((RedProperty)rb.Property("OptOutFlag")).Value.ToString();
                    ArryCustInfo[17] = ((RedProperty)rb.Property("CatErrCode")).Value.ToString();
                    ArryCustInfo[18] = ((RedProperty)rb.Property("CatErrMsg")).Value.ToString();
                    ArryCustInfo[19] = ((RedProperty)rb.Property("Bonus")).Value.ToString();
                    ArryCustInfo[20] = ((RedProperty)rb.Property("debug")).Value.ToString();
                    ArryCustInfo[21] = ((RedProperty)rb.Property("debug_id")).Value.ToString();
                    ArryCustInfo[22] = ((RedProperty)rb.Property("BirthMonth")).Value.ToString();
                    ArryCustInfo[23] = ((RedProperty)rb.Property("BirthDay")).Value.ToString();
                    ArryCustInfo[24] = ((RedProperty)rb.Property("ConfirmEmail")).Value.ToString();
                    ArryCustInfo[25] = ((RedProperty)rb.Property("IPaddress")).Value.ToString();
                    ArryCustInfo[26] = ((RedProperty)rb.Property("TimeStamp")).Value.ToString();
                //}
                CustInfo.Rows.Add(ArryCustInfo);
                OutPutParam = CatalogRequestGetData(ArryCustInfo);
                string LogOn = RedBackServiceGetLogOnDetails(Path);
                StringBuilder MethodName = new StringBuilder();
                if (LogOn == "1")
                {
                    MethodName.AppendLine(" ");
                    MethodName.AppendLine("Method Name : OptOutRequest");
                    MethodName.AppendLine(" ");
                    InputParam.AppendLine(" ");
                    InputParam.AppendLine("OutPut Details");
                    InputParam.AppendLine(" ");
                    string Log = MethodName.ToString() + InputParam.ToString();
                    Log = Log + OutPutParam.ToString();
                    WriteServiceLog(Log, Path);
                    //WriteServiceLog("Method GetPost End" + DateTime.Now, Path);
                    WriteServiceLog("Method GetPost End", Path);
                }
                return obj.Serialize(OutCutomerInfo);

            }

            //Rajesh 06/11/2012
            public string GiftCertificate(string Cert_Number, string Cert_Amount, string Cert_Status, string Cert_Err, string Cert_Errmsg, string Path)
            {
                StringBuilder rsp = new StringBuilder();
                StringBuilder InputParam = new StringBuilder();
                StringBuilder OutPutParam = new StringBuilder();
                InputParam = GiftCertificateGetData(Cert_Number, Cert_Amount, Cert_Status, Cert_Err, Cert_Errmsg, Path);
                WriteServiceLog("Method GetPost Start" + DateTime.Now, Path);
                
                JavaScriptSerializer obj = new JavaScriptSerializer();
                DataTable CustInfo = CreateGiftCertificateTable();
                string[] ArryCustInfo = new string[CustInfo.Columns.Count];
                for (int dataIndex = 0; dataIndex < CustInfo.Columns.Count; dataIndex++)
                {
                    ArryCustInfo[dataIndex] = "";
                }
                RedBackCallsService.Service1.GiftCertificate OutCutomerInfo = new RedBackCallsService.Service1.GiftCertificate();
                //if ((Keycode != ""))
                //{
                RedObject rb = new RedObject();
                //rb.Open3(RedBackAccount, "OPM:GiftCertificate");
                rb.Open3(RedBackAccount, "OPM:Gift_Replacement_Cert");
                ((RedProperty)rb.Property("Cert_Number")).Value = Cert_Number;
                ((RedProperty)rb.Property("Cert_Amount")).Value = Cert_Amount;
                ((RedProperty)rb.Property("Cert_Status")).Value = Cert_Status;
                ((RedProperty)rb.Property("Cert_Err")).Value = Cert_Err;
                ((RedProperty)rb.Property("Cert_Errmsg")).Value = Cert_Errmsg;



                rb.CallMethod("GiftCertificate");
                string custMsg = "Success";
                string retu = ((RedProperty)rb.Property("CustError")).Value;
                if (((RedProperty)rb.Property("CustError")).Value != "")
                    custMsg = ((RedProperty)rb.Property("CustMessage")).Value;

                OutCutomerInfo.Message = custMsg;
                OutCutomerInfo.Cert_Number = ((RedProperty)rb.Property("Cert_Number")).Value.ToString();
                OutCutomerInfo.Cert_Amount = ((RedProperty)rb.Property("Cert_Amount")).Value.ToString();
                OutCutomerInfo.Cert_Status = ((RedProperty)rb.Property("Cert_Status")).Value.ToString();
                OutCutomerInfo.Cert_Err = ((RedProperty)rb.Property("Cert_Err")).Value.ToString();
                OutCutomerInfo.Cert_Errmsg = ((RedProperty)rb.Property("Cert_Errmsg")).Value.ToString();




                ArryCustInfo[1] = ((RedProperty)rb.Property("Cert_Number")).Value.ToString();
                ArryCustInfo[2] = ((RedProperty)rb.Property("Cert_Amount")).Value.ToString();
                ArryCustInfo[3] = ((RedProperty)rb.Property("Cert_Status")).Value.ToString();
                ArryCustInfo[4] = ((RedProperty)rb.Property("Cert_Err")).Value.ToString();
                ArryCustInfo[5] = ((RedProperty)rb.Property("Cert_Errmsg")).Value.ToString();

                //}
                CustInfo.Rows.Add(ArryCustInfo);
                OutPutParam = GiftCertificateGetData(ArryCustInfo);
                string LogOn = RedBackServiceGetLogOnDetails(Path);
                StringBuilder MethodName = new StringBuilder();
                if (LogOn == "1")
                {
                    MethodName.AppendLine(" ");
                    MethodName.AppendLine("Method Name : GiftCertificate");
                    MethodName.AppendLine(" ");
                    InputParam.AppendLine(" ");
                    InputParam.AppendLine("OutPut Details");
                    InputParam.AppendLine(" ");
                    string Log = MethodName.ToString() + InputParam.ToString();
                    Log = Log + OutPutParam.ToString();
                    WriteServiceLog(Log, Path);
                    //WriteServiceLog("Method GetPost End" + DateTime.Now, Path);
                    WriteServiceLog("Method GetPost End", Path);
                }
                return obj.Serialize(OutCutomerInfo);
            }

            public string Replacecertificate(string Cert_Number, string Cert_Amount, string Cert_Status, string Cert_Err, string Cert_Errmsg, string Path)
            {
                StringBuilder rsp = new StringBuilder();
                StringBuilder InputParam = new StringBuilder();
                StringBuilder OutPutParam = new StringBuilder();
                InputParam = GiftCertificateGetData(Cert_Number, Cert_Amount, Cert_Status, Cert_Err, Cert_Errmsg, Path);
                WriteServiceLog("Method GetPost Start" + DateTime.Now, Path);
                
                JavaScriptSerializer obj = new JavaScriptSerializer();
                DataTable CustInfo = CreateGiftCertificateTable();
                string[] ArryCustInfo = new string[CustInfo.Columns.Count];
                for (int dataIndex = 0; dataIndex < CustInfo.Columns.Count; dataIndex++)
                {
                    ArryCustInfo[dataIndex] = "";
                }
                RedBackCallsService.Service1.GiftCertificate OutCutomerInfo = new RedBackCallsService.Service1.GiftCertificate();
                //if ((Keycode != ""))
                //{
                RedObject rb = new RedObject();
                //rb.Open3(RedBackAccount, "OPM:Replacecertificate");
                rb.Open3(RedBackAccount, "OPM:Gift_Replacement_Cert");
                ((RedProperty)rb.Property("Cert_Number")).Value = Cert_Number;
                ((RedProperty)rb.Property("Cert_Amount")).Value = Cert_Amount;
                ((RedProperty)rb.Property("Cert_Status")).Value = Cert_Status;
                ((RedProperty)rb.Property("Cert_Err")).Value = Cert_Err;
                ((RedProperty)rb.Property("Cert_Errmsg")).Value = Cert_Errmsg;



                rb.CallMethod("ReplaceCertificate");
                string custMsg = "Success";
                string retu = ((RedProperty)rb.Property("CustError")).Value;
                if (((RedProperty)rb.Property("CustError")).Value != "")
                    custMsg = ((RedProperty)rb.Property("CustMessage")).Value;

                OutCutomerInfo.Message = custMsg;
                OutCutomerInfo.Cert_Number = ((RedProperty)rb.Property("Cert_Number")).Value.ToString();
                OutCutomerInfo.Cert_Amount = ((RedProperty)rb.Property("Cert_Amount")).Value.ToString();
                OutCutomerInfo.Cert_Status = ((RedProperty)rb.Property("Cert_Status")).Value.ToString();
                OutCutomerInfo.Cert_Err = ((RedProperty)rb.Property("Cert_Err")).Value.ToString();
                OutCutomerInfo.Cert_Errmsg = ((RedProperty)rb.Property("Cert_Errmsg")).Value.ToString();




                ArryCustInfo[1] = ((RedProperty)rb.Property("Cert_Number")).Value.ToString();
                ArryCustInfo[2] = ((RedProperty)rb.Property("Cert_Amount")).Value.ToString();
                ArryCustInfo[3] = ((RedProperty)rb.Property("Cert_Status")).Value.ToString();
                ArryCustInfo[4] = ((RedProperty)rb.Property("Cert_Err")).Value.ToString();
                ArryCustInfo[5] = ((RedProperty)rb.Property("Cert_Errmsg")).Value.ToString();

                //}
                CustInfo.Rows.Add(ArryCustInfo);
                OutPutParam = GiftCertificateGetData(ArryCustInfo);
                string LogOn = RedBackServiceGetLogOnDetails(Path);
                StringBuilder MethodName = new StringBuilder();
                if (LogOn == "1")
                {
                    MethodName.AppendLine(" ");
                    MethodName.AppendLine("Method Name : Replacecertificate");
                    MethodName.AppendLine(" ");
                    InputParam.AppendLine(" ");
                    InputParam.AppendLine("OutPut Details");
                    InputParam.AppendLine(" ");
                    string Log = MethodName.ToString() + InputParam.ToString();
                    Log = Log + OutPutParam.ToString();
                    WriteServiceLog(Log, Path);
                    //WriteServiceLog("Method GetPost End" + DateTime.Now, Path);
                    WriteServiceLog("Method GetPost End", Path);
                }
                return obj.Serialize(OutCutomerInfo);
            }


            //venkat add

            public string RedBackServiceGetLogOnDetails(string Path)
            {


                DataTable dt = new DataTable("LogDetails");
                dt.Columns.Add("LoginOn");
                if (File.Exists(Path + "\\LogInNew.xml") == true)
                {
                    dt.ReadXml(Path + "\\LogInNew.xml");

                }

                string LogIn = dt.Rows[0]["LoginOn"].ToString();
                return LogIn;

            }

            public void WriteServiceLog(string Log, string Path)
            {

                string[] spliter = new string[] { "OutPut Details"};
                string[] ActData = Log.Split(spliter, StringSplitOptions.None);
                if (ActData.Length > 1)
                {
                    //Details of Input to RedBack
                    //hema
                    var est = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");                    
                    //end
                    //InsertServiceLog(DateTime.Now, ActData[0].ToString(), "Input to RedBack");
                    InsertServiceLog(TimeZoneInfo.ConvertTime(DateTime.Now, est), ActData[0].ToString(), "Input to RedBack");
                    //Details of Output from RedBack
                    //InsertServiceLog(DateTime.Now, ActData[1].ToString(), "Output from RedBack");
                    InsertServiceLog(TimeZoneInfo.ConvertTime(DateTime.Now, est), ActData[1].ToString(), "Output from RedBack");
                }
                

                    
//OutPut Details
                var est2 = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");   
                string FileName = Path + "\\Webservicelog" + DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year + ".txt";
                StreamWriter sw = new StreamWriter(FileName, true);
                sw.WriteLine(TimeZoneInfo.ConvertTime(DateTime.Now, est2).ToString());
                sw.WriteLine(Log);
                sw.Flush();
                sw.Close();
            }


            private StringBuilder ParamMasterGetData(string CustNumber, string Title, string CustName, string CustAdd1, string CustAdd2, string CustState, string CustCity, string CustZip, string CustCountry, string CustPhone, string CustFax, string CustEmail, string CustPin, string CustClubFlag, string CustClubDisc, string CustClubDate, string CustPinHint, string CustOptInFlag, string CustError, string CustMessage, string CustOrder, string CustFirstName, string CustLastName, string ClubNumber, string Path)
            {
                StringBuilder InputParam = new StringBuilder();

                InputParam.AppendLine("CustNumber: " + CustNumber);
                InputParam.AppendLine("Title: " + Title);
                InputParam.AppendLine("CustName: " + CustName);
                InputParam.AppendLine("CustAdd1: " + CustAdd1);
                InputParam.AppendLine("CustAdd2: " + CustAdd2);
                InputParam.AppendLine("CustState: " + CustState);
                InputParam.AppendLine("CustCity: " + CustCity);
                InputParam.AppendLine("CustZip: " + CustZip);
                InputParam.AppendLine("CustCountry: " + CustCountry);
                InputParam.AppendLine("CustPhone: " + CustPhone);
                InputParam.AppendLine("CustFax: " + CustFax);
                InputParam.AppendLine("CustEmail: " + CustEmail);
                InputParam.AppendLine("CustPin: " + CustPin);
                //InputParam.AppendLine("CustNumber: " + CustNumber);
                InputParam.AppendLine("CustClubFlag: " + CustClubFlag);
                InputParam.AppendLine("CustClubDisc: " + CustClubDisc);
                InputParam.AppendLine("CustClubDate: " + CustClubDate);
                InputParam.AppendLine("CustPinHint: " + CustPinHint);
                InputParam.AppendLine("CustOptInFlag: " + CustOptInFlag);
                InputParam.AppendLine("CustError: " + CustError);
                InputParam.AppendLine("CustMessage: " + CustMessage);
                InputParam.AppendLine("CustOrder: " + CustOrder);
                InputParam.AppendLine("CustFirstName: " + CustFirstName);
                InputParam.AppendLine("CustLastName: " + CustLastName);
                InputParam.AppendLine("ClubNumber: " + ClubNumber);

                return InputParam;
            }

            //Code bolck added by Rajesh 22/10/2012
            private StringBuilder KeyCodeLoginGetData(string Keycode, string Title, string Catalog, string CustNumber, string CustName, string CustFirstName, string CustLastName, string CustCompany, string CustAdd1, string CustAdd2, string CustCity, string CustState, string CustZip, string CustPhone, string CustEmail, string CustClubFlag, string CustClubDisc, string CustClubDate, string CustError, string CustMessage, string debug_id, string Path)
            {
                StringBuilder InputParam = new StringBuilder();

                InputParam.AppendLine("Keycode: " + Keycode);
                InputParam.AppendLine("Title: " + Title);
                InputParam.AppendLine("Catalog: " + Catalog);
                InputParam.AppendLine("CustNumber: " + CustNumber);
                InputParam.AppendLine("CustName: " + CustName);
                InputParam.AppendLine("CustFirstName: " + CustFirstName);
                InputParam.AppendLine("CustLastName: " + CustLastName);
                InputParam.AppendLine("CustCompany: " + CustCompany);
                InputParam.AppendLine("CustAdd1: " + CustAdd1);
                InputParam.AppendLine("CustAdd2: " + CustAdd2);
                InputParam.AppendLine("CustCity: " + CustCity);
                InputParam.AppendLine("CustState: " + CustState);
                InputParam.AppendLine("CustZip: " + CustZip);
                InputParam.AppendLine("CustPhone: " + CustPhone);
                InputParam.AppendLine("CustEmail: " + CustEmail);
                InputParam.AppendLine("CustClubFlag: " + CustClubFlag);
                InputParam.AppendLine("CustClubDisc: " + CustClubDisc);
                InputParam.AppendLine("CustClubDate: " + CustClubDate);
                InputParam.AppendLine("CustError: " + CustError);
                InputParam.AppendLine("CustMessage: " + CustMessage);
                InputParam.AppendLine("debug_id: " + debug_id);

                return InputParam;
            }

            private StringBuilder KeyCodeLoginGetData(string[] ArryCustInfo)
            {
                StringBuilder OutPutParam = new StringBuilder();

                OutPutParam.AppendLine("Keycode: " + ArryCustInfo[1]);
                OutPutParam.AppendLine("Title: " + ArryCustInfo[2]);
                OutPutParam.AppendLine("Catalog: " + ArryCustInfo[3]);
                OutPutParam.AppendLine("CustNumber: " + ArryCustInfo[4]);
                OutPutParam.AppendLine("CustName: " + ArryCustInfo[5]);
                OutPutParam.AppendLine("CustFirstName: " + ArryCustInfo[6]);
                OutPutParam.AppendLine("CustLastName: " + ArryCustInfo[7]);
                OutPutParam.AppendLine("CustCompany: " + ArryCustInfo[8]);
                OutPutParam.AppendLine("CustAdd1: " + ArryCustInfo[9]);
                OutPutParam.AppendLine("CustAdd2: " + ArryCustInfo[10]);
                OutPutParam.AppendLine("CustCity: " + ArryCustInfo[11]);
                OutPutParam.AppendLine("CustState: " + ArryCustInfo[12]);
                OutPutParam.AppendLine("CustZip: " + ArryCustInfo[13]);
                OutPutParam.AppendLine("CustEmail: " + ArryCustInfo[14]);
                OutPutParam.AppendLine("CustClubFlag: " + ArryCustInfo[15]);
                OutPutParam.AppendLine("CustClubDisc: " + ArryCustInfo[16]);
                OutPutParam.AppendLine("CustClubDate: " + ArryCustInfo[17]);
                OutPutParam.AppendLine("CustError: " + ArryCustInfo[19]);
                OutPutParam.AppendLine("CustMessage: " + ArryCustInfo[20]);
                OutPutParam.AppendLine("debug_id: " + ArryCustInfo[21]);

                return OutPutParam;
            }

            private StringBuilder CatalogRequestGetData(string Title, string CustNumber, string CustFirstName, string CustLastName, string CustCompany, string CustAddr1, string CustAddr2, string CustCity, string CustState,
                                    string CustZip, string CustEmail, string CustEmIP, string CustPhone, string PromoCode, string ContestCode, string OptOutFlag, string CatErrCode,
                                    string CatErrMsg, string Bonus, string debug, string debug_id, string BirthMonth, string BirthDay, string ConfirmEmail, string IPaddress, string TimeStamp, string Path)
            {
                StringBuilder InputParam = new StringBuilder();

                InputParam.AppendLine("Title: " + Title);
                InputParam.AppendLine("CustNumber: " + CustNumber);
                InputParam.AppendLine("CustFirstName: " + CustFirstName);
                InputParam.AppendLine("CustLastName: " + CustLastName);
                InputParam.AppendLine("CustCompany: " + CustCompany);
                InputParam.AppendLine("CustAddr1: " + CustAddr1);
                InputParam.AppendLine("CustAddr2: " + CustAddr2);
                InputParam.AppendLine("CustCity: " + CustCity);
                InputParam.AppendLine("CustState: " + CustState);
                InputParam.AppendLine("CustZip: " + CustZip);
                InputParam.AppendLine("CustEmail: " + CustEmail);
                InputParam.AppendLine("CustEmIP: " + CustEmIP);
                InputParam.AppendLine("CustPhone: " + CustPhone);
                InputParam.AppendLine("PromoCode: " + PromoCode);
                InputParam.AppendLine("ContestCode: " + ContestCode);
                InputParam.AppendLine("OptOutFlag: " + OptOutFlag);
                InputParam.AppendLine("CatErrCode: " + CatErrCode);
                InputParam.AppendLine("CatErrMsg: " + CatErrMsg);
                InputParam.AppendLine("Bonus: " + Bonus);
                InputParam.AppendLine("debug: " + debug);
                InputParam.AppendLine("debug_id: " + debug_id);
                InputParam.AppendLine("BirthMonth: " + BirthMonth);
                InputParam.AppendLine("ConfirmEmail: " + ConfirmEmail);
                InputParam.AppendLine("IPaddress: " + IPaddress);
                InputParam.AppendLine("TimeStamp: " + TimeStamp);
                return InputParam;
            }



            private StringBuilder OrderNewGetData(string OrderNumber, string CustFirstName, string CustLastName, string CustAdd1, string CustAdd2, string CustAdd3, string CustCity, string CustState, string CustZip, string CustCountry,
                                string CustPhoneDay, string CustPhoneEve, string CustEmail1, string CustEmail2, string CustPin, string ShipFirstName, string ShipLastName, string ShipAdd1, string ShipAdd2, string ShipAdd3, string ShipCity, string ShipState, string ShipZip, string ShipCountry,
                                string ShipPhone, string ShipEmail, string ShipAttention, string CustNumber, string WebReference, string PromoCode, string Title, string ShipMethod, string PaymentMethod,
                                string CreditCardNumber, string CardExpDate, string CardCVV, string CardAddress, string CardZip, string MerchAmount, string CouponAmount, string DiscAmount, string ShipAmount, string PremShipAmount,
                                string OverweightAmount, string TaxAmount, string TotalAmount, string Comments, string Items, string QtyOrdered, string UnitPrice,
                                string ReleaseDate, string FreeFlag, string PersonalizationCode, string PersonalizationDetail1, string PersonalizationDetail2, string PersonalizationDetail3, string PersonalizationDetail4,
                                string PersonalizationDetail5, string PIN, string PINHint, string OptInFlag, string OptInDate, string OptOutDate, string Etrack, string IPSource,
                                string GVCFlag, string GVCDate, string ValidErrCode, string ValidErrMsg, string OrderErrCode, string OrderErrMsg, string Cert1_Number, string Cert1_Amount, string Cert2_Number, string Cert2_Amount, string Cert3_Number,
                                string Cert3_Amount, string Repc1_Number, string Repc1_Amount, string Repc2_Number, string Repc2_Amount, string Repc3_Number, string Repc3_Amount, string Web_Date, string Web_Time, string Path, string MobilePhone, string MobileOptin)
            {
                StringBuilder InputParam = new StringBuilder();

                InputParam.AppendLine("OrderNumber: " + OrderNumber);
                InputParam.AppendLine("CustFirstName: " + CustFirstName);
                InputParam.AppendLine("CustLastName: " + CustLastName);
                InputParam.AppendLine("CustAdd1: " + CustAdd1);
                InputParam.AppendLine("CustAdd2: " + CustAdd2);
                InputParam.AppendLine("CustAdd3: " + CustAdd3);
                InputParam.AppendLine("CustCity: " + CustCity);
                InputParam.AppendLine("CustState: " + CustState);
                InputParam.AppendLine("CustZip: " + CustZip);
                InputParam.AppendLine("CustCountry: " + CustCountry);
                InputParam.AppendLine("CustPhoneDay: " + CustPhoneDay);
                InputParam.AppendLine("CustPhoneEve: " + CustPhoneEve);
                InputParam.AppendLine("CustEmail1: " + CustEmail1);
                InputParam.AppendLine("CustEmail2: " + CustEmail2);
                InputParam.AppendLine("CustPin: " + CustPin);
                InputParam.AppendLine("ShipFirstName: " + ShipFirstName);
                InputParam.AppendLine("ShipLastName: " + ShipLastName);
                InputParam.AppendLine("ShipAdd1: " + ShipAdd1);
                InputParam.AppendLine("ShipAdd2: " + ShipAdd2);
                InputParam.AppendLine("ShipAdd3: " + ShipAdd3);
                InputParam.AppendLine("ShipCity: " + ShipCity);
                InputParam.AppendLine("ShipState: " + ShipState);
                InputParam.AppendLine("ShipZip: " + ShipZip);
                InputParam.AppendLine("ShipCountry: " + ShipCountry);

                InputParam.AppendLine("ShipPhone: " + ShipPhone);
                InputParam.AppendLine("ShipEmail: " + ShipEmail);
                InputParam.AppendLine("ShipAttention: " + ShipAttention);
                InputParam.AppendLine("CustNumber: " + CustNumber);
                InputParam.AppendLine("WebReference: " + WebReference);
                InputParam.AppendLine("PromoCode: " + PromoCode);
                InputParam.AppendLine("Title: " + Title);
                InputParam.AppendLine("ShipMethod: " + ShipMethod);
                InputParam.AppendLine("PaymentMethod: " + PaymentMethod);
                //For hiding credit card number in log file
                string ActCCN = CreditCardNumber;
                CreditCardNumber = "xxxxxxxxxxxx" + ActCCN.Substring(8, ActCCN.Length - 12);

                InputParam.AppendLine("CreditCardNumber: " + CreditCardNumber);
                InputParam.AppendLine("CardExpDate: " + CardExpDate);
                InputParam.AppendLine("CardCVV: " + CardCVV);
                InputParam.AppendLine("CardAddress: " + CardAddress);
                InputParam.AppendLine("CardZip: " + CardZip);
                InputParam.AppendLine("MerchAmount: " + MerchAmount);
                InputParam.AppendLine("CouponAmount: " + CouponAmount);
                InputParam.AppendLine("DiscAmount: " + DiscAmount);
                InputParam.AppendLine("ShipAmount: " + ShipAmount);
                InputParam.AppendLine("PremShipAmount: " + PremShipAmount);

                InputParam.AppendLine("OverweightAmount: " + OverweightAmount);
                InputParam.AppendLine("TaxAmount: " + TaxAmount);
                InputParam.AppendLine("TotalAmount: " + TotalAmount);
                InputParam.AppendLine("Comments: " + Comments);
                InputParam.AppendLine("Items: " + Items);
                InputParam.AppendLine("QtyOrdered: " + QtyOrdered);
                InputParam.AppendLine("UnitPrice: " + UnitPrice);
                InputParam.AppendLine("ReleaseDate: " + ReleaseDate);
                InputParam.AppendLine("FreeFlag: " + FreeFlag);
                InputParam.AppendLine("PersonalizationCode: " + PersonalizationCode);
                InputParam.AppendLine("PersonalizationDetail1: " + PersonalizationDetail1);
                InputParam.AppendLine("PersonalizationDetail2: " + PersonalizationDetail2);
                InputParam.AppendLine("PersonalizationDetail3: " + PersonalizationDetail3);
                InputParam.AppendLine("PersonalizationDetail4: " + PersonalizationDetail4);
                InputParam.AppendLine("PersonalizationDetail5: " + PersonalizationDetail5);

                InputParam.AppendLine("PIN: " + PIN);
                InputParam.AppendLine("PINHint: " + PINHint);
                InputParam.AppendLine("OptInFlag: " + OptInFlag);
                InputParam.AppendLine("OptInDate: " + OptInDate);
                InputParam.AppendLine("OptOutDate: " + OptOutDate);
                InputParam.AppendLine("Etrack: " + Etrack);
                InputParam.AppendLine("IPSource: " + IPSource);
                InputParam.AppendLine("GVCFlag: " + GVCFlag);
                InputParam.AppendLine("GVCDate: " + GVCDate);
                InputParam.AppendLine("ValidErrCode: " + ValidErrCode);

                InputParam.AppendLine("ValidErrMsg: " + ValidErrMsg);
                InputParam.AppendLine("OrderErrCode: " + OrderErrCode);
                InputParam.AppendLine("OrderErrMsg: " + OrderErrMsg);
                InputParam.AppendLine("Cert1_Number: " + Cert1_Number);
                InputParam.AppendLine("Cert1_Amount: " + Cert1_Amount);
                InputParam.AppendLine("Cert2_Number: " + Cert2_Number);
                InputParam.AppendLine("Cert2_Amount: " + Cert2_Amount);
                InputParam.AppendLine("Cert3_Number: " + Cert3_Number);
                InputParam.AppendLine("Cert3_Amount: " + Cert3_Amount);
                InputParam.AppendLine("Repc1_Number: " + Repc1_Number);
                InputParam.AppendLine("Repc1_Amount: " + Repc1_Amount);
                InputParam.AppendLine("Repc2_Number: " + Repc2_Number);
                InputParam.AppendLine("Repc2_Amount: " + Repc2_Amount);
                InputParam.AppendLine("Repc3_Number: " + Repc3_Number);
                InputParam.AppendLine("Repc3_Amount: " + Repc3_Amount);
                InputParam.AppendLine("Web_Date: " + Web_Date);
                InputParam.AppendLine("Web_Time: " + Web_Time);
                InputParam.AppendLine("MobilePhone: " + MobilePhone);  //added by aditya 15-nov-2012
                InputParam.AppendLine("MobileOptin: " + MobileOptin);  //edded by aditya 15-nov-2012

                return InputParam;
            }
            //Code bolck ended by Rajesh 22/10/2012

            private StringBuilder OrderNewGetData(string[] ArryCustInfo)
            {
                StringBuilder OutPutParam = new StringBuilder();

                OutPutParam.AppendLine("OrderNumber" + ArryCustInfo[1]);
                OutPutParam.AppendLine("CustFirstName" + ArryCustInfo[2]);
                OutPutParam.AppendLine("CustLastName" + ArryCustInfo[3]);
                OutPutParam.AppendLine("CustAdd1" + ArryCustInfo[4]);
                OutPutParam.AppendLine("CustAdd2" + ArryCustInfo[5]);
                OutPutParam.AppendLine("CustAdd3" + ArryCustInfo[6]);
                OutPutParam.AppendLine("CustCity" + ArryCustInfo[7]);
                OutPutParam.AppendLine("CustState" + ArryCustInfo[8]);
                OutPutParam.AppendLine("CustZip" + ArryCustInfo[9]);
                OutPutParam.AppendLine("CustCountry" + ArryCustInfo[10]);
                OutPutParam.AppendLine("CustPhoneDay" + ArryCustInfo[11]);
                OutPutParam.AppendLine("CustPhoneEve" + ArryCustInfo[12]);
                OutPutParam.AppendLine("CustEmail1" + ArryCustInfo[13]);
                OutPutParam.AppendLine("CustEmail2" + ArryCustInfo[14]);
                OutPutParam.AppendLine("CustPin" + ArryCustInfo[15]);
                OutPutParam.AppendLine("ShipFirstName" + ArryCustInfo[16]);
                OutPutParam.AppendLine("ShipLastName" + ArryCustInfo[17]);
                OutPutParam.AppendLine("ShipAdd1" + ArryCustInfo[18]);
                OutPutParam.AppendLine("ShipAdd2" + ArryCustInfo[19]);
                OutPutParam.AppendLine("ShipAdd3" + ArryCustInfo[20]);
                OutPutParam.AppendLine("ShipCity" + ArryCustInfo[21]);
                OutPutParam.AppendLine("ShipState" + ArryCustInfo[22]);
                OutPutParam.AppendLine("ShipZip" + ArryCustInfo[23]);
                OutPutParam.AppendLine("ShipCountry" + ArryCustInfo[24]);

                OutPutParam.AppendLine("ShipPhone" + ArryCustInfo[25]);
                OutPutParam.AppendLine("ShipEmail" + ArryCustInfo[26]);
                OutPutParam.AppendLine("ShipAttention" + ArryCustInfo[27]);
                OutPutParam.AppendLine("CustNumber" + ArryCustInfo[28]);
                OutPutParam.AppendLine("WebReference" + ArryCustInfo[29]);
                OutPutParam.AppendLine("PromoCode" + ArryCustInfo[30]);
                OutPutParam.AppendLine("Title" + ArryCustInfo[31]);
                OutPutParam.AppendLine("ShipMethod" + ArryCustInfo[32]);
                OutPutParam.AppendLine("PaymentMethod" + ArryCustInfo[33]);
                OutPutParam.AppendLine("CreditCardNumber" + ArryCustInfo[34]);
                OutPutParam.AppendLine("CardExpDate" + ArryCustInfo[35]);
                OutPutParam.AppendLine("CardCVV" + ArryCustInfo[36]);
                OutPutParam.AppendLine("CardAddress" + ArryCustInfo[37]);
                OutPutParam.AppendLine("CardZip" + ArryCustInfo[38]);
                OutPutParam.AppendLine("MerchAmount" + ArryCustInfo[39]);
                OutPutParam.AppendLine("CouponAmount" + ArryCustInfo[40]);
                OutPutParam.AppendLine("DiscAmount" + ArryCustInfo[41]);
                OutPutParam.AppendLine("ShipAmount" + ArryCustInfo[42]);
                OutPutParam.AppendLine("PremShipAmount" + ArryCustInfo[43]);
                OutPutParam.AppendLine("OverweightAmount" + ArryCustInfo[44]);
                OutPutParam.AppendLine("TaxAmount" + ArryCustInfo[45]);
                OutPutParam.AppendLine("TotalAmount" + ArryCustInfo[46]);
                OutPutParam.AppendLine("Comments" + ArryCustInfo[47]);
                OutPutParam.AppendLine("Items" + ArryCustInfo[48]);
                OutPutParam.AppendLine("QtyOrdered" + ArryCustInfo[49]);
                OutPutParam.AppendLine("UnitPrice" + ArryCustInfo[50]);
                OutPutParam.AppendLine("ReleaseDate" + ArryCustInfo[51]);
                OutPutParam.AppendLine("FreeFlag" + ArryCustInfo[52]);
                OutPutParam.AppendLine("PersonalizationCode" + ArryCustInfo[53]);
                OutPutParam.AppendLine("PersonalizationDetail1" + ArryCustInfo[54]);
                OutPutParam.AppendLine("PersonalizationDetail2" + ArryCustInfo[55]);
                OutPutParam.AppendLine("PersonalizationDetail3" + ArryCustInfo[56]);
                OutPutParam.AppendLine("PersonalizationDetail4" + ArryCustInfo[57]);
                OutPutParam.AppendLine("PersonalizationDetail5" + ArryCustInfo[58]);

                OutPutParam.AppendLine("PIN" + ArryCustInfo[59]);
                OutPutParam.AppendLine("PINHint" + ArryCustInfo[60]);
                OutPutParam.AppendLine("OptInFlag" + ArryCustInfo[61]);
                OutPutParam.AppendLine("OptInDate" + ArryCustInfo[62]);
                OutPutParam.AppendLine("OptOutDate" + ArryCustInfo[63]);
                OutPutParam.AppendLine("Etrack" + ArryCustInfo[64]);
                OutPutParam.AppendLine("IPSource" + ArryCustInfo[65]);
                OutPutParam.AppendLine("GVCFlag" + ArryCustInfo[66]);
                OutPutParam.AppendLine("GVCDate" + ArryCustInfo[67]);
                OutPutParam.AppendLine("ValidErrCode" + ArryCustInfo[68]);

                OutPutParam.AppendLine("ValidErrMsg" + ArryCustInfo[69]);
                OutPutParam.AppendLine("OrderErrCode" + ArryCustInfo[70]);
                OutPutParam.AppendLine("OrderErrMsg" + ArryCustInfo[71]);
                OutPutParam.AppendLine("Cert1_Number" + ArryCustInfo[72]);
                OutPutParam.AppendLine("Cert1_Amount" + ArryCustInfo[73]);
                OutPutParam.AppendLine("Cert2_Number" + ArryCustInfo[74]);
                OutPutParam.AppendLine("Cert2_Amount" + ArryCustInfo[75]);
                OutPutParam.AppendLine("Cert3_Number" + ArryCustInfo[76]);
                OutPutParam.AppendLine("Cert3_Amount" + ArryCustInfo[77]);
                OutPutParam.AppendLine("Repc1_Number" + ArryCustInfo[78]);
                OutPutParam.AppendLine("Repc1_Amount" + ArryCustInfo[79]);
                OutPutParam.AppendLine("Repc2_Number" + ArryCustInfo[80]);
                OutPutParam.AppendLine("Repc2_Amount" + ArryCustInfo[81]);
                OutPutParam.AppendLine("Repc3_Number" + ArryCustInfo[82]);
                OutPutParam.AppendLine("Repc3_Amount" + ArryCustInfo[83]);
                OutPutParam.AppendLine("Web_Date" + ArryCustInfo[84]);
                OutPutParam.AppendLine("Web_Time" + ArryCustInfo[85]);
                OutPutParam.AppendLine("MobilePhone" + ArryCustInfo[86]);  //added by aditya 15-nov-2012
                OutPutParam.AppendLine("MobileOptin" + ArryCustInfo[87]);  //end by aditya 15-nov-2012



                return OutPutParam;
            }

            private StringBuilder OrderStatusGetData(string CustNumber, string CustName, string CustAdd1, string CustAdd2, string CustState, string CustCity, string CustZip, string CustCountry,
                                   string CustPhone, string CustFax, string CustEmail, string CustPin, string Title, string SumOrderNumbers, string SumOrderDates, string SumShipNames,
                                   string SumShipStatus, string SumOrderTotal, string DetOrderNumber, string DetOrderDate, string DetOrderAmount, string DetShipName, string DetShipAdd1,
                                   string DetShipAdd2, string DetShipCity, string DetShipState, string DetShipZip, string DetShipNumber, string DetShipItems, string DetShipQty,
                                   string DetShipDesc, string DetShipVia, string DetShipDate, string DetShipTrackNums, string DetShipTrackLink,
                                   string DetShipEstDelBegin, string DetShipEstDelEnd, string DetOpenLocs, string DetOpenItems, string DetOpenQtys,
                                   string DetOpenDesc, string DetOpenEstDelBegin, string DetOpenEstDelEnd, string StatusErr, string StatusMsg, string Path)
            {
                StringBuilder InputParam = new StringBuilder();

                InputParam.AppendLine("CustNumber: " + CustNumber);
                InputParam.AppendLine("CustName: " + CustName);
                InputParam.AppendLine("CustAdd1: " + CustAdd1);
                InputParam.AppendLine("CustAdd2: " + CustAdd2);
                InputParam.AppendLine("CustState: " + CustState);
                InputParam.AppendLine("CustCity: " + CustCity);
                InputParam.AppendLine("CustZip: " + CustZip);
                InputParam.AppendLine("CustCountry: " + CustCountry);
                InputParam.AppendLine("CustPhone: " + CustPhone);
                InputParam.AppendLine("CustFax: " + CustFax);
                InputParam.AppendLine("CustEmail: " + CustEmail);
                InputParam.AppendLine("CustPin: " + CustPin);
                InputParam.AppendLine("Title: " + Title);
                InputParam.AppendLine("SumOrderNumbers: " + SumOrderNumbers);
                InputParam.AppendLine("SumOrderDates: " + SumOrderDates);
                InputParam.AppendLine("SumShipNames: " + SumShipNames);
                InputParam.AppendLine("SumShipStatus: " + SumShipStatus);
                InputParam.AppendLine("SumOrderTotal: " + SumOrderTotal);
                InputParam.AppendLine("DetOrderNumber: " + DetOrderNumber);
                InputParam.AppendLine("DetOrderDate: " + DetOrderDate);
                InputParam.AppendLine("DetOrderAmount: " + DetOrderAmount);
                InputParam.AppendLine("DetShipName: " + DetShipName);
                InputParam.AppendLine("DetShipAdd1: " + DetShipAdd1);
                InputParam.AppendLine("DetShipAdd2: " + DetShipAdd2);

                InputParam.AppendLine("DetShipCity: " + DetShipCity);
                InputParam.AppendLine("DetShipState: " + DetShipState);
                InputParam.AppendLine("DetShipZip: " + DetShipZip);
                InputParam.AppendLine("DetShipNumber: " + DetShipNumber);
                InputParam.AppendLine("DetShipItems: " + DetShipItems);
                InputParam.AppendLine("DetShipQty: " + DetShipQty);
                InputParam.AppendLine("DetShipDesc: " + DetShipDesc);
                InputParam.AppendLine("DetShipVia: " + DetShipVia);
                InputParam.AppendLine("DetShipDate: " + DetShipDate);
                InputParam.AppendLine("DetShipTrackNums: " + DetShipTrackNums);
                InputParam.AppendLine("DetShipTrackLink: " + DetShipTrackLink);

                InputParam.AppendLine("DetShipEstDelBegin: " + DetShipEstDelBegin);
                InputParam.AppendLine("DetShipEstDelEnd: " + DetShipEstDelEnd);
                InputParam.AppendLine("DetOpenLocs: " + DetOpenLocs);
                InputParam.AppendLine("DetOpenItems: " + DetOpenItems);
                InputParam.AppendLine("DetOpenQtys: " + DetOpenQtys);
                InputParam.AppendLine("DetOpenDesc: " + DetOpenDesc);
                InputParam.AppendLine("DetOpenEstDelBegin: " + DetOpenEstDelBegin);
                InputParam.AppendLine("DetOpenEstDelEnd: " + DetOpenEstDelEnd);
                InputParam.AppendLine("StatusErr: " + StatusErr);
                InputParam.AppendLine("StatusMsg: " + StatusMsg);
                return InputParam;
            }

            private StringBuilder OrderStatusGetData(string[] ArryCustInfo)
            {
                StringBuilder OutPutParam = new StringBuilder();

                OutPutParam.AppendLine("CustNumber" + ArryCustInfo[1]);
                OutPutParam.AppendLine("CustName" + ArryCustInfo[2]);
                OutPutParam.AppendLine("CustAdd1" + ArryCustInfo[3]);
                OutPutParam.AppendLine("CustAdd2" + ArryCustInfo[4]);
                OutPutParam.AppendLine("CustState" + ArryCustInfo[5]);
                OutPutParam.AppendLine("CustCity" + ArryCustInfo[6]);
                OutPutParam.AppendLine("CustZip" + ArryCustInfo[7]);
                OutPutParam.AppendLine("CustCountry" + ArryCustInfo[8]);
                OutPutParam.AppendLine("CustPhone" + ArryCustInfo[9]);
                OutPutParam.AppendLine("CustFax" + ArryCustInfo[10]);
                OutPutParam.AppendLine("CustEmail" + ArryCustInfo[11]);
                OutPutParam.AppendLine("CustPin" + ArryCustInfo[12]);
                OutPutParam.AppendLine("Title" + ArryCustInfo[13]);
                OutPutParam.AppendLine("SumOrderNumbers" + ArryCustInfo[14]);
                OutPutParam.AppendLine("SumOrderDates" + ArryCustInfo[15]);
                OutPutParam.AppendLine("SumShipNames" + ArryCustInfo[16]);
                OutPutParam.AppendLine("SumShipStatus" + ArryCustInfo[17]);
                OutPutParam.AppendLine("SumOrderTotal" + ArryCustInfo[18]);
                OutPutParam.AppendLine("DetOrderNumber" + ArryCustInfo[19]);
                OutPutParam.AppendLine("DetOrderDate" + ArryCustInfo[20]);
                OutPutParam.AppendLine("DetOrderAmount" + ArryCustInfo[21]);
                OutPutParam.AppendLine("DetShipName" + ArryCustInfo[22]);
                OutPutParam.AppendLine("DetShipAdd1" + ArryCustInfo[23]);
                OutPutParam.AppendLine("DetShipAdd2" + ArryCustInfo[24]);

                OutPutParam.AppendLine("DetShipCity" + ArryCustInfo[25]);
                OutPutParam.AppendLine("DetShipState" + ArryCustInfo[26]);
                OutPutParam.AppendLine("DetShipZip" + ArryCustInfo[27]);
                OutPutParam.AppendLine("DetShipNumber" + ArryCustInfo[28]);
                OutPutParam.AppendLine("DetShipItems" + ArryCustInfo[29]);
                OutPutParam.AppendLine("DetShipQty" + ArryCustInfo[30]);
                OutPutParam.AppendLine("DetShipDesc" + ArryCustInfo[31]);
                OutPutParam.AppendLine("DetShipVia" + ArryCustInfo[32]);
                OutPutParam.AppendLine("DetShipDate" + ArryCustInfo[33]);
                OutPutParam.AppendLine("DetShipTrackNums" + ArryCustInfo[34]);
                OutPutParam.AppendLine("DetShipTrackLink" + ArryCustInfo[35]);
                OutPutParam.AppendLine("DetShipEstDelBegin" + ArryCustInfo[36]);
                OutPutParam.AppendLine("DetShipEstDelEnd" + ArryCustInfo[37]);
                OutPutParam.AppendLine("DetOpenLocs" + ArryCustInfo[38]);
                OutPutParam.AppendLine("DetOpenItems" + ArryCustInfo[39]);
                OutPutParam.AppendLine("DetOpenQtys" + ArryCustInfo[40]);
                OutPutParam.AppendLine("DetOpenDesc" + ArryCustInfo[41]);
                OutPutParam.AppendLine("DetOpenEstDelBegin" + ArryCustInfo[42]);
                OutPutParam.AppendLine("DetOpenEstDelEnd" + ArryCustInfo[43]);
                OutPutParam.AppendLine("StatusErr" + ArryCustInfo[44]);
                OutPutParam.AppendLine("StatusMsg" + ArryCustInfo[45]);

                return OutPutParam;
            }

            private StringBuilder ParamMasterGetData(string[] ArryCustInfo)
            {
                StringBuilder OutPutParam = new StringBuilder();

                OutPutParam.AppendLine("CustNumber: " + ArryCustInfo[0]);
                OutPutParam.AppendLine("Title: " + ArryCustInfo[1]);
                OutPutParam.AppendLine("CustName: " + ArryCustInfo[2]);
                OutPutParam.AppendLine("CustAdd1: " + ArryCustInfo[3]);
                OutPutParam.AppendLine("CustAdd2: " + ArryCustInfo[4]);
                OutPutParam.AppendLine("CustCity: " + ArryCustInfo[5]);
                OutPutParam.AppendLine("CustZip: " + ArryCustInfo[6]);
                OutPutParam.AppendLine("CustCountry: " + ArryCustInfo[7]);
                OutPutParam.AppendLine("CustPhone: " + ArryCustInfo[8]);
                OutPutParam.AppendLine("CustFax: " + ArryCustInfo[9]);
                OutPutParam.AppendLine("CustEmail: " + ArryCustInfo[10]);
                OutPutParam.AppendLine("CustPin: " + ArryCustInfo[11]);
                OutPutParam.AppendLine("CustNumber: " + ArryCustInfo[12]);
                OutPutParam.AppendLine("CustClubFlag: " + ArryCustInfo[13]);
                OutPutParam.AppendLine("CustClubDisc: " + ArryCustInfo[14]);
                OutPutParam.AppendLine("CustClubDate: " + ArryCustInfo[15]);
                OutPutParam.AppendLine("CustPinHint: " + ArryCustInfo[16]);
                OutPutParam.AppendLine("CustOptInFlag: " + ArryCustInfo[17]);
                OutPutParam.AppendLine("CustError: " + ArryCustInfo[18]);
                OutPutParam.AppendLine("CustMessage: " + ArryCustInfo[19]);
                OutPutParam.AppendLine("CustOrder: " + ArryCustInfo[20]);
                OutPutParam.AppendLine("CustFirstName: " + ArryCustInfo[21]);
                OutPutParam.AppendLine("CustLastName: " + ArryCustInfo[22]);
                OutPutParam.AppendLine("ClubNumber: " + ArryCustInfo[23]);
                OutPutParam.AppendLine("CustState: " + ArryCustInfo[24]);
                return OutPutParam;
            }

            private StringBuilder CatalogRequestGetData(string[] ArryCustInfo)
            {
                StringBuilder OutPutParam = new StringBuilder();


                OutPutParam.AppendLine("Title: " + ArryCustInfo[1]);
                OutPutParam.AppendLine("CustNumber: " + ArryCustInfo[2]);
                OutPutParam.AppendLine("CustFirstName: " + ArryCustInfo[3]);
                OutPutParam.AppendLine("CustLastName: " + ArryCustInfo[4]);
                OutPutParam.AppendLine("CustCompany: " + ArryCustInfo[5]);
                OutPutParam.AppendLine("CustAddr1: " + ArryCustInfo[6]);
                OutPutParam.AppendLine("CustAddr2: " + ArryCustInfo[7]);
                OutPutParam.AppendLine("CustCity: " + ArryCustInfo[8]);
                OutPutParam.AppendLine("CustState: " + ArryCustInfo[9]);
                OutPutParam.AppendLine("CustZip: " + ArryCustInfo[10]);
                OutPutParam.AppendLine("CustEmail: " + ArryCustInfo[11]);
                OutPutParam.AppendLine("CustEmIP: " + ArryCustInfo[12]);
                OutPutParam.AppendLine("CustPhone: " + ArryCustInfo[13]);
                OutPutParam.AppendLine("PromoCode: " + ArryCustInfo[14]);
                OutPutParam.AppendLine("ContestCode: " + ArryCustInfo[15]);
                OutPutParam.AppendLine("OptOutFlag: " + ArryCustInfo[16]);
                OutPutParam.AppendLine("CatErrCode: " + ArryCustInfo[17]);
                OutPutParam.AppendLine("CatErrMsg: " + ArryCustInfo[18]);
                OutPutParam.AppendLine("Bonus: " + ArryCustInfo[19]);
                OutPutParam.AppendLine("debug: " + ArryCustInfo[20]);
                OutPutParam.AppendLine("debug_id: " + ArryCustInfo[21]);
                OutPutParam.AppendLine("BirthMonth: " + ArryCustInfo[22]);
                OutPutParam.AppendLine("BirthDay: " + ArryCustInfo[23]);
                OutPutParam.AppendLine("ConfirmEmail: " + ArryCustInfo[24]);
                OutPutParam.AppendLine("IPaddress: " + ArryCustInfo[25]);
                OutPutParam.AppendLine("TimeStamp: " + ArryCustInfo[26]);

                return OutPutParam;
            }

            //Rajesh 06/11/2012
            private StringBuilder GiftCertificateGetData(string Cert_Number, string Cert_Amount, string Cert_Status, string Cert_Err, string Cert_Errmsg, string Path)
            {
                StringBuilder InputParam = new StringBuilder();

                InputParam.AppendLine("Cert_Number: " + Cert_Number);
                InputParam.AppendLine("Cert_Amount: " + Cert_Amount);
                InputParam.AppendLine("Cert_Status: " + Cert_Status);
                InputParam.AppendLine("Cert_Err: " + Cert_Err);
                InputParam.AppendLine("Cert_Errmsg: " + Cert_Errmsg);
                return InputParam;
            }

            //Rajesh 06/11/2012
            private StringBuilder GiftCertificateGetData(string[] ArryCustInfo)
            {
                StringBuilder OutPutParam = new StringBuilder();

                OutPutParam.AppendLine("Cert_Number: " + ArryCustInfo[1]);
                OutPutParam.AppendLine("Cert_Amount: " + ArryCustInfo[2]);
                OutPutParam.AppendLine("Cert_Status: " + ArryCustInfo[3]);
                OutPutParam.AppendLine("Cert_Err: " + ArryCustInfo[4]);
                OutPutParam.AppendLine("Cert_Errmsg: " + ArryCustInfo[5]);

                return OutPutParam;
            }

            public string Cust_MasterAccessCust(string CustNumber, string Title, string CustName, string CustAdd1, string CustAdd2, string CustState, string CustCity, string CustZip, string CustCountry, string CustPhone, string CustFax, string CustEmail, string CustPin, string CustClubFlag, string CustClubDisc, string CustClubDate, string CustPinHint, string CustOptInFlag, string CustError, string CustMessage, string CustOrder, string CustFirstName, string CustLastName, string ClubNumber, string Path)
            {
                StringBuilder rsp = new StringBuilder();
                StringBuilder InputParam = new StringBuilder();
                StringBuilder OutPutParam = new StringBuilder();
                InputParam = ParamMasterGetData(CustNumber, Title, CustName, CustAdd1, CustAdd2, CustState, CustCity, CustZip, CustCountry, CustPhone, CustFax, CustEmail, CustPin, CustClubFlag, CustClubDisc, CustClubDate, CustPinHint, CustOptInFlag, CustError, CustMessage, CustOrder, CustFirstName, CustLastName, ClubNumber, Path);
                //WriteServiceLog("Method GetPost Start" + DateTime.Now, Path);
                WriteServiceLog("Method GetPost Start" , Path);
                JavaScriptSerializer obj = new JavaScriptSerializer();
                DataTable CustInfo = CreateCustomerTable();
                string[] ArryCustInfo = new string[CustInfo.Columns.Count];
                for (int dataIndex = 0; dataIndex < CustInfo.Columns.Count; dataIndex++)
                {
                    ArryCustInfo[dataIndex] = "";
                }
                RedBackCallsService.Service1.CustomerInfo OutCutomerInfo = new RedBackCallsService.Service1.CustomerInfo();
                //if ((Title != ""))
                //{
                    RedObject rb = new RedObject();

                    rb.Open3(RedBackAccount, "OPM:Cust_Master");
                    ((RedProperty)rb.Property("CustNumber")).Value = CustNumber;
                    ((RedProperty)rb.Property("Title")).Value = Title;
                    ((RedProperty)rb.Property("CustName")).Value = CustName;
                    ((RedProperty)rb.Property("CustAdd1")).Value = CustAdd1;
                    ((RedProperty)rb.Property("CustAdd2")).Value = CustAdd2;
		    ((RedProperty)rb.Property("CustState")).Value = CustState;
                    ((RedProperty)rb.Property("CustCity")).Value = CustCity;
                    ((RedProperty)rb.Property("CustZip")).Value = CustZip;
                    ((RedProperty)rb.Property("CustCountry")).Value = CustCountry;
                    ((RedProperty)rb.Property("CustPhone")).Value = CustPhone;
                    ((RedProperty)rb.Property("CustFax")).Value = CustFax;
                    ((RedProperty)rb.Property("CustEmail")).Value = CustEmail;
                    ((RedProperty)rb.Property("CustPin")).Value = CustPin;
                    ((RedProperty)rb.Property("CustNumber")).Value = CustNumber;
                    ((RedProperty)rb.Property("CustClubFlag")).Value = CustClubFlag;
                    ((RedProperty)rb.Property("CustClubDisc")).Value = CustClubDisc;
                    ((RedProperty)rb.Property("CustClubDate")).Value = CustClubDate;
                    ((RedProperty)rb.Property("CustPinHint")).Value = CustPinHint;
                    ((RedProperty)rb.Property("CustOptInFlag")).Value = CustOptInFlag;
                    ((RedProperty)rb.Property("CustError")).Value = CustError;
                    ((RedProperty)rb.Property("CustMessage")).Value = CustMessage;
                    ((RedProperty)rb.Property("CustOrder")).Value = CustOrder;
                    ((RedProperty)rb.Property("CustFirstName")).Value = CustFirstName;
                    ((RedProperty)rb.Property("CustLastName")).Value = CustLastName;
                    ((RedProperty)rb.Property("ClubNumber")).Value = ClubNumber;

                    rb.CallMethod("AccessCust");
                    string custMsg = "Success";
                    string retu = ((RedProperty)rb.Property("CustError")).Value;
                    if (((RedProperty)rb.Property("CustError")).Value != "")
                        custMsg = ((RedProperty)rb.Property("CustMessage")).Value;

                    OutCutomerInfo.Message = custMsg;
                    OutCutomerInfo.CustNumber = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    OutCutomerInfo.Title = ((RedProperty)rb.Property("Title")).Value.ToString();
                    OutCutomerInfo.CustName = ((RedProperty)rb.Property("CustName")).Value.ToString();
                    OutCutomerInfo.CustAdd1 = ((RedProperty)rb.Property("CustAdd1")).Value.ToString();
                    OutCutomerInfo.CustAdd2 = ((RedProperty)rb.Property("CustAdd2")).Value.ToString();
		    OutCutomerInfo.CustState = ((RedProperty)rb.Property("CustState")).Value.ToString();
                    OutCutomerInfo.CustCity = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    OutCutomerInfo.CustZip = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    OutCutomerInfo.CustCountry = ((RedProperty)rb.Property("CustCountry")).Value.ToString();
                    OutCutomerInfo.CustPhone = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    OutCutomerInfo.CustFax = ((RedProperty)rb.Property("CustFax")).Value.ToString();
                    OutCutomerInfo.CustEmail = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    OutCutomerInfo.CustPin = ((RedProperty)rb.Property("CustPin")).Value.ToString();
                    OutCutomerInfo.CustNumber = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    OutCutomerInfo.CustClubFlag = ((RedProperty)rb.Property("CustClubFlag")).Value.ToString();
                    OutCutomerInfo.CustClubDisc = ((RedProperty)rb.Property("CustClubDisc")).Value.ToString();
                    OutCutomerInfo.CustClubDate = ((RedProperty)rb.Property("CustClubDate")).Value.ToString();
                    OutCutomerInfo.CustPinHint = ((RedProperty)rb.Property("CustPinHint")).Value.ToString();
                    OutCutomerInfo.CustOptInFlag = ((RedProperty)rb.Property("CustOptInFlag")).Value.ToString();
                    OutCutomerInfo.CustError = ((RedProperty)rb.Property("CustError")).Value.ToString();
                    OutCutomerInfo.CustMessage = ((RedProperty)rb.Property("CustMessage")).Value.ToString();
                    OutCutomerInfo.CustOrder = ((RedProperty)rb.Property("CustOrder")).Value.ToString();
                    OutCutomerInfo.CustFirstName = ((RedProperty)rb.Property("CustFirstName")).Value.ToString();
                    OutCutomerInfo.CustLastName = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    OutCutomerInfo.ClubNumber = ((RedProperty)rb.Property("ClubNumber")).Value.ToString();

                    ArryCustInfo[0] = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    ArryCustInfo[1] = ((RedProperty)rb.Property("Title")).Value.ToString();
                    ArryCustInfo[2] = ((RedProperty)rb.Property("CustName")).Value.ToString();
                    ArryCustInfo[3] = ((RedProperty)rb.Property("CustAdd1")).Value.ToString();
                    ArryCustInfo[4] = ((RedProperty)rb.Property("CustAdd2")).Value.ToString();
                    //ArryCustInfo[6] = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    ArryCustInfo[5] = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    ArryCustInfo[6] = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    ArryCustInfo[7] = ((RedProperty)rb.Property("CustCountry")).Value.ToString();
                    ArryCustInfo[8] = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    ArryCustInfo[9] = ((RedProperty)rb.Property("CustFax")).Value.ToString();
                    ArryCustInfo[10] = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    ArryCustInfo[11] = ((RedProperty)rb.Property("CustPin")).Value.ToString();
                    ArryCustInfo[12] = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    ArryCustInfo[13] = ((RedProperty)rb.Property("CustClubFlag")).Value.ToString();
                    ArryCustInfo[14] = ((RedProperty)rb.Property("CustClubDisc")).Value.ToString();
                    ArryCustInfo[15] = ((RedProperty)rb.Property("CustClubDate")).Value.ToString();
                    ArryCustInfo[16] = ((RedProperty)rb.Property("CustPinHint")).Value.ToString();
                    ArryCustInfo[17] = ((RedProperty)rb.Property("CustOptInFlag")).Value.ToString();
                    ArryCustInfo[18] = ((RedProperty)rb.Property("CustError")).Value.ToString();
                    ArryCustInfo[19] = ((RedProperty)rb.Property("CustMessage")).Value.ToString();
                    ArryCustInfo[20] = ((RedProperty)rb.Property("CustOrder")).Value.ToString();
                    ArryCustInfo[21] = ((RedProperty)rb.Property("CustFirstName")).Value.ToString();
                    ArryCustInfo[22] = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    ArryCustInfo[23] = ((RedProperty)rb.Property("ClubNumber")).Value.ToString();
                    ArryCustInfo[24] = ((RedProperty)rb.Property("CustState")).Value.ToString();

                //}

                CustInfo.Rows.Add(ArryCustInfo);
                OutPutParam = ParamMasterGetData(ArryCustInfo);
                string LogOn = RedBackServiceGetLogOnDetails(Path);
                StringBuilder MethodName = new StringBuilder();
                if (LogOn == "1")
                {
                    MethodName.AppendLine(" ");
                    MethodName.AppendLine("Method Name : Cust_Master");
                    MethodName.AppendLine(" ");
                    InputParam.AppendLine(" ");
                    InputParam.AppendLine("xOutPut Details");
                    InputParam.AppendLine(" ");
                    string Log = MethodName.ToString() + InputParam.ToString();
                    Log = Log + OutPutParam.ToString();
                    WriteServiceLog(Log, Path);
                    //WriteServiceLog("Method GetPost End" + DateTime.Now, Path);
                    WriteServiceLog("Method GetPost End" , Path);
                }
                return obj.Serialize(OutCutomerInfo);
            }

            //Modified by Rajesh 22/10/2012
            public string Cust_Master_NationalCity(string CustNumber, string Title, string CustName, string CustAdd1, string CustAdd2, string CustState, string CustCity, string CustZip, string CustCountry, string CustPhone, string CustFax, string CustEmail, string CustPin, string CustClubFlag, string CustClubDisc, string CustClubDate, string CustPinHint, string CustOptInFlag, string CustError, string CustMessage, string CustOrder, string CustFirstName, string CustLastName, string ClubNumber, string Path)
            {
                StringBuilder rsp = new StringBuilder();
                StringBuilder InputParam = new StringBuilder();
                StringBuilder OutPutParam = new StringBuilder();
                InputParam = ParamMasterGetData(CustNumber, Title, CustName, CustAdd1, CustAdd2, CustState, CustCity, CustZip, CustCountry, CustPhone, CustFax, CustEmail, CustPin, CustClubFlag, CustClubDisc, CustClubDate, CustPinHint, CustOptInFlag, CustError, CustMessage, CustOrder, CustFirstName, CustLastName, ClubNumber, Path);
                WriteServiceLog("Method GetPost Start" + DateTime.Now, Path);
               
                JavaScriptSerializer obj = new JavaScriptSerializer();
                DataTable CustInfo = CreateCustomerTable();
                string[] ArryCustInfo = new string[CustInfo.Columns.Count];
                for (int dataIndex = 0; dataIndex < CustInfo.Columns.Count; dataIndex++)
                {
                    ArryCustInfo[dataIndex] = "";
                }
                RedBackCallsService.Service1.CustomerInfo OutCutomerInfo = new RedBackCallsService.Service1.CustomerInfo();
                //if ((CustEmail != "") && (CustZip != ""))
                //{
                    RedObject rb = new RedObject();
                    rb.Open3(RedBackAccount, "OPM:Cust_Master");
                    ((RedProperty)rb.Property("CustNumber")).Value = CustNumber;
                    ((RedProperty)rb.Property("Title")).Value = Title;
                    ((RedProperty)rb.Property("CustName")).Value = CustName;
                    ((RedProperty)rb.Property("CustAdd1")).Value = CustAdd1;
                    ((RedProperty)rb.Property("CustAdd2")).Value = CustAdd2;
                    ((RedProperty)rb.Property("CustCity")).Value = CustCity;
                    ((RedProperty)rb.Property("CustState")).Value = CustState;
                    ((RedProperty)rb.Property("CustZip")).Value = CustZip;
                    ((RedProperty)rb.Property("CustCountry")).Value = CustCountry;
                    ((RedProperty)rb.Property("CustPhone")).Value = CustPhone;
                    ((RedProperty)rb.Property("CustFax")).Value = CustFax;
                    ((RedProperty)rb.Property("CustEmail")).Value = CustEmail;
                    ((RedProperty)rb.Property("CustPin")).Value = CustPin;
                    ((RedProperty)rb.Property("CustNumber")).Value = CustNumber;
                    ((RedProperty)rb.Property("CustClubFlag")).Value = CustClubFlag;
                    ((RedProperty)rb.Property("CustClubDisc")).Value = CustClubDisc;
                    ((RedProperty)rb.Property("CustClubDate")).Value = CustClubDate;
                    ((RedProperty)rb.Property("CustPinHint")).Value = CustPinHint;
                    ((RedProperty)rb.Property("CustOptInFlag")).Value = CustOptInFlag;
                    ((RedProperty)rb.Property("CustError")).Value = CustError;
                    ((RedProperty)rb.Property("CustMessage")).Value = CustMessage;
                    ((RedProperty)rb.Property("CustOrder")).Value = CustOrder;
                    ((RedProperty)rb.Property("CustFirstName")).Value = CustFirstName;
                    ((RedProperty)rb.Property("CustLastName")).Value = CustLastName;
                    ((RedProperty)rb.Property("ClubNumber")).Value = ClubNumber;

                    rb.CallMethod("NationalCity");
                    string custMsg = "Success";
                    string retu = ((RedProperty)rb.Property("CustError")).Value;
                    if (((RedProperty)rb.Property("CustError")).Value != "")
                        custMsg = ((RedProperty)rb.Property("CustMessage")).Value;

                    OutCutomerInfo.Message = custMsg;
                    OutCutomerInfo.CustNumber = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    OutCutomerInfo.Title = ((RedProperty)rb.Property("Title")).Value.ToString();
                    OutCutomerInfo.CustName = ((RedProperty)rb.Property("CustName")).Value.ToString();
                    OutCutomerInfo.CustAdd1 = ((RedProperty)rb.Property("CustAdd1")).Value.ToString();
                    OutCutomerInfo.CustAdd2 = ((RedProperty)rb.Property("CustAdd2")).Value.ToString();
                    OutCutomerInfo.CustState = ((RedProperty)rb.Property("CustState")).Value.ToString();
                    OutCutomerInfo.CustCity = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    OutCutomerInfo.CustZip = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    OutCutomerInfo.CustCountry = ((RedProperty)rb.Property("CustCountry")).Value.ToString();
                    OutCutomerInfo.CustPhone = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    OutCutomerInfo.CustFax = ((RedProperty)rb.Property("CustFax")).Value.ToString();
                    OutCutomerInfo.CustEmail = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    OutCutomerInfo.CustPin = ((RedProperty)rb.Property("CustPin")).Value.ToString();
                    OutCutomerInfo.CustNumber = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    OutCutomerInfo.CustClubFlag = ((RedProperty)rb.Property("CustClubFlag")).Value.ToString();
                    OutCutomerInfo.CustClubDisc = ((RedProperty)rb.Property("CustClubDisc")).Value.ToString();
                    OutCutomerInfo.CustClubDate = ((RedProperty)rb.Property("CustClubDate")).Value.ToString();
                    OutCutomerInfo.CustPinHint = ((RedProperty)rb.Property("CustPinHint")).Value.ToString();
                    OutCutomerInfo.CustOptInFlag = ((RedProperty)rb.Property("CustOptInFlag")).Value.ToString();
                    OutCutomerInfo.CustError = ((RedProperty)rb.Property("CustError")).Value.ToString();
                    OutCutomerInfo.CustMessage = ((RedProperty)rb.Property("CustMessage")).Value.ToString();
                    OutCutomerInfo.CustOrder = ((RedProperty)rb.Property("CustOrder")).Value.ToString();
                    OutCutomerInfo.CustFirstName = ((RedProperty)rb.Property("CustFirstName")).Value.ToString();
                    OutCutomerInfo.CustLastName = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    OutCutomerInfo.ClubNumber = ((RedProperty)rb.Property("ClubNumber")).Value.ToString();

                    ArryCustInfo[1] = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    ArryCustInfo[2] = ((RedProperty)rb.Property("Title")).Value.ToString();
                    ArryCustInfo[3] = ((RedProperty)rb.Property("CustName")).Value.ToString();
                    ArryCustInfo[4] = ((RedProperty)rb.Property("CustAdd1")).Value.ToString();
                    ArryCustInfo[5] = ((RedProperty)rb.Property("CustAdd2")).Value.ToString();
                    ArryCustInfo[6] = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    ArryCustInfo[7] = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    ArryCustInfo[8] = ((RedProperty)rb.Property("CustCountry")).Value.ToString();
                    ArryCustInfo[9] = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    ArryCustInfo[10] = ((RedProperty)rb.Property("CustFax")).Value.ToString();
                    ArryCustInfo[11] = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    ArryCustInfo[12] = ((RedProperty)rb.Property("CustPin")).Value.ToString();
                    ArryCustInfo[13] = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    ArryCustInfo[14] = ((RedProperty)rb.Property("CustClubFlag")).Value.ToString();
                    ArryCustInfo[15] = ((RedProperty)rb.Property("CustClubDisc")).Value.ToString();
                    ArryCustInfo[16] = ((RedProperty)rb.Property("CustClubDate")).Value.ToString();
                    ArryCustInfo[17] = ((RedProperty)rb.Property("CustPinHint")).Value.ToString();
                    ArryCustInfo[18] = ((RedProperty)rb.Property("CustOptInFlag")).Value.ToString();
                    ArryCustInfo[19] = ((RedProperty)rb.Property("CustError")).Value.ToString();
                    ArryCustInfo[20] = ((RedProperty)rb.Property("CustMessage")).Value.ToString();
                    ArryCustInfo[21] = ((RedProperty)rb.Property("CustOrder")).Value.ToString();
                    ArryCustInfo[22] = ((RedProperty)rb.Property("CustFirstName")).Value.ToString();
                    ArryCustInfo[23] = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    ArryCustInfo[24] = ((RedProperty)rb.Property("ClubNumber")).Value.ToString();
                    ArryCustInfo[25] = ((RedProperty)rb.Property("CustState")).Value.ToString();
                //}
                CustInfo.Rows.Add(ArryCustInfo);
                OutPutParam = ParamMasterGetData(ArryCustInfo);
                string LogOn = RedBackServiceGetLogOnDetails(Path);
                StringBuilder MethodName = new StringBuilder();
                if (LogOn == "1")
                {
                    MethodName.AppendLine(" ");
                    MethodName.AppendLine("Method Name : Cust_Master");
                    MethodName.AppendLine(" ");
                    InputParam.AppendLine(" ");
                    InputParam.AppendLine("OutPut Details");
                    InputParam.AppendLine(" ");
                    string Log = MethodName.ToString() + InputParam.ToString();
                    Log = Log + OutPutParam.ToString();
                    WriteServiceLog(Log, Path);
                    //WriteServiceLog("Method GetPost End" + DateTime.Now, Path);
                    WriteServiceLog("Method GetPost End", Path);
                }
                return obj.Serialize(OutCutomerInfo);
            }

            public string Cust_Master_UpdateCust(string CustNumber, string Title, string CustName, string CustAdd1, string CustAdd2, string CustState, string CustCity, string CustZip, string CustCountry, string CustPhone, string CustFax, string CustEmail, string CustPin, string CustClubFlag, string CustClubDisc, string CustClubDate, string CustPinHint, string CustOptInFlag, string CustError, string CustMessage, string CustOrder, string CustFirstName, string CustLastName, string ClubNumber, string Path)
            {
                StringBuilder rsp = new StringBuilder();
                StringBuilder InputParam = new StringBuilder();
                StringBuilder OutPutParam = new StringBuilder();
                InputParam = ParamMasterGetData(CustNumber, Title, CustName, CustAdd1, CustAdd2, CustState, CustCity, CustZip, CustCountry, CustPhone, CustFax, CustEmail, CustPin, CustClubFlag, CustClubDisc, CustClubDate, CustPinHint, CustOptInFlag, CustError, CustMessage, CustOrder, CustFirstName, CustLastName, ClubNumber, Path);
                WriteServiceLog("Method GetPost Start" + DateTime.Now, Path);
                
                JavaScriptSerializer obj = new JavaScriptSerializer();
                DataTable CustInfo = CreateCustomerTable();
                string[] ArryCustInfo = new string[CustInfo.Columns.Count];
                for (int dataIndex = 0; dataIndex < CustInfo.Columns.Count; dataIndex++)
                {
                    ArryCustInfo[dataIndex] = "";
                }
                RedBackCallsService.Service1.CustomerInfo OutCutomerInfo = new RedBackCallsService.Service1.CustomerInfo();
               // if ((CustEmail != "") && (CustPin != ""))
                //{
                    RedObject rb = new RedObject();
                    rb.Open3(RedBackAccount, "OPM:Cust_Master");
                    ((RedProperty)rb.Property("CustNumber")).Value = CustNumber;
                    ((RedProperty)rb.Property("Title")).Value = Title;
                    ((RedProperty)rb.Property("CustName")).Value = CustName;
                    ((RedProperty)rb.Property("CustAdd1")).Value = CustAdd1;
                    ((RedProperty)rb.Property("CustAdd2")).Value = CustAdd2;
                    ((RedProperty)rb.Property("CustState")).Value = CustState;
                    ((RedProperty)rb.Property("CustCity")).Value = CustCity;
                    ((RedProperty)rb.Property("CustZip")).Value = CustZip;
                    ((RedProperty)rb.Property("CustCountry")).Value = CustCountry;
                    ((RedProperty)rb.Property("CustPhone")).Value = CustPhone;
                    ((RedProperty)rb.Property("CustFax")).Value = CustFax;
                    ((RedProperty)rb.Property("CustEmail")).Value = CustEmail;
                    ((RedProperty)rb.Property("CustPin")).Value = CustPin;
                    ((RedProperty)rb.Property("CustNumber")).Value = CustNumber;
                    ((RedProperty)rb.Property("CustClubFlag")).Value = CustClubFlag;
                    ((RedProperty)rb.Property("CustClubDisc")).Value = CustClubDisc;
                    ((RedProperty)rb.Property("CustClubDate")).Value = CustClubDate;
                    ((RedProperty)rb.Property("CustPinHint")).Value = CustPinHint;
                    ((RedProperty)rb.Property("CustOptInFlag")).Value = CustOptInFlag;
                    ((RedProperty)rb.Property("CustError")).Value = CustError;
                    ((RedProperty)rb.Property("CustMessage")).Value = CustMessage;
                    ((RedProperty)rb.Property("CustOrder")).Value = CustOrder;
                    ((RedProperty)rb.Property("CustFirstName")).Value = CustFirstName;
                    ((RedProperty)rb.Property("CustLastName")).Value = CustLastName;
                    ((RedProperty)rb.Property("ClubNumber")).Value = ClubNumber;
                    rb.CallMethod("UpdateCust");
                    string custMsg = "Success";
                    string retu = ((RedProperty)rb.Property("CustError")).Value;
                    if (((RedProperty)rb.Property("CustError")).Value != "")
                        custMsg = ((RedProperty)rb.Property("CustMessage")).Value;

                    OutCutomerInfo.Message = custMsg;
                    OutCutomerInfo.CustNumber = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    OutCutomerInfo.Title = ((RedProperty)rb.Property("Title")).Value.ToString();
                    OutCutomerInfo.CustName = ((RedProperty)rb.Property("CustName")).Value.ToString();
                    OutCutomerInfo.CustAdd1 = ((RedProperty)rb.Property("CustAdd1")).Value.ToString();
                    OutCutomerInfo.CustAdd2 = ((RedProperty)rb.Property("CustAdd2")).Value.ToString();
                    OutCutomerInfo.CustState = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    OutCutomerInfo.CustCity = ((RedProperty)rb.Property("CustState")).Value.ToString();
                    OutCutomerInfo.CustZip = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    OutCutomerInfo.CustCountry = ((RedProperty)rb.Property("CustCountry")).Value.ToString();
                    OutCutomerInfo.CustPhone = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    OutCutomerInfo.CustFax = ((RedProperty)rb.Property("CustFax")).Value.ToString();
                    OutCutomerInfo.CustEmail = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    OutCutomerInfo.CustPin = ((RedProperty)rb.Property("CustPin")).Value.ToString();
                    OutCutomerInfo.CustNumber = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    OutCutomerInfo.CustClubFlag = ((RedProperty)rb.Property("CustClubFlag")).Value.ToString();
                    OutCutomerInfo.CustClubDisc = ((RedProperty)rb.Property("CustClubDisc")).Value.ToString();
                    OutCutomerInfo.CustClubDate = ((RedProperty)rb.Property("CustClubDate")).Value.ToString();
                    OutCutomerInfo.CustPinHint = ((RedProperty)rb.Property("CustPinHint")).Value.ToString();
                    OutCutomerInfo.CustOptInFlag = ((RedProperty)rb.Property("CustOptInFlag")).Value.ToString();
                    OutCutomerInfo.CustError = ((RedProperty)rb.Property("CustError")).Value.ToString();
                    OutCutomerInfo.CustMessage = ((RedProperty)rb.Property("CustMessage")).Value.ToString();
                    OutCutomerInfo.CustOrder = ((RedProperty)rb.Property("CustOrder")).Value.ToString();
                    OutCutomerInfo.CustFirstName = ((RedProperty)rb.Property("CustFirstName")).Value.ToString();
                    OutCutomerInfo.CustLastName = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    OutCutomerInfo.ClubNumber = ((RedProperty)rb.Property("ClubNumber")).Value.ToString();

                    ArryCustInfo[1] = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    ArryCustInfo[2] = ((RedProperty)rb.Property("Title")).Value.ToString();
                    ArryCustInfo[3] = ((RedProperty)rb.Property("CustName")).Value.ToString();
                    ArryCustInfo[4] = ((RedProperty)rb.Property("CustAdd1")).Value.ToString();
                    ArryCustInfo[5] = ((RedProperty)rb.Property("CustAdd2")).Value.ToString();
                    ArryCustInfo[6] = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    ArryCustInfo[7] = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    ArryCustInfo[8] = ((RedProperty)rb.Property("CustCountry")).Value.ToString();
                    ArryCustInfo[9] = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    ArryCustInfo[10] = ((RedProperty)rb.Property("CustFax")).Value.ToString();
                    ArryCustInfo[11] = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    ArryCustInfo[12] = ((RedProperty)rb.Property("CustPin")).Value.ToString();
                    ArryCustInfo[13] = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    ArryCustInfo[14] = ((RedProperty)rb.Property("CustClubFlag")).Value.ToString();
                    ArryCustInfo[15] = ((RedProperty)rb.Property("CustClubDisc")).Value.ToString();
                    ArryCustInfo[16] = ((RedProperty)rb.Property("CustClubDate")).Value.ToString();
                    ArryCustInfo[17] = ((RedProperty)rb.Property("CustPinHint")).Value.ToString();
                    ArryCustInfo[18] = ((RedProperty)rb.Property("CustOptInFlag")).Value.ToString();
                    ArryCustInfo[19] = ((RedProperty)rb.Property("CustError")).Value.ToString();
                    ArryCustInfo[20] = ((RedProperty)rb.Property("CustMessage")).Value.ToString();
                    ArryCustInfo[21] = ((RedProperty)rb.Property("CustOrder")).Value.ToString();
                    ArryCustInfo[22] = ((RedProperty)rb.Property("CustFirstName")).Value.ToString();
                    ArryCustInfo[23] = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    ArryCustInfo[24] = ((RedProperty)rb.Property("ClubNumber")).Value.ToString();
                    ArryCustInfo[25] = ((RedProperty)rb.Property("CustState")).Value.ToString();
                //}
                CustInfo.Rows.Add(ArryCustInfo);
                OutPutParam = ParamMasterGetData(ArryCustInfo);
                string LogOn = RedBackServiceGetLogOnDetails(Path);
                StringBuilder MethodName = new StringBuilder();
                if (LogOn == "1")
                {
                    MethodName.AppendLine(" ");
                    MethodName.AppendLine("Method Name : Cust_Master");
                    MethodName.AppendLine(" ");
                    InputParam.AppendLine(" ");
                    InputParam.AppendLine("OutPut Details");
                    InputParam.AppendLine(" ");
                    string Log = MethodName.ToString() + InputParam.ToString();
                    Log = Log + OutPutParam.ToString();
                    WriteServiceLog(Log, Path);
                    //WriteServiceLog("Method GetPost End" + DateTime.Now, Path);
                    WriteServiceLog("Method GetPost End", Path);
                }
                return obj.Serialize(OutCutomerInfo);
            }

            public string Cust_Master_AccessCustTest(string CustNumber, string Title, string CustName, string CustAdd1, string CustAdd2, string CustState, string CustCity, string CustZip, string CustCountry, string CustPhone, string CustFax, string CustEmail, string CustPin, string CustClubFlag, string CustClubDisc, string CustClubDate, string CustPinHint, string CustOptInFlag, string CustError, string CustMessage, string CustOrder, string CustFirstName, string CustLastName, string ClubNumber, string Path)
            {
                StringBuilder rsp = new StringBuilder();
                StringBuilder InputParam = new StringBuilder();
                StringBuilder OutPutParam = new StringBuilder();
                InputParam = ParamMasterGetData(CustNumber, Title, CustName, CustAdd1, CustAdd2, CustState, CustCity, CustZip, CustCountry, CustPhone, CustFax, CustEmail, CustPin, CustClubFlag, CustClubDisc, CustClubDate, CustPinHint, CustOptInFlag, CustError, CustMessage, CustOrder, CustFirstName, CustLastName, ClubNumber, Path);
                WriteServiceLog("Method GetPost Start" + DateTime.Now, Path);
                JavaScriptSerializer obj = new JavaScriptSerializer();
                DataTable CustInfo = CreateCustomerTable();
                string[] ArryCustInfo = new string[CustInfo.Columns.Count];
                for (int dataIndex = 0; dataIndex < CustInfo.Columns.Count; dataIndex++)
                {
                    ArryCustInfo[dataIndex] = "";
                }
                RedBackCallsService.Service1.CustomerInfo OutCutomerInfo = new RedBackCallsService.Service1.CustomerInfo();
                //if ((CustEmail != ""))
                //{
                    RedObject rb = new RedObject();
                    rb.Open3(RedBackAccount, "OPM:Cust_Master");
                    ((RedProperty)rb.Property("CustNumber")).Value = CustNumber;
                    ((RedProperty)rb.Property("Title")).Value = Title;
                    ((RedProperty)rb.Property("CustName")).Value = CustName;
                    ((RedProperty)rb.Property("CustAdd1")).Value = CustAdd1;
                    ((RedProperty)rb.Property("CustAdd2")).Value = CustAdd2;
		    ((RedProperty)rb.Property("CustState")).Value = CustState;
                    ((RedProperty)rb.Property("CustCity")).Value = CustCity;
                    ((RedProperty)rb.Property("CustZip")).Value = CustZip;
                    ((RedProperty)rb.Property("CustCountry")).Value = CustCountry;
                    ((RedProperty)rb.Property("CustPhone")).Value = CustPhone;
                    ((RedProperty)rb.Property("CustFax")).Value = CustFax;
                    ((RedProperty)rb.Property("CustEmail")).Value = CustEmail;
                    ((RedProperty)rb.Property("CustPin")).Value = CustPin;
                    ((RedProperty)rb.Property("CustNumber")).Value = CustNumber;
                    ((RedProperty)rb.Property("CustClubFlag")).Value = CustClubFlag;
                    ((RedProperty)rb.Property("CustClubDisc")).Value = CustClubDisc;
                    ((RedProperty)rb.Property("CustClubDate")).Value = CustClubDate;
                    ((RedProperty)rb.Property("CustPinHint")).Value = CustPinHint;
                    ((RedProperty)rb.Property("CustOptInFlag")).Value = CustOptInFlag;
                    ((RedProperty)rb.Property("CustError")).Value = CustError;
                    ((RedProperty)rb.Property("CustMessage")).Value = CustMessage;
                    ((RedProperty)rb.Property("CustOrder")).Value = CustOrder;
                    ((RedProperty)rb.Property("CustFirstName")).Value = CustFirstName;
                    ((RedProperty)rb.Property("CustLastName")).Value = CustLastName;
                    ((RedProperty)rb.Property("ClubNumber")).Value = ClubNumber;
                    rb.CallMethod("AccessCustTest");
                    string custMsg = "Success";
                    string retu = ((RedProperty)rb.Property("CustError")).Value;
                    if (((RedProperty)rb.Property("CustError")).Value != "")
                        custMsg = ((RedProperty)rb.Property("CustMessage")).Value;

                    OutCutomerInfo.Message = custMsg;
                    OutCutomerInfo.CustNumber = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    OutCutomerInfo.Title = ((RedProperty)rb.Property("Title")).Value.ToString();
                    OutCutomerInfo.CustName = ((RedProperty)rb.Property("CustName")).Value.ToString();
                    OutCutomerInfo.CustAdd1 = ((RedProperty)rb.Property("CustAdd1")).Value.ToString();
                    OutCutomerInfo.CustAdd2 = ((RedProperty)rb.Property("CustAdd2")).Value.ToString();
                    OutCutomerInfo.CustState = ((RedProperty)rb.Property("CustState")).Value.ToString();
                    OutCutomerInfo.CustCity = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    OutCutomerInfo.CustZip = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    OutCutomerInfo.CustCountry = ((RedProperty)rb.Property("CustCountry")).Value.ToString();
                    OutCutomerInfo.CustPhone = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    OutCutomerInfo.CustFax = ((RedProperty)rb.Property("CustFax")).Value.ToString();
                    OutCutomerInfo.CustEmail = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    OutCutomerInfo.CustPin = ((RedProperty)rb.Property("CustPin")).Value.ToString();
                    OutCutomerInfo.CustNumber = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    OutCutomerInfo.CustClubFlag = ((RedProperty)rb.Property("CustClubFlag")).Value.ToString();
                    OutCutomerInfo.CustClubDisc = ((RedProperty)rb.Property("CustClubDisc")).Value.ToString();
                    OutCutomerInfo.CustClubDate = ((RedProperty)rb.Property("CustClubDate")).Value.ToString();
                    OutCutomerInfo.CustPinHint = ((RedProperty)rb.Property("CustPinHint")).Value.ToString();
                    OutCutomerInfo.CustOptInFlag = ((RedProperty)rb.Property("CustOptInFlag")).Value.ToString();
                    OutCutomerInfo.CustError = ((RedProperty)rb.Property("CustError")).Value.ToString();
                    OutCutomerInfo.CustMessage = ((RedProperty)rb.Property("CustMessage")).Value.ToString();
                    OutCutomerInfo.CustOrder = ((RedProperty)rb.Property("CustOrder")).Value.ToString();
                    OutCutomerInfo.CustFirstName = ((RedProperty)rb.Property("CustFirstName")).Value.ToString();
                    OutCutomerInfo.CustLastName = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    OutCutomerInfo.ClubNumber = ((RedProperty)rb.Property("ClubNumber")).Value.ToString();

                    ArryCustInfo[1] = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    ArryCustInfo[2] = ((RedProperty)rb.Property("Title")).Value.ToString();
                    ArryCustInfo[3] = ((RedProperty)rb.Property("CustName")).Value.ToString();
                    ArryCustInfo[4] = ((RedProperty)rb.Property("CustAdd1")).Value.ToString();
                    ArryCustInfo[5] = ((RedProperty)rb.Property("CustAdd2")).Value.ToString();
                    ArryCustInfo[6] = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    ArryCustInfo[7] = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    ArryCustInfo[8] = ((RedProperty)rb.Property("CustCountry")).Value.ToString();
                    ArryCustInfo[9] = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    ArryCustInfo[10] = ((RedProperty)rb.Property("CustFax")).Value.ToString();
                    ArryCustInfo[11] = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    ArryCustInfo[12] = ((RedProperty)rb.Property("CustPin")).Value.ToString();
                    ArryCustInfo[13] = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    ArryCustInfo[14] = ((RedProperty)rb.Property("CustClubFlag")).Value.ToString();
                    ArryCustInfo[15] = ((RedProperty)rb.Property("CustClubDisc")).Value.ToString();
                    ArryCustInfo[16] = ((RedProperty)rb.Property("CustClubDate")).Value.ToString();
                    ArryCustInfo[17] = ((RedProperty)rb.Property("CustPinHint")).Value.ToString();
                    ArryCustInfo[18] = ((RedProperty)rb.Property("CustOptInFlag")).Value.ToString();
                    ArryCustInfo[19] = ((RedProperty)rb.Property("CustError")).Value.ToString();
                    ArryCustInfo[20] = ((RedProperty)rb.Property("CustMessage")).Value.ToString();
                    ArryCustInfo[21] = ((RedProperty)rb.Property("CustOrder")).Value.ToString();
                    ArryCustInfo[22] = ((RedProperty)rb.Property("CustFirstName")).Value.ToString();
                    ArryCustInfo[23] = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    ArryCustInfo[24] = ((RedProperty)rb.Property("ClubNumber")).Value.ToString();
                    ArryCustInfo[25] = ((RedProperty)rb.Property("CustState")).Value.ToString();
                //}
                CustInfo.Rows.Add(ArryCustInfo);
                OutPutParam = ParamMasterGetData(ArryCustInfo);
                string LogOn = RedBackServiceGetLogOnDetails(Path);
                StringBuilder MethodName = new StringBuilder();
                if (LogOn == "1")
                {
                    MethodName.AppendLine(" ");
                    MethodName.AppendLine("Method Name : Cust_Master");
                    MethodName.AppendLine(" ");
                    InputParam.AppendLine(" ");
                    InputParam.AppendLine("OutPut Details");
                    InputParam.AppendLine(" ");
                    string Log = MethodName.ToString() + InputParam.ToString();
                    Log = Log + OutPutParam.ToString();
                    WriteServiceLog(Log, Path);
                    //WriteServiceLog("Method GetPost End" + DateTime.Now, Path);
                    WriteServiceLog("Method GetPost End", Path);
                }
                return obj.Serialize(OutCutomerInfo);
            }

            //Code Block changes End
            //Rajesh 22/10/2012
            public string Order_New(string OrderNumber, string CustFirstName, string CustLastName, string CustAdd1, string CustAdd2, string CustAdd3, string CustCity, string CustState, string CustZip, string CustCountry,
                                string CustPhoneDay, string CustPhoneEve, string CustEmail1, string CustEmail2, string CustPin, string ShipFirstName, string ShipLastName, string ShipAdd1, string ShipAdd2, string ShipAdd3, string ShipCity, string ShipState, string ShipZip, string ShipCountry,
                                string ShipPhone, string ShipEmail, string ShipAttention, string CustNumber, string WebReference, string PromoCode, string Title, string ShipMethod, string PaymentMethod,
                                string CreditCardNumber, string CardExpDate, string CardCVV, string CardAddress, string CardZip, string MerchAmount, string CouponAmount, string DiscAmount, string ShipAmount, string PremShipAmount,
                                string OverweightAmount, string TaxAmount, string TotalAmount, string Comments, string Items, string QtyOrdered, string UnitPrice,
                                string ReleaseDate, string FreeFlag, string PersonalizationCode, string PersonalizationDetail1, string PersonalizationDetail2, string PersonalizationDetail3, string PersonalizationDetail4,
                                string PersonalizationDetail5, string PIN, string PINHint, string OptInFlag, string OptInDate, string OptOutDate, string Etrack, string IPSource,
                                string GVCFlag, string GVCDate, string ValidErrCode, string ValidErrMsg, string OrderErrCode, string OrderErrMsg, string Cert1_Number, string Cert1_Amount, string Cert2_Number, string Cert2_Amount, string Cert3_Number,
                                string Cert3_Amount, string Repc1_Number, string Repc1_Amount, string Repc2_Number, string Repc2_Amount, string Repc3_Number, string Repc3_Amount, string Web_Date, string Web_Time, string Path, string MobilePhone, string MobileOptin)
            {
                StringBuilder rsp = new StringBuilder();
                StringBuilder InputParam = new StringBuilder();
                StringBuilder OutPutParam = new StringBuilder();
                InputParam = OrderNewGetData(OrderNumber, CustFirstName, CustLastName, CustAdd1, CustAdd2, CustAdd3, CustCity, CustState, CustZip, CustCountry, CustPhoneDay, CustPhoneEve, CustEmail1, CustEmail2,
                                                CustPin, ShipFirstName, ShipLastName, ShipAdd1, ShipAdd2, ShipAdd3, ShipCity, ShipState, ShipZip, ShipCountry,
                                                ShipPhone, ShipEmail, ShipAttention, CustNumber, WebReference, PromoCode, Title, ShipMethod, PaymentMethod,
                                                CreditCardNumber, CardExpDate, CardCVV, CardAddress, CardZip, MerchAmount, CouponAmount, DiscAmount, ShipAmount, PremShipAmount,
                                                OverweightAmount, TaxAmount, TotalAmount, Comments, Items, QtyOrdered, UnitPrice,
                                                ReleaseDate, FreeFlag, PersonalizationCode, PersonalizationDetail1, PersonalizationDetail2, PersonalizationDetail3, PersonalizationDetail4,
                                                PersonalizationDetail5, PIN, PINHint, OptInFlag, OptInDate, OptOutDate, Etrack, IPSource,
                                                GVCFlag, GVCDate, ValidErrCode, ValidErrMsg, OrderErrCode, OrderErrMsg, Cert1_Number, Cert1_Amount, Cert2_Number, Cert2_Amount, Cert3_Number,
                                                Cert3_Amount, Repc1_Number, Repc1_Amount, Repc2_Number, Repc2_Amount, Repc3_Number, Repc3_Amount, Web_Date, Web_Time, Path, MobilePhone, MobileOptin);
                WriteServiceLog("Method GetPost Start" + DateTime.Now, Path);
                JavaScriptSerializer obj = new JavaScriptSerializer();
                DataTable CustInfo = CreateOrderNewTable();
                string[] ArryCustInfo = new string[CustInfo.Columns.Count];
                for (int dataIndex = 0; dataIndex < CustInfo.Columns.Count; dataIndex++)
                {
                    ArryCustInfo[dataIndex] = "";
                }
                RedBackCallsService.Service1.OrderNew OutCutomerInfo = new RedBackCallsService.Service1.OrderNew();
                //if ((CustFirstName != ""))// && (custZip != ""))
                //{
                    RedObject rb = new RedObject();
                    rb.Open3(RedBackAccount, "OPM:Order_New");

                    ((RedProperty)rb.Property("CustFirstName")).Value = CustFirstName;
                    ((RedProperty)rb.Property("CustLastName")).Value = CustLastName;
                    ((RedProperty)rb.Property("CustAdd1")).Value = CustAdd1;
                    ((RedProperty)rb.Property("CustAdd2")).Value = CustAdd2;
                    ((RedProperty)rb.Property("CustAdd3")).Value = CustAdd3;
                    ((RedProperty)rb.Property("CustCity")).Value = CustCity;
                    ((RedProperty)rb.Property("CustState")).Value = CustState;
                    ((RedProperty)rb.Property("CustZip")).Value = CustZip;
                    ((RedProperty)rb.Property("CustCountry")).Value = CustCountry;
                    ((RedProperty)rb.Property("CustPhoneDay")).Value = CustPhoneDay;
                    ((RedProperty)rb.Property("CustPhoneEve")).Value = CustPhoneEve;
                    ((RedProperty)rb.Property("CustEmail1")).Value = CustEmail1;
                    ((RedProperty)rb.Property("CustEmail2")).Value = CustEmail2;
                    ((RedProperty)rb.Property("CustPin")).Value = CustPin;
                    ((RedProperty)rb.Property("ShipFirstName")).Value = ShipFirstName;
                    ((RedProperty)rb.Property("ShipLastName")).Value = ShipLastName;
                    ((RedProperty)rb.Property("ShipAdd1")).Value = ShipAdd1;
                    ((RedProperty)rb.Property("ShipAdd2")).Value = ShipAdd2;
                    ((RedProperty)rb.Property("ShipAdd3")).Value = ShipAdd3;
                    ((RedProperty)rb.Property("ShipCity")).Value = ShipCity;
                    ((RedProperty)rb.Property("ShipState")).Value = ShipState;
                    ((RedProperty)rb.Property("ShipZip")).Value = ShipZip;
                    ((RedProperty)rb.Property("ShipCountry")).Value = ShipCountry;
                    ((RedProperty)rb.Property("ShipPhone")).Value = ShipPhone;
                    ((RedProperty)rb.Property("ShipEmail")).Value = ShipEmail;
                    ((RedProperty)rb.Property("ShipAttention")).Value = ShipAttention;
                    ((RedProperty)rb.Property("CustNumber")).Value = CustNumber;
                    ((RedProperty)rb.Property("WebReference")).Value = WebReference;
                    ((RedProperty)rb.Property("PromoCode")).Value = PromoCode;
                    ((RedProperty)rb.Property("Title")).Value = Title;
                    ((RedProperty)rb.Property("ShipMethod")).Value = ShipMethod;
                    ((RedProperty)rb.Property("PaymentMethod")).Value = PaymentMethod;                    
                    ((RedProperty)rb.Property("CreditCardNumber")).Value = CreditCardNumber;
                    ((RedProperty)rb.Property("CardExpDate")).Value = CardExpDate;
                    ((RedProperty)rb.Property("CardCVV")).Value = CardCVV;
                    ((RedProperty)rb.Property("CardAddress")).Value = CardAddress;
                    ((RedProperty)rb.Property("CardZip")).Value = CardZip;
                    ((RedProperty)rb.Property("MerchAmount")).Value = MerchAmount;
                    ((RedProperty)rb.Property("CouponAmount")).Value = CouponAmount;
                    ((RedProperty)rb.Property("DiscAmount")).Value = DiscAmount;
                    ((RedProperty)rb.Property("ShipAmount")).Value = ShipAmount;
                    ((RedProperty)rb.Property("PremShipAmount")).Value = PremShipAmount;
                    ((RedProperty)rb.Property("OverweightAmount")).Value = OverweightAmount;
                    ((RedProperty)rb.Property("TaxAmount")).Value = TaxAmount;
                    ((RedProperty)rb.Property("TotalAmount")).Value = TotalAmount;
                    ((RedProperty)rb.Property("Comments")).Value = Comments;
                    ((RedProperty)rb.Property("Items")).Value = Items;
                    ((RedProperty)rb.Property("QtyOrdered")).Value = QtyOrdered;
                    ((RedProperty)rb.Property("UnitPrice")).Value = UnitPrice;
                    ((RedProperty)rb.Property("ReleaseDate")).Value = ReleaseDate;
                    ((RedProperty)rb.Property("FreeFlag")).Value = FreeFlag;
                    ((RedProperty)rb.Property("PersonalizationCode")).Value = PersonalizationCode;
                    ((RedProperty)rb.Property("PersonalizationDetail1")).Value = PersonalizationDetail1;
                    ((RedProperty)rb.Property("PersonalizationDetail2")).Value = PersonalizationDetail2;
                    ((RedProperty)rb.Property("PersonalizationDetail3")).Value = PersonalizationDetail3;
                    ((RedProperty)rb.Property("PersonalizationDetail4")).Value = PersonalizationDetail4;
                    ((RedProperty)rb.Property("PersonalizationDetail5")).Value = PersonalizationDetail5;
                    ((RedProperty)rb.Property("PIN")).Value = PIN;
                    ((RedProperty)rb.Property("PINHint")).Value = PINHint;
                    ((RedProperty)rb.Property("OptInFlag")).Value = OptInFlag;
                    ((RedProperty)rb.Property("OptInDate")).Value = OptInDate;
                    ((RedProperty)rb.Property("OptOutDate")).Value = OptOutDate;
                    ((RedProperty)rb.Property("Etrack")).Value = Etrack;
                    ((RedProperty)rb.Property("IPSource")).Value = IPSource;
                    ((RedProperty)rb.Property("GVCFlag")).Value = GVCFlag;
                    ((RedProperty)rb.Property("GVCDate")).Value = GVCDate;
                    ((RedProperty)rb.Property("ValidErrCode")).Value = ValidErrCode;
                    ((RedProperty)rb.Property("ValidErrMsg")).Value = ValidErrMsg;
                    ((RedProperty)rb.Property("OrderErrCode")).Value = OrderErrCode;
                    ((RedProperty)rb.Property("OrderErrMsg")).Value = OrderErrMsg;
                    ((RedProperty)rb.Property("Cert1_Number")).Value = Cert1_Number;
                    ((RedProperty)rb.Property("Cert1_Amount")).Value = Cert1_Amount;
                    ((RedProperty)rb.Property("Cert2_Number")).Value = Cert2_Number;
                    ((RedProperty)rb.Property("Cert2_Amount")).Value = Cert2_Amount;
                    ((RedProperty)rb.Property("Cert3_Number")).Value = Cert3_Number;
                    ((RedProperty)rb.Property("Cert3_Amount")).Value = Cert3_Amount;
                    ((RedProperty)rb.Property("Repc1_Number")).Value = Repc1_Number;
                    ((RedProperty)rb.Property("Repc1_Amount")).Value = Repc1_Amount;
                    ((RedProperty)rb.Property("Repc2_Number")).Value = Repc2_Number;
                    ((RedProperty)rb.Property("Repc2_Amount")).Value = Repc2_Amount;
                    ((RedProperty)rb.Property("Repc3_Number")).Value = Repc3_Number;
                    ((RedProperty)rb.Property("Repc3_Amount")).Value = Repc3_Amount;
                    ((RedProperty)rb.Property("Web_Date")).Value = Web_Date;
                    ((RedProperty)rb.Property("Web_Time")).Value = Web_Time;
                    ((RedProperty)rb.Property("OrderNumber")).Value = OrderNumber;
                    ((RedProperty)rb.Property("MobilePhone")).Value = MobilePhone;  //added by aditya 15-nov-2012
                    ((RedProperty)rb.Property("MobileOptin")).Value = MobileOptin;  //end by aditya 15-nov-2012


                    rb.CallMethod("AddOrder");
                    string custMsg = "Success";
                    string retu = ((RedProperty)rb.Property("StatusError")).Value;
                    if (((RedProperty)rb.Property("StatusError")).Value != "")
                        custMsg = ((RedProperty)rb.Property("StatusMessage")).Value;

                    OutCutomerInfo.Message = custMsg;
                    OutCutomerInfo.OrderNumber = ((RedProperty)rb.Property("OrderNumber")).Value.ToString();
                    OutCutomerInfo.CustFirstName = ((RedProperty)rb.Property("CustFirstName")).Value;
                    OutCutomerInfo.CustLastName = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    OutCutomerInfo.CustAdd1 = ((RedProperty)rb.Property("CustAdd1")).Value.ToString();
                    OutCutomerInfo.CustAdd2 = ((RedProperty)rb.Property("CustAdd2")).Value.ToString();
                    OutCutomerInfo.CustAdd3 = ((RedProperty)rb.Property("CustAdd3")).Value.ToString();
                    OutCutomerInfo.CustCity = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    OutCutomerInfo.CustState = ((RedProperty)rb.Property("CustState")).Value.ToString();
                    OutCutomerInfo.CustZip = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    OutCutomerInfo.CustCountry = ((RedProperty)rb.Property("CustCountry")).Value.ToString();
                    OutCutomerInfo.CustPhoneDay = ((RedProperty)rb.Property("CustPhoneDay")).Value.ToString();
                    OutCutomerInfo.CustPhoneEve = ((RedProperty)rb.Property("CustPhoneEve")).Value.ToString();
                    OutCutomerInfo.CustEmail1 = ((RedProperty)rb.Property("CustEmail1")).Value.ToString();
                    OutCutomerInfo.CustEmail2 = ((RedProperty)rb.Property("CustEmail2")).Value.ToString();
                    OutCutomerInfo.CustPin = ((RedProperty)rb.Property("CustPin")).Value.ToString();
                    OutCutomerInfo.ShipFirstName = ((RedProperty)rb.Property("ShipFirstName")).Value.ToString();
                    OutCutomerInfo.ShipLastName = ((RedProperty)rb.Property("ShipLastName")).Value.ToString();
                    OutCutomerInfo.ShipAdd1 = ((RedProperty)rb.Property("ShipAdd1")).Value.ToString();
                    OutCutomerInfo.ShipAdd2 = ((RedProperty)rb.Property("ShipAdd2")).Value.ToString();
                    OutCutomerInfo.ShipAdd3 = ((RedProperty)rb.Property("ShipAdd3")).Value.ToString();
                    OutCutomerInfo.ShipCity = ((RedProperty)rb.Property("ShipCity")).Value.ToString();
                    OutCutomerInfo.ShipState = ((RedProperty)rb.Property("ShipState")).Value.ToString();
                    OutCutomerInfo.ShipZip = ((RedProperty)rb.Property("ShipZip")).Value.ToString();


                    OutCutomerInfo.ShipCountry = ((RedProperty)rb.Property("ShipCountry")).Value.ToString();
                    OutCutomerInfo.ShipPhone = ((RedProperty)rb.Property("ShipPhone")).Value.ToString();
                    OutCutomerInfo.ShipEmail = ((RedProperty)rb.Property("ShipEmail")).Value.ToString();
                    OutCutomerInfo.ShipAttention = ((RedProperty)rb.Property("ShipAttention")).Value.ToString();
                    OutCutomerInfo.CustNumber = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    OutCutomerInfo.WebReference = ((RedProperty)rb.Property("WebReference")).Value.ToString();
                    OutCutomerInfo.PromoCode = ((RedProperty)rb.Property("PromoCode")).Value.ToString();
                    OutCutomerInfo.Title = ((RedProperty)rb.Property("Title")).Value.ToString();
                    OutCutomerInfo.ShipMethod = ((RedProperty)rb.Property("ShipMethod")).Value.ToString();
                    OutCutomerInfo.PaymentMethod = ((RedProperty)rb.Property("PaymentMethod")).Value.ToString();
                    OutCutomerInfo.CreditCardNumber = ((RedProperty)rb.Property("CreditCardNumber")).Value.ToString();
                    OutCutomerInfo.CardExpDate = ((RedProperty)rb.Property("CardExpDate")).Value.ToString();
                    OutCutomerInfo.CardCVV = ((RedProperty)rb.Property("CardCVV")).Value.ToString();
                    OutCutomerInfo.CardAddress = ((RedProperty)rb.Property("CardAddress")).Value.ToString();
                    OutCutomerInfo.CardZip = ((RedProperty)rb.Property("CardZip")).Value.ToString();
                    OutCutomerInfo.MerchAmount = ((RedProperty)rb.Property("MerchAmount")).Value.ToString();
                    OutCutomerInfo.CouponAmount = ((RedProperty)rb.Property("CouponAmount")).Value.ToString();
                    OutCutomerInfo.DiscAmount = ((RedProperty)rb.Property("DiscAmount")).Value.ToString();
                    OutCutomerInfo.ShipAmount = ((RedProperty)rb.Property("ShipAmount")).Value.ToString();
                    OutCutomerInfo.PremShipAmount = ((RedProperty)rb.Property("PremShipAmount")).Value.ToString();
                    OutCutomerInfo.OverweightAmount = ((RedProperty)rb.Property("OverweightAmount")).Value.ToString();
                    OutCutomerInfo.TaxAmount = ((RedProperty)rb.Property("TaxAmount")).Value.ToString();
                    OutCutomerInfo.TotalAmount = ((RedProperty)rb.Property("TotalAmount")).Value.ToString();
                    OutCutomerInfo.Comments = ((RedProperty)rb.Property("Comments")).Value.ToString();
                    OutCutomerInfo.Items = ((RedProperty)rb.Property("Items")).Value.ToString();
                    OutCutomerInfo.QtyOrdered = ((RedProperty)rb.Property("QtyOrdered")).Value.ToString();
                    OutCutomerInfo.UnitPrice = ((RedProperty)rb.Property("UnitPrice")).Value.ToString();
                    OutCutomerInfo.ReleaseDate = ((RedProperty)rb.Property("ReleaseDate")).Value.ToString();
                    OutCutomerInfo.FreeFlag = ((RedProperty)rb.Property("FreeFlag")).Value.ToString();
                    OutCutomerInfo.PersonalizationCode = ((RedProperty)rb.Property("PersonalizationCode")).Value.ToString();
                    OutCutomerInfo.PersonalizationDetail1 = ((RedProperty)rb.Property("PersonalizationDetail1")).Value.ToString();
                    OutCutomerInfo.PersonalizationDetail2 = ((RedProperty)rb.Property("PersonalizationDetail2")).Value.ToString();
                    OutCutomerInfo.PersonalizationDetail3 = ((RedProperty)rb.Property("PersonalizationDetail3")).Value.ToString();
                    OutCutomerInfo.PersonalizationDetail4 = ((RedProperty)rb.Property("PersonalizationDetail4")).Value.ToString();
                    OutCutomerInfo.PersonalizationDetail5 = ((RedProperty)rb.Property("PersonalizationDetail5")).Value.ToString();
                    OutCutomerInfo.PIN = ((RedProperty)rb.Property("PIN")).Value.ToString();
                    OutCutomerInfo.PINHint = ((RedProperty)rb.Property("PINHint")).Value.ToString();
                    OutCutomerInfo.OptInFlag = ((RedProperty)rb.Property("OptInFlag")).Value.ToString();
                    OutCutomerInfo.OptInDate = ((RedProperty)rb.Property("OptInDate")).Value.ToString();
                    OutCutomerInfo.OptOutDate = ((RedProperty)rb.Property("OptOutDate")).Value.ToString();
                    OutCutomerInfo.Etrack = ((RedProperty)rb.Property("Etrack")).Value.ToString();
                    OutCutomerInfo.IPSource = ((RedProperty)rb.Property("IPSource")).Value.ToString();
                    OutCutomerInfo.GVCFlag = ((RedProperty)rb.Property("GVCFlag")).Value.ToString();
                    OutCutomerInfo.GVCDate = ((RedProperty)rb.Property("GVCDate")).Value.ToString();
                    OutCutomerInfo.ValidErrCode = ((RedProperty)rb.Property("ValidErrCode")).Value.ToString();
                    OutCutomerInfo.ValidErrMsg = ((RedProperty)rb.Property("ValidErrMsg")).Value.ToString();
                    OutCutomerInfo.OrderErrCode = ((RedProperty)rb.Property("OrderErrCode")).Value.ToString();
                    OutCutomerInfo.OrderErrMsg = ((RedProperty)rb.Property("OrderErrMsg")).Value.ToString();
                    OutCutomerInfo.Cert1_Number = ((RedProperty)rb.Property("Cert1_Number")).Value.ToString();
                    OutCutomerInfo.Cert1_Amount = ((RedProperty)rb.Property("Cert1_Amount")).Value.ToString();
                    OutCutomerInfo.Cert2_Number = ((RedProperty)rb.Property("Cert2_Number")).Value.ToString();
                    OutCutomerInfo.Cert2_Amount = ((RedProperty)rb.Property("Cert2_Amount")).Value.ToString();
                    OutCutomerInfo.Cert3_Number = ((RedProperty)rb.Property("Cert3_Number")).Value.ToString();
                    OutCutomerInfo.Cert3_Amount = ((RedProperty)rb.Property("Cert3_Amount")).Value.ToString();
                    OutCutomerInfo.Repc1_Number = ((RedProperty)rb.Property("Repc1_Number")).Value.ToString();
                    OutCutomerInfo.Repc1_Amount = ((RedProperty)rb.Property("Repc1_Amount")).Value.ToString();
                    OutCutomerInfo.Repc2_Number = ((RedProperty)rb.Property("Repc2_Number")).Value.ToString();
                    OutCutomerInfo.Repc2_Amount = ((RedProperty)rb.Property("Repc2_Amount")).Value.ToString();
                    OutCutomerInfo.Repc3_Number = ((RedProperty)rb.Property("Repc3_Number")).Value.ToString();
                    OutCutomerInfo.Repc3_Amount = ((RedProperty)rb.Property("Repc3_Amount")).Value.ToString();
                    OutCutomerInfo.Web_Date = ((RedProperty)rb.Property("Web_Date")).Value.ToString();
                    OutCutomerInfo.Web_Time = ((RedProperty)rb.Property("Web_Time")).Value.ToString();
                    OutCutomerInfo.MobilePhone = ((RedProperty)rb.Property("MobilePhone")).Value.ToString();  //added by aditya 15-nov-2012
                    OutCutomerInfo.MobileOptin = ((RedProperty)rb.Property("MobileOptin")).Value.ToString();  //end by aditya 15-nov-2012

                    ArryCustInfo[1] = ((RedProperty)rb.Property("OrderNumber")).Value.ToString();
                    ArryCustInfo[2] = ((RedProperty)rb.Property("CustFirstName")).Value;
                    ArryCustInfo[3] = ((RedProperty)rb.Property("CustLastName")).Value.ToString();
                    ArryCustInfo[4] = ((RedProperty)rb.Property("CustAdd1")).Value.ToString();
                    ArryCustInfo[5] = ((RedProperty)rb.Property("CustAdd2")).Value.ToString();
                    ArryCustInfo[6] = ((RedProperty)rb.Property("CustAdd3")).Value.ToString();
                    ArryCustInfo[7] = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    ArryCustInfo[8] = ((RedProperty)rb.Property("CustState")).Value.ToString();
                    ArryCustInfo[9] = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    ArryCustInfo[10] = ((RedProperty)rb.Property("CustCountry")).Value.ToString();
                    ArryCustInfo[11] = ((RedProperty)rb.Property("CustPhoneDay")).Value.ToString();
                    ArryCustInfo[12] = ((RedProperty)rb.Property("CustPhoneEve")).Value.ToString();
                    ArryCustInfo[13] = ((RedProperty)rb.Property("CustEmail1")).Value.ToString();
                    ArryCustInfo[14] = ((RedProperty)rb.Property("CustEmail2")).Value.ToString();
                    ArryCustInfo[15] = ((RedProperty)rb.Property("CustPin")).Value.ToString();
                    ArryCustInfo[16] = ((RedProperty)rb.Property("ShipFirstName")).Value.ToString();
                    ArryCustInfo[17] = ((RedProperty)rb.Property("ShipLastName")).Value.ToString();
                    ArryCustInfo[18] = ((RedProperty)rb.Property("ShipAdd1")).Value.ToString();
                    ArryCustInfo[19] = ((RedProperty)rb.Property("ShipAdd2")).Value.ToString();
                    ArryCustInfo[20] = ((RedProperty)rb.Property("ShipAdd3")).Value.ToString();
                    ArryCustInfo[21] = ((RedProperty)rb.Property("ShipCity")).Value.ToString();
                    ArryCustInfo[22] = ((RedProperty)rb.Property("ShipState")).Value.ToString();
                    ArryCustInfo[23] = ((RedProperty)rb.Property("ShipZip")).Value.ToString();

                    ArryCustInfo[24] = ((RedProperty)rb.Property("ShipCountry")).Value.ToString();
                    ArryCustInfo[25] = ((RedProperty)rb.Property("ShipPhone")).Value.ToString();
                    ArryCustInfo[26] = ((RedProperty)rb.Property("ShipEmail")).Value.ToString();
                    ArryCustInfo[27] = ((RedProperty)rb.Property("ShipAttention")).Value.ToString();
                    ArryCustInfo[28] = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    ArryCustInfo[29] = ((RedProperty)rb.Property("WebReference")).Value.ToString();
                    ArryCustInfo[30] = ((RedProperty)rb.Property("PromoCode")).Value.ToString();
                    ArryCustInfo[31] = ((RedProperty)rb.Property("Title")).Value.ToString();
                    ArryCustInfo[32] = ((RedProperty)rb.Property("ShipMethod")).Value.ToString();
                    ArryCustInfo[33] = ((RedProperty)rb.Property("PaymentMethod")).Value.ToString();
                    ArryCustInfo[34] = ((RedProperty)rb.Property("CreditCardNumber")).Value.ToString();
                    ArryCustInfo[35] = ((RedProperty)rb.Property("CardExpDate")).Value.ToString();
                    ArryCustInfo[36] = ((RedProperty)rb.Property("CardCVV")).Value.ToString();
                    ArryCustInfo[37] = ((RedProperty)rb.Property("CardAddress")).Value.ToString();
                    ArryCustInfo[38] = ((RedProperty)rb.Property("CardZip")).Value.ToString();
                    ArryCustInfo[39] = ((RedProperty)rb.Property("MerchAmount")).Value.ToString();
                    ArryCustInfo[40] = ((RedProperty)rb.Property("CouponAmount")).Value.ToString();
                    ArryCustInfo[41] = ((RedProperty)rb.Property("DiscAmount")).Value.ToString();
                    ArryCustInfo[42] = ((RedProperty)rb.Property("ShipAmount")).Value.ToString();
                    ArryCustInfo[43] = ((RedProperty)rb.Property("PremShipAmount")).Value.ToString();
                    ArryCustInfo[44] = ((RedProperty)rb.Property("OverweightAmount")).Value.ToString();
                    ArryCustInfo[45] = ((RedProperty)rb.Property("TaxAmount")).Value.ToString();
                    ArryCustInfo[46] = ((RedProperty)rb.Property("TotalAmount")).Value.ToString();
                    ArryCustInfo[47] = ((RedProperty)rb.Property("Comments")).Value.ToString();
                    ArryCustInfo[48] = ((RedProperty)rb.Property("Items")).Value.ToString();
                    ArryCustInfo[49] = ((RedProperty)rb.Property("QtyOrdered")).Value.ToString();
                    ArryCustInfo[50] = ((RedProperty)rb.Property("UnitPrice")).Value.ToString();
                    ArryCustInfo[51] = ((RedProperty)rb.Property("ReleaseDate")).Value.ToString();
                    ArryCustInfo[52] = ((RedProperty)rb.Property("FreeFlag")).Value.ToString();
                    ArryCustInfo[53] = ((RedProperty)rb.Property("PersonalizationCode")).Value.ToString();
                    ArryCustInfo[54] = ((RedProperty)rb.Property("PersonalizationDetail1")).Value.ToString();
                    ArryCustInfo[55] = ((RedProperty)rb.Property("PersonalizationDetail2")).Value.ToString();
                    ArryCustInfo[56] = ((RedProperty)rb.Property("PersonalizationDetail3")).Value.ToString();
                    ArryCustInfo[57] = ((RedProperty)rb.Property("PersonalizationDetail4")).Value.ToString();
                    ArryCustInfo[58] = ((RedProperty)rb.Property("PersonalizationDetail5")).Value.ToString();
                    ArryCustInfo[59] = ((RedProperty)rb.Property("PIN")).Value.ToString();
                    ArryCustInfo[60] = ((RedProperty)rb.Property("PINHint")).Value.ToString();
                    ArryCustInfo[61] = ((RedProperty)rb.Property("OptInFlag")).Value.ToString();
                    ArryCustInfo[62] = ((RedProperty)rb.Property("OptInDate")).Value.ToString();
                    ArryCustInfo[63] = ((RedProperty)rb.Property("OptOutDate")).Value.ToString();
                    ArryCustInfo[64] = ((RedProperty)rb.Property("Etrack")).Value.ToString();
                    ArryCustInfo[65] = ((RedProperty)rb.Property("IPSource")).Value.ToString();
                    ArryCustInfo[66] = ((RedProperty)rb.Property("GVCFlag")).Value.ToString();
                    ArryCustInfo[67] = ((RedProperty)rb.Property("GVCDate")).Value.ToString();
                    ArryCustInfo[68] = ((RedProperty)rb.Property("ValidErrCode")).Value.ToString();
                    ArryCustInfo[69] = ((RedProperty)rb.Property("ValidErrMsg")).Value.ToString();
                    ArryCustInfo[70] = ((RedProperty)rb.Property("OrderErrCode")).Value.ToString();
                    ArryCustInfo[71] = ((RedProperty)rb.Property("OrderErrMsg")).Value.ToString();
                    ArryCustInfo[72] = ((RedProperty)rb.Property("Cert1_Number")).Value.ToString();
                    ArryCustInfo[73] = ((RedProperty)rb.Property("Cert1_Amount")).Value.ToString();
                    ArryCustInfo[74] = ((RedProperty)rb.Property("Cert2_Number")).Value.ToString();
                    ArryCustInfo[75] = ((RedProperty)rb.Property("Cert2_Amount")).Value.ToString();
                    ArryCustInfo[76] = ((RedProperty)rb.Property("Cert3_Number")).Value.ToString();
                    ArryCustInfo[77] = ((RedProperty)rb.Property("Cert3_Amount")).Value.ToString();
                    ArryCustInfo[78] = ((RedProperty)rb.Property("Repc1_Number")).Value.ToString();
                    ArryCustInfo[79] = ((RedProperty)rb.Property("Repc1_Amount")).Value.ToString();
                    ArryCustInfo[80] = ((RedProperty)rb.Property("Repc2_Number")).Value.ToString();
                    ArryCustInfo[81] = ((RedProperty)rb.Property("Repc2_Amount")).Value.ToString();
                    ArryCustInfo[82] = ((RedProperty)rb.Property("Repc3_Number")).Value.ToString();
                    ArryCustInfo[83] = ((RedProperty)rb.Property("Repc3_Amount")).Value.ToString();
                    ArryCustInfo[84] = ((RedProperty)rb.Property("Web_Date")).Value.ToString();
                    ArryCustInfo[85] = ((RedProperty)rb.Property("Web_Time")).Value.ToString();
                    ArryCustInfo[86] = ((RedProperty)rb.Property("MobilePhone")).Value.ToString();  //added by aditya 15-nov-2012
                    ArryCustInfo[87] = ((RedProperty)rb.Property("MobileOptin")).Value.ToString();  //end by aditya 15-nov-2012

                //}
                CustInfo.Rows.Add(ArryCustInfo);
                OutPutParam = ParamMasterGetData(ArryCustInfo);
                string LogOn = RedBackServiceGetLogOnDetails(Path);
                StringBuilder MethodName = new StringBuilder();
                if (LogOn == "1")
                {
                    MethodName.AppendLine(" ");
                    MethodName.AppendLine("Method Name : AddOrder");
                    MethodName.AppendLine(" ");
                    InputParam.AppendLine(" ");
                    InputParam.AppendLine("OutPut Details");
                    InputParam.AppendLine(" ");
                    string Log = MethodName.ToString() + InputParam.ToString();
                    Log = Log + OutPutParam.ToString();
                    WriteServiceLog(Log, Path);
                    //WriteServiceLog("Method GetPost End" + DateTime.Now, Path);
                    WriteServiceLog("Method GetPost End", Path);
                }
                return obj.Serialize(OutCutomerInfo);

            }

            public string Order_Status_SummaryOfOrders1(string CustNumber, string CustZip, string Title)
            {
                StringBuilder rsp = new StringBuilder();
                if ((CustNumber != ""))// && (custZip != ""))
                {
                    RedObject rb = new RedObject();
                    rb.Open3(RedBackAccount, "OPM:Order_Status");
                    ((RedProperty)rb.Property("CustNumber")).Value = CustNumber;
                    ((RedProperty)rb.Property("CustZip")).Value = CustZip;
                    ((RedProperty)rb.Property("Title")).Value = Title;

                    rb.CallMethod("OrderStatus");
                    string custMsg = "Success";
                    string retu = ((RedProperty)rb.Property("StatusError")).Value;
                    if (((RedProperty)rb.Property("StatusError")).Value != "")
                        custMsg = ((RedProperty)rb.Property("StatusMessage")).Value;
                    rsp.Append("<OrderStatus-SummaryOfOrders>\n");
                    rsp.Append("\t<Message>" + custMsg + "</Message>\n");
                    rsp.Append("\t\t<CustomerNumber>" + ((RedProperty)rb.Property("CustNumber")).Value + "</CustomerNumber>\n");
                    rsp.Append("\t\t<CustomerName>" + ((RedProperty)rb.Property("CustName")).Value + "</CustomerName>\n");
                    rsp.Append("\t\t<Address1>" + ((RedProperty)rb.Property("CustAdd1")).Value + "</Address1>\n");
                    rsp.Append("\t\t<Address2>" + ((RedProperty)rb.Property("CustAdd2")).Value + "</Address2>\n");
                    rsp.Append("\t\t<State>" + ((RedProperty)rb.Property("CustState")).Value + "</State>\n");
                    rsp.Append("\t\t<City>" + ((RedProperty)rb.Property("CustCity")).Value + "</City>\n");
                    rsp.Append("\t\t<Zip>" + ((RedProperty)rb.Property("CustZip")).Value + "</Zip>\n");
                    rsp.Append("\t\t<Country>" + ((RedProperty)rb.Property("CustCountry")).Value + "</Country>\n");
                    rsp.Append("\t\t<Phone>" + ((RedProperty)rb.Property("CustPhone")).Value + "</Phone>\n");
                    rsp.Append("\t\t<Fax>" + ((RedProperty)rb.Property("CustFax")).Value + "</Fax>\n");
                    rsp.Append("\t\t<Email>" + ((RedProperty)rb.Property("CustEmail")).Value + "</Email>\n");
                    rsp.Append("\t\t<CustPin>" + ((RedProperty)rb.Property("CustPin")).Value + "</CustPin>\n");
                    rsp.Append("\t\t<Title>" + ((RedProperty)rb.Property("Title")).Value + "</Title>\n");
                    rsp.Append("\t\t<SumOrderNumbers>" + ((RedProperty)rb.Property("SumOrderNumbers")).Value + "</SumOrderNumbers>\n");
                    rsp.Append("\t\t<SumOrderDates>" + ((RedProperty)rb.Property("SumOrderDates")).Value + "</SumOrderDates>\n");
                    rsp.Append("\t\t<SumShipNames>" + ((RedProperty)rb.Property("SumShipNames")).Value + "</SumShipNames>\n");
                    rsp.Append("\t\t<SumShipStatus>" + ((RedProperty)rb.Property("SumShipStatus")).Value + "</SumShipStatus>\n");
                    rsp.Append("\t\t<SumOrderTotal>" + ((RedProperty)rb.Property("SumOrderTotal")).Value + "</SumOrderTotal>\n");
                    rsp.Append("\t\t<DetOrderNumber>" + ((RedProperty)rb.Property("DetOrderNumber")).Value + "</DetOrderNumber>\n");
                    rsp.Append("\t\t<DetOrderDate>" + ((RedProperty)rb.Property("DetOrderDate")).Value + "</DetOrderDate>\n");
                    rsp.Append("\t\t<DetOrderAmount>" + ((RedProperty)rb.Property("DetOrderAmount")).Value + "</DetOrderAmount>\n");
                    rsp.Append("\t\t<DetShipName>" + ((RedProperty)rb.Property("DetShipName")).Value + "</DetShipName>\n");
                    rsp.Append("\t\t<DetShipAdd1>" + ((RedProperty)rb.Property("DetShipAdd1")).Value + "</DetShipAdd1>\n");
                    rsp.Append("\t\t<DetShipAdd2>" + ((RedProperty)rb.Property("DetShipAdd2")).Value + "</DetShipAdd2>\n");
                    rsp.Append("\t\t<DetShipCity>" + ((RedProperty)rb.Property("DetShipCity")).Value + "</DetShipCity>\n");
                    rsp.Append("\t\t<DetShipState>" + ((RedProperty)rb.Property("DetShipState")).Value + "</DetShipState>\n");
                    rsp.Append("\t\t<DetShipZip>" + ((RedProperty)rb.Property("DetShipZip")).Value + "</DetShipZip>\n");
                    rsp.Append("\t\t<DetShipNumber>" + ((RedProperty)rb.Property("DetShipNumber")).Value + "</DetShipNumber>\n");
                    rsp.Append("\t\t<DetShipItems>" + ((RedProperty)rb.Property("DetShipItems")).Value + "</DetShipItems>\n");
                    rsp.Append("\t\t<DetShipQty>" + ((RedProperty)rb.Property("DetShipQty")).Value + "</DetShipQty>\n");
                    rsp.Append("\t\t<DetShipDesc>" + ((RedProperty)rb.Property("DetShipDesc")).Value + "</DetShipDesc>\n");
                    rsp.Append("\t\t<DetShipVia>" + ((RedProperty)rb.Property("DetShipVia")).Value + "</DetShipVia>\n");
                    rsp.Append("\t\t<DetShipDate>" + ((RedProperty)rb.Property("DetShipDate")).Value + "</DetShipDate>\n");
                    rsp.Append("\t\t<DetShipTrackNums>" + ((RedProperty)rb.Property("DetShipTrackNums")).Value + "</DetShipTrackNums>\n");
                    rsp.Append("\t\t<DetShipTrackLink>" + ((RedProperty)rb.Property("DetShipTrackLink")).Value + "</DetShipTrackLink>\n");
                    rsp.Append("\t\t<DetShipEstDelBegin>" + ((RedProperty)rb.Property("DetShipEstDelBegin")).Value + "</DetShipEstDelBegin>\n");
                    rsp.Append("\t\t<DetShipEstDelEnd>" + ((RedProperty)rb.Property("DetShipEstDelEnd")).Value + "</DetShipEstDelEnd>\n");
                    rsp.Append("\t\t<DetOpenLocs>" + ((RedProperty)rb.Property("DetOpenLocs")).Value + "</DetOpenLocs>\n");
                    rsp.Append("\t\t<DetOpenItems>" + ((RedProperty)rb.Property("DetOpenItems")).Value + "</DetOpenItems>\n");
                    rsp.Append("\t\t<DetOpenQtys>" + ((RedProperty)rb.Property("DetOpenQtys")).Value + "</DetOpenQtys>\n");
                    rsp.Append("\t\t<DetOpenDesc>" + ((RedProperty)rb.Property("DetOpenDesc")).Value + "</DetOpenDesc>\n");
                    rsp.Append("\t\t<DetOpenEstDelBegin>" + ((RedProperty)rb.Property("DetOpenEstDelBegin")).Value + "</DetOpenEstDelBegin>\n");
                    rsp.Append("\t\t<DetOpenEstDelEnd>" + ((RedProperty)rb.Property("DetOpenEstDelEnd")).Value + "</DetOpenEstDelEnd>\n");
                    rsp.Append("\t\t<StatusErr>" + ((RedProperty)rb.Property("StatusErr")).Value + "</StatusErr>\n");
                    rsp.Append("\t\t<StatusMsg>" + ((RedProperty)rb.Property("StatusMsg")).Value + "</StatusMsg>\n");

                    rsp.Append("</OrderStatus-SummaryOfOrders>\n");
                }
                else
                {
                    rsp.Append("<OrderStatus-SummaryOfOrders>\n");
                    rsp.Append("\t<Message>No data</Message>\n");
                    rsp.Append("\t\t<FirstName></FirstName>\n");
                    rsp.Append("\t\t<LastName></LastName>\n");
                    rsp.Append("\t\t<Address1></Address1>\n");
                    rsp.Append("\t\t<Address2></Address2>\n");
                    rsp.Append("\t\t<City></City>\n");
                    rsp.Append("\t\t<State></State>\n");
                    rsp.Append("\t\t<Zip></Zip>\n");
                    rsp.Append("\t\t<Country></Country>\n");
                    rsp.Append("\t\t<Email></Email>\n");
                    rsp.Append("\t\t<Phone></Phone>\n");
                    rsp.Append("\t<KeycodeValid></KeycodeValid>\n");
                    rsp.Append("\t<RevCreditPlan></RevCreditPlan>\n");
                    rsp.Append("\t<InterestRate></InterestRate>\n");
                    rsp.Append("</OrderStatus-SummaryOfOrders>\n");
                }
                return rsp.ToString();
            }

            public string Order_Status_OrderDetail1(string CustNumber, string CustZip, string Title, string DetOrderNumber)
            {
                StringBuilder rsp = new StringBuilder();
                if ((CustNumber != "") && (DetOrderNumber != ""))
                {
                    RedObject rb = new RedObject();
                    rb.Open3(RedBackAccount, "OPM:Order_Status");
                    ((RedProperty)rb.Property("CustNumber")).Value = CustNumber;
                    ((RedProperty)rb.Property("CustZip")).Value = CustZip;
                    ((RedProperty)rb.Property("Title")).Value = Title;
                    ((RedProperty)rb.Property("DetOrderNumber")).Value = DetOrderNumber;

                    rb.CallMethod("OrderStatus");
                    string custMsg = "Success";
                    string retu = ((RedProperty)rb.Property("StatusError")).Value;
                    if (((RedProperty)rb.Property("StatusError")).Value != "")
                        custMsg = ((RedProperty)rb.Property("StatusMessage")).Value;
                    rsp.Append("<OrderStatus-OrderDetails>\n");
                    rsp.Append("\t<Message>" + custMsg + "</Message>\n");
                    rsp.Append("\t\t<CustomerNumber>" + ((RedProperty)rb.Property("CustNumber")).Value + "</CustomerNumber>\n");
                    rsp.Append("\t\t<CustomerName>" + ((RedProperty)rb.Property("CustName")).Value + "</CustomerName>\n");
                    rsp.Append("\t\t<Address1>" + ((RedProperty)rb.Property("CustAdd1")).Value + "</Address1>\n");
                    rsp.Append("\t\t<Address2>" + ((RedProperty)rb.Property("CustAdd2")).Value + "</Address2>\n");
                    rsp.Append("\t\t<State>" + ((RedProperty)rb.Property("CustState")).Value + "</State>\n");
                    rsp.Append("\t\t<City>" + ((RedProperty)rb.Property("CustCity")).Value + "</City>\n");
                    rsp.Append("\t\t<Zip>" + ((RedProperty)rb.Property("CustZip")).Value + "</Zip>\n");
                    rsp.Append("\t\t<Country>" + ((RedProperty)rb.Property("CustCountry")).Value + "</Country>\n");
                    rsp.Append("\t\t<Phone>" + ((RedProperty)rb.Property("CustPhone")).Value + "</Phone>\n");
                    rsp.Append("\t\t<Fax>" + ((RedProperty)rb.Property("CustFax")).Value + "</Fax>\n");
                    rsp.Append("\t\t<Email>" + ((RedProperty)rb.Property("CustEmail")).Value + "</Email>\n");
                    rsp.Append("\t\t<CustPin>" + ((RedProperty)rb.Property("CustPin")).Value + "</CustPin>\n");
                    rsp.Append("\t\t<Title>" + ((RedProperty)rb.Property("Title")).Value + "</Title>\n");
                    rsp.Append("\t\t<SumOrderNumbers>" + ((RedProperty)rb.Property("SumOrderNumbers")).Value + "</SumOrderNumbers>\n");
                    rsp.Append("\t\t<SumOrderDates>" + ((RedProperty)rb.Property("SumOrderDates")).Value + "</SumOrderDates>\n");
                    rsp.Append("\t\t<SumShipNames>" + ((RedProperty)rb.Property("SumShipNames")).Value + "</SumShipNames>\n");
                    rsp.Append("\t\t<SumShipStatus>" + ((RedProperty)rb.Property("SumShipStatus")).Value + "</SumShipStatus>\n");
                    rsp.Append("\t\t<SumOrderTotal>" + ((RedProperty)rb.Property("SumOrderTotal")).Value + "</SumOrderTotal>\n");
                    rsp.Append("\t\t<DetOrderNumber>" + ((RedProperty)rb.Property("DetOrderNumber")).Value + "</DetOrderNumber>\n");
                    rsp.Append("\t\t<DetOrderDate>" + ((RedProperty)rb.Property("DetOrderDate")).Value + "</DetOrderDate>\n");
                    rsp.Append("\t\t<DetOrderAmount>" + ((RedProperty)rb.Property("DetOrderAmount")).Value + "</DetOrderAmount>\n");
                    rsp.Append("\t\t<DetShipName>" + ((RedProperty)rb.Property("DetShipName")).Value + "</DetShipName>\n");
                    rsp.Append("\t\t<DetShipAdd1>" + ((RedProperty)rb.Property("DetShipAdd1")).Value + "</DetShipAdd1>\n");
                    rsp.Append("\t\t<DetShipAdd2>" + ((RedProperty)rb.Property("DetShipAdd2")).Value + "</DetShipAdd2>\n");
                    rsp.Append("\t\t<DetShipCity>" + ((RedProperty)rb.Property("DetShipCity")).Value + "</DetShipCity>\n");
                    rsp.Append("\t\t<DetShipState>" + ((RedProperty)rb.Property("DetShipState")).Value + "</DetShipState>\n");
                    rsp.Append("\t\t<DetShipZip>" + ((RedProperty)rb.Property("DetShipZip")).Value + "</DetShipZip>\n");
                    rsp.Append("\t\t<DetShipNumber>" + ((RedProperty)rb.Property("DetShipNumber")).Value + "</DetShipNumber>\n");
                    rsp.Append("\t\t<DetShipItems>" + ((RedProperty)rb.Property("DetShipItems")).Value + "</DetShipItems>\n");
                    rsp.Append("\t\t<DetShipQty>" + ((RedProperty)rb.Property("DetShipQty")).Value + "</DetShipQty>\n");
                    rsp.Append("\t\t<DetShipDesc>" + ((RedProperty)rb.Property("DetShipDesc")).Value + "</DetShipDesc>\n");
                    rsp.Append("\t\t<DetShipVia>" + ((RedProperty)rb.Property("DetShipVia")).Value + "</DetShipVia>\n");
                    rsp.Append("\t\t<DetShipDate>" + ((RedProperty)rb.Property("DetShipDate")).Value + "</DetShipDate>\n");
                    rsp.Append("\t\t<DetShipTrackNums>" + ((RedProperty)rb.Property("DetShipTrackNums")).Value + "</DetShipTrackNums>\n");
                    rsp.Append("\t\t<DetShipTrackLink>" + ((RedProperty)rb.Property("DetShipTrackLink")).Value + "</DetShipTrackLink>\n");
                    rsp.Append("\t\t<DetShipEstDelBegin>" + ((RedProperty)rb.Property("DetShipEstDelBegin")).Value + "</DetShipEstDelBegin>\n");
                    rsp.Append("\t\t<DetShipEstDelEnd>" + ((RedProperty)rb.Property("DetShipEstDelEnd")).Value + "</DetShipEstDelEnd>\n");
                    rsp.Append("\t\t<DetOpenLocs>" + ((RedProperty)rb.Property("DetOpenLocs")).Value + "</DetOpenLocs>\n");
                    rsp.Append("\t\t<DetOpenItems>" + ((RedProperty)rb.Property("DetOpenItems")).Value + "</DetOpenItems>\n");
                    rsp.Append("\t\t<DetOpenQtys>" + ((RedProperty)rb.Property("DetOpenQtys")).Value + "</DetOpenQtys>\n");
                    rsp.Append("\t\t<DetOpenDesc>" + ((RedProperty)rb.Property("DetOpenDesc")).Value + "</DetOpenDesc>\n");
                    rsp.Append("\t\t<DetOpenEstDelBegin>" + ((RedProperty)rb.Property("DetOpenEstDelBegin")).Value + "</DetOpenEstDelBegin>\n");
                    rsp.Append("\t\t<DetOpenEstDelEnd>" + ((RedProperty)rb.Property("DetOpenEstDelEnd")).Value + "</DetOpenEstDelEnd>\n");
                    rsp.Append("\t\t<StatusErr>" + ((RedProperty)rb.Property("StatusErr")).Value + "</StatusErr>\n");
                    rsp.Append("\t\t<StatusMsg>" + ((RedProperty)rb.Property("StatusMsg")).Value + "</StatusMsg>\n");

                    rsp.Append("</OrderStatus-OrderDetails>\n");
                }
                else
                {
                    rsp.Append("<OrderStatus-OrderDetails>\n");
                    rsp.Append("\t<Message>No data</Message>\n");
                    rsp.Append("\t\t<FirstName></FirstName>\n");
                    rsp.Append("\t\t<LastName></LastName>\n");
                    rsp.Append("\t\t<Address1></Address1>\n");
                    rsp.Append("\t\t<Address2></Address2>\n");
                    rsp.Append("\t\t<City></City>\n");
                    rsp.Append("\t\t<State></State>\n");
                    rsp.Append("\t\t<Zip></Zip>\n");
                    rsp.Append("\t\t<Country></Country>\n");
                    rsp.Append("\t\t<Email></Email>\n");
                    rsp.Append("\t\t<Phone></Phone>\n");
                    rsp.Append("\t<KeycodeValid></KeycodeValid>\n");
                    rsp.Append("\t<RevCreditPlan></RevCreditPlan>\n");
                    rsp.Append("\t<InterestRate></InterestRate>\n");
                    rsp.Append("</OrderStatus-OrderDetails>\n");
                }
                return rsp.ToString();
            }

            public string Order_Status(string CustNumber, string CustName, string CustAdd1, string CustAdd2, string CustState, string CustCity, string CustZip, string CustCountry,
                                   string CustPhone, string CustFax, string CustEmail, string CustPin, string Title, string SumOrderNumbers, string SumOrderDates, string SumShipNames,
                                   string SumShipStatus, string SumOrderTotal, string DetOrderNumber, string DetOrderDate, string DetOrderAmount, string DetShipName, string DetShipAdd1,
                                   string DetShipAdd2, string DetShipCity, string DetShipState, string DetShipZip, string DetShipNumber, string DetShipItems, string DetShipQty,
                                   string DetShipDesc, string DetShipVia, string DetShipDate, string DetShipTrackNums, string DetShipTrackLink,
                                   string DetShipEstDelBegin, string DetShipEstDelEnd, string DetOpenLocs, string DetOpenItems, string DetOpenQtys,
                                   string DetOpenDesc, string DetOpenEstDelBegin, string DetOpenEstDelEnd, string StatusErr, string StatusMsg, string Path)
            {
                StringBuilder rsp = new StringBuilder();
                StringBuilder InputParam = new StringBuilder();
                StringBuilder OutPutParam = new StringBuilder();
                InputParam = OrderStatusGetData(CustNumber, CustName, CustAdd1, CustAdd2, CustState, CustCity, CustZip, CustCountry,
                                    CustPhone, CustFax, CustEmail, CustPin, Title, SumOrderNumbers, SumOrderDates, SumShipNames,
                                    SumShipStatus, SumOrderTotal, DetOrderNumber, DetOrderDate, DetOrderAmount, DetShipName, DetShipAdd1,
                                    DetShipAdd2, DetShipCity, DetShipState, DetShipZip, DetShipNumber, DetShipItems, DetShipQty,
                                    DetShipDesc, DetShipVia, DetShipDate, DetShipTrackNums, DetShipTrackLink,
                                    DetShipEstDelBegin, DetShipEstDelEnd, DetOpenLocs, DetOpenItems, DetOpenQtys,
                                    DetOpenDesc, DetOpenEstDelBegin, DetOpenEstDelEnd, StatusErr, StatusMsg, Path);
                WriteServiceLog("Method GetPost Start" + DateTime.Now, Path);
                JavaScriptSerializer obj = new JavaScriptSerializer();
                DataTable CustInfo = CreateOrderStatusTable();
                string[] ArryCustInfo = new string[CustInfo.Columns.Count];
                for (int dataIndex = 0; dataIndex < CustInfo.Columns.Count; dataIndex++)
                {
                    ArryCustInfo[dataIndex] = "";
                }
                RedBackCallsService.Service1.OrderStatus OutCutomerInfo = new RedBackCallsService.Service1.OrderStatus();
                //if ((CustNumber != "") && (DetOrderNumber != ""))
                //{
                    RedObject rb = new RedObject();
                    rb.Open3(RedBackAccount, "OPM:Order_Status");
                    ((RedProperty)rb.Property("CustNumber")).Value = CustNumber;
                    ((RedProperty)rb.Property("CustName")).Value = CustName;
                    ((RedProperty)rb.Property("CustAdd1")).Value = CustAdd1;
                    ((RedProperty)rb.Property("CustAdd2")).Value = CustAdd2;
                    ((RedProperty)rb.Property("CustState")).Value = CustState;
                    ((RedProperty)rb.Property("CustCity")).Value = CustCity;
                    ((RedProperty)rb.Property("CustZip")).Value = CustZip;
                    ((RedProperty)rb.Property("CustCountry")).Value = CustCountry;
                    ((RedProperty)rb.Property("CustPhone")).Value = CustPhone;
                    ((RedProperty)rb.Property("CustFax")).Value = CustFax;
                    ((RedProperty)rb.Property("CustEmail")).Value = CustEmail;
                    ((RedProperty)rb.Property("CustPin")).Value = CustPin;
                    ((RedProperty)rb.Property("Title")).Value = Title;
                    ((RedProperty)rb.Property("SumOrderNumbers")).Value = SumOrderNumbers;
                    ((RedProperty)rb.Property("SumOrderDates")).Value = SumOrderDates;
                    ((RedProperty)rb.Property("SumShipNames")).Value = SumShipNames;
                    ((RedProperty)rb.Property("SumShipStatus")).Value = SumShipStatus;
                    ((RedProperty)rb.Property("SumOrderTotal")).Value = SumOrderTotal;
                    ((RedProperty)rb.Property("DetOrderNumber")).Value = DetOrderNumber;
                    ((RedProperty)rb.Property("DetOrderDate")).Value = DetOrderDate;
                    ((RedProperty)rb.Property("DetOrderAmount")).Value = DetOrderAmount;
                    ((RedProperty)rb.Property("DetShipName")).Value = DetShipName;
                    ((RedProperty)rb.Property("DetShipAdd1")).Value = DetShipAdd1;
                    ((RedProperty)rb.Property("DetShipAdd2")).Value = DetShipAdd2;

                    ((RedProperty)rb.Property("DetShipCity")).Value = DetShipCity;
                    ((RedProperty)rb.Property("DetShipState")).Value = DetShipState;
                    ((RedProperty)rb.Property("DetShipZip")).Value = DetShipZip;
                    ((RedProperty)rb.Property("DetShipNumber")).Value = DetShipNumber;
                    ((RedProperty)rb.Property("DetShipItems")).Value = DetShipItems;
                    ((RedProperty)rb.Property("DetShipQty")).Value = DetShipQty;
                    ((RedProperty)rb.Property("DetShipDesc")).Value = DetShipDesc;
                    ((RedProperty)rb.Property("DetShipVia")).Value = DetShipVia;
                    ((RedProperty)rb.Property("DetShipDate")).Value = DetShipDate;
                    ((RedProperty)rb.Property("DetShipTrackNums")).Value = DetShipTrackNums;
                    ((RedProperty)rb.Property("DetShipTrackLink")).Value = DetShipTrackLink;
                    ((RedProperty)rb.Property("DetShipEstDelBegin")).Value = DetShipEstDelBegin;
                    ((RedProperty)rb.Property("DetShipEstDelEnd")).Value = DetShipEstDelEnd;

                    ((RedProperty)rb.Property("DetOpenLocs")).Value = DetOpenLocs;
                    ((RedProperty)rb.Property("DetOpenItems")).Value = DetOpenItems;
                    ((RedProperty)rb.Property("DetOpenQtys")).Value = DetOpenQtys;
                    ((RedProperty)rb.Property("DetOpenDesc")).Value = DetOpenDesc;
                    ((RedProperty)rb.Property("DetOpenEstDelBegin")).Value = DetOpenEstDelBegin;
                    ((RedProperty)rb.Property("DetOpenEstDelEnd")).Value = DetOpenEstDelEnd;
                    ((RedProperty)rb.Property("StatusErr")).Value = StatusErr;
                    ((RedProperty)rb.Property("StatusMsg")).Value = StatusMsg;


                    rb.CallMethod("OrderStatus");
                    string custMsg = "Success";
                    string retu = ((RedProperty)rb.Property("CustError")).Value;
                    if (((RedProperty)rb.Property("CustError")).Value != "")
                        custMsg = ((RedProperty)rb.Property("CustMessage")).Value;

                    OutCutomerInfo.Message = custMsg;
                    OutCutomerInfo.CustNumber = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    OutCutomerInfo.CustName = ((RedProperty)rb.Property("CustName")).Value.ToString();
                    OutCutomerInfo.CustAdd1 = ((RedProperty)rb.Property("CustAdd1")).Value.ToString();
                    OutCutomerInfo.CustAdd2 = ((RedProperty)rb.Property("CustAdd2")).Value.ToString();
                    OutCutomerInfo.CustState = ((RedProperty)rb.Property("CustState")).Value.ToString();
                    OutCutomerInfo.CustCity = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    OutCutomerInfo.CustZip = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    OutCutomerInfo.CustCountry = ((RedProperty)rb.Property("CustCountry")).Value.ToString();
                    OutCutomerInfo.CustPhone = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    OutCutomerInfo.CustFax = ((RedProperty)rb.Property("CustFax")).Value.ToString();
                    OutCutomerInfo.CustEmail = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    OutCutomerInfo.CustPin = ((RedProperty)rb.Property("CustPin")).Value.ToString();
                    OutCutomerInfo.Title = ((RedProperty)rb.Property("Title")).Value.ToString();
                    OutCutomerInfo.SumOrderNumbers = ((RedProperty)rb.Property("SumOrderNumbers")).Value.ToString();
                    OutCutomerInfo.SumOrderDates = ((RedProperty)rb.Property("SumOrderDates")).Value.ToString();
                    OutCutomerInfo.SumShipNames = ((RedProperty)rb.Property("SumShipNames")).Value.ToString();
                    OutCutomerInfo.SumShipStatus = ((RedProperty)rb.Property("SumShipStatus")).Value.ToString();
                    OutCutomerInfo.SumOrderTotal = ((RedProperty)rb.Property("SumOrderTotal")).Value.ToString();
                    OutCutomerInfo.DetOrderNumber = ((RedProperty)rb.Property("DetOrderNumber")).Value.ToString();
                    OutCutomerInfo.DetOrderDate = ((RedProperty)rb.Property("DetOrderDate")).Value.ToString();
                    OutCutomerInfo.DetOrderAmount = ((RedProperty)rb.Property("DetOrderAmount")).Value.ToString();
                    OutCutomerInfo.DetShipName = ((RedProperty)rb.Property("DetShipName")).Value.ToString();
                    OutCutomerInfo.DetShipAdd1 = ((RedProperty)rb.Property("DetShipAdd1")).Value.ToString();
                    OutCutomerInfo.DetShipAdd2 = ((RedProperty)rb.Property("DetShipAdd2")).Value.ToString();
                    OutCutomerInfo.DetShipCity = ((RedProperty)rb.Property("DetShipCity")).Value.ToString();
                    OutCutomerInfo.DetShipState = ((RedProperty)rb.Property("DetShipState")).Value.ToString();
                    OutCutomerInfo.DetShipZip = ((RedProperty)rb.Property("DetShipZip")).Value.ToString();
                    OutCutomerInfo.DetShipNumber = ((RedProperty)rb.Property("DetShipNumber")).Value.ToString();
                    OutCutomerInfo.DetShipItems = ((RedProperty)rb.Property("DetShipItems")).Value.ToString();
                    OutCutomerInfo.DetShipQty = ((RedProperty)rb.Property("DetShipQty")).Value.ToString();
                    OutCutomerInfo.DetShipDesc = ((RedProperty)rb.Property("DetShipDesc")).Value.ToString();
                    OutCutomerInfo.DetShipVia = ((RedProperty)rb.Property("DetShipVia")).Value.ToString();
                    OutCutomerInfo.DetShipDate = ((RedProperty)rb.Property("DetShipDate")).Value.ToString();
                    OutCutomerInfo.DetShipTrackNums = ((RedProperty)rb.Property("DetShipTrackNums")).Value.ToString();
                    OutCutomerInfo.DetShipTrackLink = ((RedProperty)rb.Property("DetShipTrackLink")).Value.ToString();
                    OutCutomerInfo.DetShipEstDelBegin = ((RedProperty)rb.Property("DetShipEstDelBegin")).Value.ToString();
                    OutCutomerInfo.DetShipEstDelEnd = ((RedProperty)rb.Property("DetShipEstDelEnd")).Value.ToString();
                    OutCutomerInfo.DetOpenLocs = ((RedProperty)rb.Property("DetOpenLocs")).Value.ToString();
                    OutCutomerInfo.DetOpenItems = ((RedProperty)rb.Property("DetOpenItems")).Value.ToString();
                    OutCutomerInfo.DetOpenQtys = ((RedProperty)rb.Property("DetOpenQtys")).Value.ToString();
                    OutCutomerInfo.DetOpenDesc = ((RedProperty)rb.Property("DetOpenDesc")).Value.ToString();
                    OutCutomerInfo.DetOpenEstDelBegin = ((RedProperty)rb.Property("DetOpenEstDelBegin")).Value.ToString();
                    OutCutomerInfo.DetOpenEstDelEnd = ((RedProperty)rb.Property("DetOpenEstDelEnd")).Value.ToString();
                    OutCutomerInfo.StatusErr = ((RedProperty)rb.Property("StatusErr")).Value.ToString();
                    OutCutomerInfo.StatusMsg = ((RedProperty)rb.Property("StatusMsg")).Value.ToString();

                    ArryCustInfo[1] = ((RedProperty)rb.Property("CustNumber")).Value.ToString();
                    ArryCustInfo[2] = ((RedProperty)rb.Property("CustName")).Value.ToString();
                    ArryCustInfo[3] = ((RedProperty)rb.Property("CustAdd1")).Value.ToString();
                    ArryCustInfo[4] = ((RedProperty)rb.Property("CustAdd2")).Value.ToString();
                    ArryCustInfo[5] = ((RedProperty)rb.Property("CustState")).Value.ToString();
                    ArryCustInfo[6] = ((RedProperty)rb.Property("CustCity")).Value.ToString();
                    ArryCustInfo[7] = ((RedProperty)rb.Property("CustZip")).Value.ToString();
                    ArryCustInfo[8] = ((RedProperty)rb.Property("CustCountry")).Value.ToString();
                    ArryCustInfo[9] = ((RedProperty)rb.Property("CustPhone")).Value.ToString();
                    ArryCustInfo[10] = ((RedProperty)rb.Property("CustFax")).Value.ToString();
                    ArryCustInfo[11] = ((RedProperty)rb.Property("CustEmail")).Value.ToString();
                    ArryCustInfo[12] = ((RedProperty)rb.Property("CustPin")).Value.ToString();
                    ArryCustInfo[13] = ((RedProperty)rb.Property("Title")).Value.ToString();
                    ArryCustInfo[14] = ((RedProperty)rb.Property("SumOrderNumbers")).Value.ToString();
                    ArryCustInfo[15] = ((RedProperty)rb.Property("SumOrderDates")).Value.ToString();
                    ArryCustInfo[16] = ((RedProperty)rb.Property("SumShipNames")).Value.ToString();
                    ArryCustInfo[17] = ((RedProperty)rb.Property("SumShipStatus")).Value.ToString();
                    ArryCustInfo[18] = ((RedProperty)rb.Property("SumOrderTotal")).Value.ToString();
                    ArryCustInfo[19] = ((RedProperty)rb.Property("DetOrderNumber")).Value.ToString();
                    ArryCustInfo[20] = ((RedProperty)rb.Property("DetOrderDate")).Value.ToString();
                    ArryCustInfo[21] = ((RedProperty)rb.Property("DetOrderAmount")).Value.ToString();
                    ArryCustInfo[22] = ((RedProperty)rb.Property("DetShipName")).Value.ToString();
                    ArryCustInfo[23] = ((RedProperty)rb.Property("DetShipAdd1")).Value.ToString();
                    ArryCustInfo[24] = ((RedProperty)rb.Property("DetShipAdd2")).Value.ToString();

                    ArryCustInfo[25] = ((RedProperty)rb.Property("DetShipCity")).Value.ToString();
                    ArryCustInfo[26] = ((RedProperty)rb.Property("DetShipState")).Value.ToString();
                    ArryCustInfo[27] = ((RedProperty)rb.Property("DetShipZip")).Value.ToString();
                    ArryCustInfo[28] = ((RedProperty)rb.Property("DetShipNumber")).Value.ToString();
                    ArryCustInfo[29] = ((RedProperty)rb.Property("DetShipItems")).Value.ToString();
                    ArryCustInfo[30] = ((RedProperty)rb.Property("DetShipQty")).Value.ToString();
                    ArryCustInfo[31] = ((RedProperty)rb.Property("DetShipDesc")).Value.ToString();
                    ArryCustInfo[32] = ((RedProperty)rb.Property("DetShipVia")).Value.ToString();
                    ArryCustInfo[33] = ((RedProperty)rb.Property("DetShipDate")).Value.ToString();
                    ArryCustInfo[34] = ((RedProperty)rb.Property("DetShipTrackNums")).Value.ToString();
                    ArryCustInfo[35] = ((RedProperty)rb.Property("DetShipTrackLink")).Value.ToString();
                    ArryCustInfo[36] = ((RedProperty)rb.Property("DetShipEstDelBegin")).Value.ToString();
                    ArryCustInfo[37] = ((RedProperty)rb.Property("DetShipEstDelEnd")).Value.ToString();
                    ArryCustInfo[38] = ((RedProperty)rb.Property("DetOpenLocs")).Value.ToString();
                    ArryCustInfo[39] = ((RedProperty)rb.Property("DetOpenItems")).Value.ToString();
                    ArryCustInfo[40] = ((RedProperty)rb.Property("DetOpenQtys")).Value.ToString();

                    ArryCustInfo[41] = ((RedProperty)rb.Property("DetOpenDesc")).Value.ToString();
                    ArryCustInfo[42] = ((RedProperty)rb.Property("DetOpenEstDelBegin")).Value.ToString();
                    ArryCustInfo[43] = ((RedProperty)rb.Property("DetOpenEstDelEnd")).Value.ToString();
                    ArryCustInfo[44] = ((RedProperty)rb.Property("StatusErr")).Value.ToString();
                    ArryCustInfo[45] = ((RedProperty)rb.Property("StatusMsg")).Value.ToString();
                //}
                CustInfo.Rows.Add(ArryCustInfo);
                OutPutParam = OrderStatusGetData(ArryCustInfo);
                string LogOn = RedBackServiceGetLogOnDetails(Path);
                StringBuilder MethodName = new StringBuilder();
                if (LogOn == "1")
                {
                    MethodName.AppendLine(" ");
                    MethodName.AppendLine("Method Name : OrderStatus");
                    MethodName.AppendLine(" ");
                    InputParam.AppendLine(" ");
                    InputParam.AppendLine("OutPut Details");
                    InputParam.AppendLine(" ");
                    string Log = MethodName.ToString() + InputParam.ToString();
                    Log = Log + OutPutParam.ToString();
                    WriteServiceLog(Log, Path);
                    //WriteServiceLog("Method GetPost End" + DateTime.Now, Path);
                    WriteServiceLog("Method GetPost End", Path);
                }
                return obj.Serialize(OutCutomerInfo);

            }

            public bool  InsertServiceLog(DateTime InsertDateTime, string Message ,String vType )
            {
                using (MySqlConnection mysqlConn = new MySqlConnection(Connection))
                {
                    using (MySqlCommand mysqlCommand = new MySqlCommand())
                    {

                        mysqlCommand.CommandType = CommandType.StoredProcedure;
                        mysqlCommand.CommandText = "InsertServiceLog";
                        mysqlCommand.Connection = mysqlConn;

                        mysqlCommand.Parameters.Add(new MySqlParameter("?vInsertDateTime", MySqlDbType.DateTime));
                        mysqlCommand.Parameters["?vInsertDateTime"].Direction = ParameterDirection.Input;
                        mysqlCommand.Parameters["?vInsertDateTime"].Value = InsertDateTime;

                        mysqlCommand.Parameters.Add(new MySqlParameter("?vMessage", MySqlDbType.LongText));
                        mysqlCommand.Parameters["?vMessage"].Direction = ParameterDirection.Input;
                        mysqlCommand.Parameters["?vMessage"].Value = Message;

                        mysqlCommand.Parameters.Add(new MySqlParameter("?vType", MySqlDbType.VarChar));
                        mysqlCommand.Parameters["?vType"].Direction = ParameterDirection.Input;
                        mysqlCommand.Parameters["?vType"].Value = vType;

                        if (mysqlConn.State == ConnectionState.Closed)
                            mysqlConn.Open();
                        mysqlCommand.ExecuteNonQuery();
                    }
                    return true;
                } 
            }
        }
    }
}
